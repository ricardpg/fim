%
% TakeImagePatch ----------------------------------------------------------
%
%  Function takes Image Patches around specified keypoints. Patches can be 
%  circular or rectangular, they can be rotated according to the specified
%  orientation of the keypoints or not.
%
%  Author(s)     : Marina Kudinova
%  e-mail        : kudinova@eia.udg.es
%  Working Group : Computer Vision and Robotics Group
%  Homepage      : http://vicorob.udg.es/
%  Project       : FIM
%  File          : TakeImagePatch.m
%  Date          : 19/06/2007 - 21/06/2007
%  Compiler      : MATLAB >= 7.0
%
% -------------------------------------------------------------------------
%
%  U S A G E :
%
%  [ Patches, PatchesInd ] = TakeImagePatch ( I, Frames, Radius, Circular,
%                                             Rotate )
%
%  I N P U T :
%
%  I        - image where the frames were detected.
%  Frames   - matrix with keypoints, where X and Y should be in the row1 
%             and row2 and Orientation should be in the row4 if Rotate = 1.
%  Radius   - radius of the image patch to take.
%  Circular - boolean: 1 - use circular patches; 0 - rectangular.
%  Rotate   - boolean: 1 - rotate image patches according to orientation of
%             the keypoints; 0 - not rotate.
%
%  O U T P U T :
%
%  Patches    - matrix containing each patch as a column.
%  PatchesInd - matrix containing indices of the Patches in the image I.
%               Indices are global, can be used directly to address pixels 
%               of the image.
%
% -------------------------------------------------------------------------

function ...
[ Patches, PatchesInd ] = TakeImagePatch ( I, Frames, Radius, ...
                                           Circular, Rotate )

% Test the input parameters
error ( nargchk ( 3, 5, nargin ) );
error ( nargoutchk ( 1, 2, nargout ) );

if isempty(Frames); Patches = []; return; end

if nargin == 3; Circular = 1; Rotate = 1; end

% Input Radius
Rin = round ( Radius );

% radius calculated from Maximum possible Rotation
Rrot = round ( ( Rin ) * sqrt(2) ) ;

% preparation of the circular mask
CorrMask = find(ceil(fspecial('disk',Rin)));

% length of the "correlation" descriptor
if Circular; L = size(CorrMask,1); else L = (2*Rin+1).^2; end

[ h, w ] = size ( I );
n = size ( Frames, 2 );
Patches = zeros(L,n);
PatchesInd = zeros(L,n);

if Rotate; R = Rrot; else R = Rin; end

for j = 1 : n

    x = round(Frames(1,j));
    y = round(Frames(2,j));
    RotAngle = -rad2deg(Frames(4,j));

    % Avoid keypoints in the borders of the Images
    if ( x > R ) && ( x < w-R ) && ...
       ( y > R ) && ( y < h-R ) && ...
       ( x > R ) && ( x < w-R ) && ...
       ( y > R ) && ( y < h-R )

        ImPatch = I ( y-R : y+R, x-R : x+R );
        Prows = repmat ( ( y-Rin:y+Rin )', 1, 2*Rin+1 );
        Pcols = repmat ( ( x-Rin:x+Rin ), 2*Rin+1, 1 );
        
        if Rotate
            ImPatchR = imrotate ( ImPatch, RotAngle, 'bicubic', 'crop' );
            ImPatch = ImPatchR ( R+1-Rin : R+1+Rin, R+1-Rin : R+1+Rin );
        end

        if Circular
            ImPatch = ImPatch ( CorrMask );
            Prows = Prows(CorrMask);
            Pcols = Pcols(CorrMask);
        else
            ImPatch = ImPatch ( : );
            Prows = Prows(:);
            Pcols = Pcols(:);
        end
        
        IND = sub2ind(size(I),Prows,Pcols);
    else
        ImPatch = repmat(NaN,L,1);
        IND = NaN;
    end
    Patches(:,j) = ImPatch;        
    PatchesInd(:,j) = IND;
end

% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% END

% recovery of the rectangular image patch from the descriptor form;

% 2Dpatch = zeros(2*Rin+1);
% CorrMask = find(ceil(fspecial('disk',Rin)));
% if Circular
%     2Dpatch(CorrMask) = ImPatch;
% else
%     2Dpatch(1:(2*Rin+1)^2) = ImPatch;
% end