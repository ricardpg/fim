%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.es
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.es
%
%  Module        : Compute the SIFT descriptor arround a set of Keypoints.
%
%  File          : SiftDescriptor.m
%  Date          : 15/01/2007 - 15/01/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written in ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  SiftDescriptor Compute the SIFT descriptors arround a set of Keypoints.
%
%     Descriptors = SiftDescriptor ( InputImg, Frames )
%
%     Input Parameters:
%      InputImg: Source MxN graylevel image.
%      Frames: Set of 5xK frames that must contain the X, Y, Scale,
%              Orientation, Goodness and Octave.
%
%     Output Parameters:
%      Descriptors: A matrix of 128xK Sift descriptors.
%

function Descriptors = SiftDescriptor ( InputImg, Frames )
  % Test the input parameters
  error ( nargchk ( 2, 2, nargin ) );
  error ( nargoutchk ( 1, 1, nargout ) );

  % Compute the sizes
  [h, w] = size ( InputImg );
  [rf, cf] = size ( Frames );
  % Test the Sizes
  if h <= 16 || w <= 16,
    error ( 'MATLAB:SiftDescriptor:Input', ...
            'Input Image Width and Height must be bigger than 16!' );
  end

  % Test Frames Size: (x, y, Scale, Orientation, Octave)
  if rf ~= 6
    error ( 'MATLAB:SiftDescriptor:Input', ...
            'Input Frames must have 5 columns: X, Y, Scale, Orientation, Goodness and Octave!' );
  end

  % Set Lowe Parameters for computing the scale space
  S = 3;                         % Scale
  OMin = -1;                     % Minimum octave -> Double Source Img Size
  % OMin = 0;                     % Minimum octave -> Do not Double source Img Size
  O = floor ( log2 ( min ( h, w ) ) ) - OMin - 3;  % Up to Images of 8x8 pixels
  Sigma0 = 1.6 * 2 ^ ( 1 / S );  % Starting Sigma
  SigmaN = 0.5;                  % Nominal pre-smothing of the Source Img

  % Compute Scale Space
  Gss = gaussianss ( InputImg, SigmaN, O, S, OMin, -1, S + 1, Sigma0 );

  % The siftdescriptor function uses the octave -> Call only by octaves.
  % Sort by octaves without losing their position in the source vector.
  for o = 1 : Gss.O
    [I, J] = find ( Frames(6,:) == o );
    if size ( J, 1 )
      % Compute the frames ( X, Y, Scale, Orientation ) at the Octave and Scale
      oframes = [];
      oframes(1,:) = Frames(1,J) / ( 2^( o-1+Gss.omin ) );
      oframes(2,:) = Frames(2,J) / ( 2^( o-1+Gss.omin ) );
      oframes(3,:) = log2 ( Frames(3,J) / ( 2^(o-1+Gss.omin) * Gss.sigma0 ) ) * Gss.S;
      oframes(4,:) = Frames(4,J);
      % Compute Descriptor
      oDescriptors = siftdescriptor ( Gss.octave{o}, oframes, Gss.sigma0, Gss.S, Gss.smin );

      % Add new descriptor
      Descriptors(:,J) = oDescriptors;
    end
  end
end
