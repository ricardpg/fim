%
%  Autor(s)      : Jordi Ferrer Plana, Marina Kudinova
%  e-mail        : jferrerp@eia.udg.es, kudinova@eia.udg.es
%
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.es
%
%  Module        : Compute the SURF descriptor arround a set of Keypoints.
%
%  File          : SurfDescriptor.m
%  Date          : 15/01/2007 - 01/02/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written in ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  SurfDescriptor Compute the SURF descriptors arround a set of Keypoints.
%
%     Descriptors = SurfDescriptor ( InputImg, Frames )
%
%     Input Parameters:
%      SrcImg: Source MxN graylevel image.
%      Frames: Set of 4xK frames that must contain the X, Y, Scale and
%              Strength (Goodness).
%
%     Output Parameters:
%      Descriptors: A matrix of 128xK Surf descriptors.
%

function Descriptors = SurfDescriptor ( InputImg, Frames, INdoubleImageSize, ...
                                        INupright, INextended, INindexSize )
  % Test the input parameters
  error ( nargchk ( 2, 6, nargin ) );
  error ( nargoutchk ( 1, 1, nargout ) );

  % Compute the sizes
  rf = size ( Frames, 1 );

  % Test Frames Size: (x, y, Scale and Orientation)
  if rf ~= 5
    error ( 'MATLAB:SurfDescriptor:Input', ...
            'Input Frames must have 5 columns: X, Y, Scale, Orientation and Strength (Goodness)!' );
  end
  
  % Descriptors = SurfDescriptorMx ( Image, Frames, doubleImageSize = false,
  %                                  upright = false, extended = false,
  %                                  indexSize = 4 );
  
  % Default parameters
  if nargin == 2
      doubleImageSize = false;
      upright = false;
      extended = false;
      indexSize = 4;
  end
  
  % If parameters are provided as input, take them
  if nargin == 6
      doubleImageSize = INdoubleImageSize;
      upright = INupright;
      extended = INextended;
      indexSize = INindexSize;
  end
  
  % Can be used only 2 first input paramemters or all 6 ones  
  Descriptors = SurfDescriptorMx ( InputImg, Frames, doubleImageSize, upright, extended, indexSize );
      
end
