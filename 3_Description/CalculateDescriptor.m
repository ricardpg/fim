% -------------------------------------------------------------------------
%
%  Autor(s)      : Jordi Ferrer Plana, Marina Kudinova
%  e-mail        : {jferrerp,kudinova}@eia.udg.es
%  Branch        : Computer Vision
%  Working Group : Underwater Vision Lab
%  Project       : FIM
%  Homepage      : http://porcsenglar.udg.es
%  Module        : Compute an image descriptor arround a set of Keypoints.
%  File          : CalculateDescriptor.m
%  Date          : 15/08/2006 - 20/06/2007
%  Compiler      : MATLAB >= 7.0
%  Notes         : - File written in ISO-8859-1 encoding.
%
% -------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -------------------------------------------------------------------------
%
%  Usage:
%
%  CalculateDescriptor Compute an image descriptors arround a set of Keypoints.
%
%     Descriptors = CalculateDescriptor ( InputImg, Frames, DescriptionParameters )
%
%     Input Parameters:
%        InputImg:              Source MxN graylevel image.
%        Frames:                Set of [5/6]xK frames that must contain the X, Y, Scale,
%                               Orientation, Octave and optative Goodness (never used).
%        DescriptionParameters: Structure with the configuration for the
%                               description step.
%        DescType:             { 'SIFT', 'SURF', 'SURF-128', 'SURF-36', 'U-SURF', 
%                              'U-SURF-128', 'U-SURF-36' };
%     Output Parameters:
%        Descriptors:          A matrix of [128,64,36]xK descriptors.
%                              ( 128: SIFT, SURF-128, U-SURF-128 )
%                              ( 64:  SURF, U-SURF )
%                              ( 36:  SURF-36, U-SURF-36 )
%
% -------------------------------------------------------------------------

function Descriptors = CalculateDescriptor ( InputImg, Frames, DescriptionParameters )

% Test the input parameters
error ( nargchk ( 2, 3, nargin ) );
error ( nargoutchk ( 1, 1, nargout ) );

% Check input image dimensions
if ndims ( InputImg ) ~= 2
    error ( 'MATLAB:CalculateDescriptor:Input', 'Incorrect Input matrix sizes!' );
end

%% ~~~~~~~~~~ Checking DescriptionParameters

if nargin == 2 % If DescriptionParameters is omitted => Use default values
    DescrParamsChecked = CheckDescriptionParameters(1);
    DescType = DescrParamsChecked.DescType;
    PatchRadius = DescrParamsChecked.PatchRadius;
else
    DescrParamsChecked = CheckDescriptionParameters(DescriptionParameters);
    if ~isstruct(DescrParamsChecked)
        Descriptors = [];
        return;
    else
        DescType = DescrParamsChecked.DescType;
        PatchRadius = DescrParamsChecked.PatchRadius;
    end
end

%% ~~~~~~~~~~ Calculation of Descriptors

if strcmpi ( DescType, 'sift' ),
    % Provide the X, Y, Scale, Orientation and Octave
    Descriptors = SiftDescriptor ( InputImg, Frames );
elseif strcmpi ( DescType, 'surf' ),
    % Provide the X, Y, Scale, Orientation, Strength (Goodness)
    Descriptors = SurfDescriptor ( InputImg, Frames );
elseif strcmpi ( DescType, 'u-surf' ),
    Descriptors = SurfDescriptor ( InputImg, Frames, false, true, false, 4 );
elseif strcmpi ( DescType, 'surf-128' ),
    Descriptors = SurfDescriptor ( InputImg, Frames, false, false, true, 4 );
elseif strcmpi ( DescType, 'u-surf-128' ),
    Descriptors = SurfDescriptor ( InputImg, Frames, false, true, true, 4 );
elseif strcmpi ( DescType, 'surf-36' ),
    Descriptors = SurfDescriptor ( InputImg, Frames, false, false, false, 3 );
elseif strcmpi ( DescType, 'u-surf-36' ),
    Descriptors = SurfDescriptor ( InputImg, Frames, false, true, false, 3 );
elseif strcmpi ( DescType, 'ImagePatch' ),
    Descriptors = TakeImagePatch ( InputImg, Frames, PatchRadius, 1, 0 );
elseif strcmpi ( DescType, 'RotatedImagePatch' ),
    Descriptors = TakeImagePatch ( InputImg, Frames, PatchRadius, 1, 1 );
else
    error ( 'MATLAB:CalculateDescriptor:InputOutput', ...
          [ 'Unknown descriptor type!\n',...
            'Available: SIFT, SURF, SURF-128, SURF-36, U-SURF\n',...
            '           U-SURF-128, U-SURF-36, ImagePatch,\n',...
            '           RotatedImagePatch.' ] );
end
end
