
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%  Rejecting the MisMatched Keypoints using Correlation
%
%  Author(s)     : Marina Kudinova
%  e-mail        : kudinova@eia.udg.es
%
%  Working Group : Computer Vision and Robotics Group
%  Homepage      : http://vicorob.udg.es/
%
%  Project       : FIM
%  File          : CorrCheck.m
%  Date          : 25/09/2006 - 09/10/2006
%  Compiler      : MATLAB >= 7.0
%
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function [ RejectedMatchesCorr, AcceptedMatchesCorr ] = ...           
           CorrCheck ( Img1, Img2, Img1Features, Img2Features, ...
                       Matches, Radius, Threshold )

    if ismember( nargin, [5,6,7] ) && nargout == 2
        
        if ~exist('Radius'); Radius = 14; end;
        if ~exist('Threshold'); Threshold = 0.7; end;
        
        if sum(abs(size(Img1(:,:,1)) - size(Img2(:,:,1)))) == 0 && ...
           ~isempty(Img1Features) && size(Img1Features,1) >= 4 && ... 
           ~isempty(Img2Features) && size(Img2Features,1) >= 4 && ...
           ~isempty(Matches) && size(Matches,1) >=2 && ...
           Radius >=2 && abs(Threshold) <= 1
      
           [ h1, w1 ] = size ( Img1 );
           [ h2, w2 ] = size ( Img2 );
           R = round ( Radius );
           Rrot = round ( ( R ) * sqrt(2) ) ; % radius calculated from Maximum possible Rotation
           n = size ( Matches, 2 );
           T = Threshold^2;
           CorrInliers = [];
           CorrOutliers = [];
           AcceptedCorrCoef = [];
           RejectedCorrCoef = [];
           CloseToBorders = [];
           
           %preparation of the circular mask
           CorrMask = find(ceil(fspecial('disk',Radius)));
           
           for j = 1 : n

               i1 = Matches(2,j);
               i2 = Matches(1,j);
               x1 = round(Img1Features(1,i1));
               y1 = round(Img1Features(2,i1));
               x2 = round(Img2Features(1,i2));
               y2 = round(Img2Features(2,i2));
               RotAngle = -rad2deg(Img1Features(4,i1)-Img2Features(4,i2));

               % Avoid keypoints in the borders of the Images
               if ( x1 > Rrot ) && ( x1 < w1-Rrot) && ...
                       ( y1 > Rrot ) && ( y1 < h1-Rrot ) && ...
                       ( x2 > Rrot ) && ( x2 < w2-Rrot) && ...
                       ( y2 > Rrot ) && ( y2 < h2-Rrot )

                   ImgTile1 = Img1 ( y1-R : y1+R, x1-R : x1+R );
                   ImgTile1 = ImgTile1 ( CorrMask );
                   % just the area in the Im2 is rotated to the area in Im1
                   % to avoid double interpolation
                   SImgTile2 = Img2 ( y2-Rrot:y2+Rrot, x2-Rrot:x2+Rrot );
                   K2 = imrotate ( SImgTile2, RotAngle, 'bicubic', 'crop');
                   ImgTile2 = K2 ( Rrot+1-R:Rrot+1+R, Rrot+1-R:Rrot+1+R );
                   ImgTile2 = ImgTile2 ( CorrMask );

                   CorrCoef = correl ( ImgTile1, ImgTile2 );

                   if CorrCoef >= T
                       CorrInliers = [ CorrInliers, j ];
                       AcceptedCorrCoef = [ AcceptedCorrCoef, CorrCoef ];
                   else
                       CorrOutliers = [ CorrOutliers, j ];
                       RejectedCorrCoef = [ RejectedCorrCoef, CorrCoef ];
                   end
               else
                   CloseToBorders = [ CloseToBorders, Matches(:,j) ];
               end
           end

           if ~isempty ( CorrInliers )
               AcceptedMatchesCorr = Matches ( : , CorrInliers );
           else
               AcceptedMatchesCorr = [];
           end

           if ~isempty ( CorrOutliers )
               RejectedMatchesCorr = Matches ( : , CorrOutliers );
           else
               RejectedMatchesCorr = [];
           end

           AcceptedCorrCoef;
           RejectedCorrCoef;
           CloseToBorders;

        else
            if sum(abs(size(Img1(:,:,1)) - size(Img2(:,:,1)))) ~= 0
                error ( 'MATLAB:FIM:CorrCheck:Input', ...
                        'Incorrect Input Images sizes!' );
            end
            if size(Img1Features,1) < 4
                error ( 'MATLAB:FIM:CorrCheck:Input', ...
                        'Incorrect Img1Features input matrix!' );
            end
            if size(Img2Features,1) < 4
                error ( 'MATLAB:FIM:CorrCheck:Input', ...
                        'Incorrect Img2Features input matrix!' );
            end
            if size(Matches,1) < 2
                error ( 'MATLAB:FIM:CorrCheck:Input', ...
                        'Incorrect Matches input matrix!' );
            end
            if Radius < 2
                error ( 'MATLAB:FIM:CorrCheck:Input', ...
                        ['Incorrect Radius of Correlation Window : ', ...
                        'Has to be >= 2!'] );
            end
            if abs(Threshold) > 1
                error ( 'MATLAB:FIM:CorrCheck:Input', ...
                        ['Incorrect Threshold for Correlation ', ...
                        'Coefficient: Not in the [-1,1] range!']);
            end
            RejectedMatchesCorr = [];
            AcceptedMatchesCorr = [];
        end
    else
        error ( ['Usage: ',...
                 '[ RejectedMatchesCorr, AcceptedMatchesCorr ] = ',...
                 'CorrCheck ( Img1, Img2, Img1Features, Img2Features, ',...
                 'Matches, Radius, Threshold )']);
        RejectedMatchesCorr = [];
        AcceptedMatchesCorr = [];
end
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% END
