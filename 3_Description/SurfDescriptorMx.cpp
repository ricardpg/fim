/*
  Autor(s)      : Marina Kudinova, Jordi Ferrer Plana
  e-mail        : kudinova@eia.udg.es, jferrerp@eia.udg.es
  Branch        : Computer Vision

  Working Group : Underwater Vision Lab
  Project       : -

  Homepage      : http://porcsenglar.udg.es

  Module        : 

  File          : SurfDescriptorMx.cpp
  Date          : 12/01/2007 - 01/02/2007

  Compiler      : MATLAB >= 7.0 & g++ >= 4.x
  Libraries     : -

  Notes         : - File written using ISO-8859-1 encoding.

 -----------------------------------------------------------------------------

  Copyright (C) 2005-2006 by Jordi Ferrer Plana

  This source code is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This source code is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
  more details.

 -----------------------------------------------------------------------------
*/

#include <math.h>         // Standard Template Library
#include <cassert>
#include <cstdlib>

#include <mex.h>          // Matlab Mex Functions

#include <image.h>        // SURF Code
#include <ipoint.h>
#include <fasthessian.h>
#include <surf.h>

#define ERROR_HEADER       "MATLAB:SurfDescriptorMx:Input\n"

using namespace surf;
using namespace std;

// Identify the input parameters by it's index
enum { I=0, FRAMES, DOUBLEIMAGESIZE, UPRIGHT, EXTENDED, INDEXSIZE };
// Identify the output parameters by it's index
enum { DESCRIPTORS=0 };

void mexFunction ( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  // Parameter checks
  if ( ( nlhs != 1 ) || ( nrhs != 6 && nrhs != 2 ) )
    mexErrMsgTxt ( ERROR_HEADER 
                   "Descriptors = SurfDescriptorMx ( Image, Frames [, doubleImageSize = false,\n"
                   "                                 upright = false, extended = false, indexSize = 4 ] )" );

  if ( mxGetNumberOfDimensions ( prhs[I] ) != 2 && mxIsDouble ( prhs[I] ) )
    mexErrMsgTxt ( ERROR_HEADER "Image must be a NxM double matrix!" );

  // One column => One Frame
  int f = mxGetM ( prhs[FRAMES] );  // Rows: X, Y, Scale, Orientation, Strenght
  int n = mxGetN ( prhs[FRAMES] );

  // Defaults
  bool doubleImageSize = false;
  bool upright = false;
  bool extended = false;
  int indexSize = 4;
  
  // Use the provided parameters
  if ( nrhs == 6 )
  {
    if ( mxIsLogical ( prhs[DOUBLEIMAGESIZE] ) && mxIsLogical ( prhs[UPRIGHT] ) &&
         mxIsLogical ( prhs[EXTENDED] ) && mxIsDouble ( prhs[INDEXSIZE] ) )
    {
      doubleImageSize = *(bool *)mxGetPr ( prhs[DOUBLEIMAGESIZE] );
      upright = *(bool *)mxGetPr ( prhs[UPRIGHT] );
      extended = *(bool *)mxGetPr ( prhs[EXTENDED] );
      indexSize = (int)(*(double *)mxGetPr ( prhs[INDEXSIZE] ));
      // mexPrintf ( "\nupright = %d, extended = %d, indexsize = %d \n", upright, extended, indexSize);
    }
    else
      mexErrMsgTxt ( ERROR_HEADER "All the parameters must be scalars!" );
  }
 
  if ( f != 5 )
    mexErrMsgTxt ( ERROR_HEADER "Frames must be a 4xK matrix containing X, Y, Scale, Orientation and Strenght (Goodness)!" );

  int DescSize = (indexSize * indexSize) << ( extended ? 3 : 2 );
  
  // Empty frames => Empty descriptors
  if ( n <= 0 )
  {
    plhs[DESCRIPTORS] = mxCreateDoubleMatrix ( 0, 0, mxREAL );

    return;
  }
  else
    // Allocate space for the result
    plhs[DESCRIPTORS] = mxCreateDoubleMatrix ( DescSize, n, mxREAL );

  int h = mxGetM ( prhs[I] );
  int w = mxGetN ( prhs[I] );

  // Construct initial image
  Image SrcImage ( w, h );

  // Pointer to the input Image
  double *Mi = (double *)mxGetPr ( prhs [I] );

  // Copy from Input image to the Surf Image
  for ( int x = 0; x < SrcImage.getWidth ( ); x++ )
    for ( int y = 0; y < SrcImage.getHeight ( ); y++ )
      SrcImage.setPix ( x, y, *Mi++ );

  // Create the integral image
  Image iimage ( &SrcImage, doubleImageSize );

  // Initialise the SURF descriptor
  Surf des ( &iimage,          // pointer to integral image
             doubleImageSize,  // double image size flag 
             upright,          // rotation invariance or upright
             extended,         // use the extended descriptor
             indexSize );      // square size of the descriptor window (default 4x4)

  int i, j;

  // Pointer to the input Frames
  double *Fi = (double *)mxGetPr ( prhs[FRAMES] );

  // Get the pointer to the result
  double *Descriptors = (double *)mxGetPr ( plhs[DESCRIPTORS] );

  for ( i = 0; i < n; i++ )
  {
    Ipoint Point;

    Point.x = *Fi++;
    Point.y = *Fi++;
    Point.scale = *Fi++;
    Point.ori = *Fi++;
    Point.strength = *Fi++;

    // Non-set parameters in the detection step
    Point.ivec = NULL;  // To check if it is assigned or not
    Point.laplace = 0;

    // set the current interest point
    des.setIpoint ( &Point );
    // assign reproducible orientation
    //des.assignOrientation ( );
    // make the SURF descriptor
    des.makeDescriptor ( );

    if ( Point.ivec )
    {
      for ( j = 0; j < DescSize; j++ )
        *Descriptors++ = Point.ivec[j];
    }
    else
      for ( j = 0; j < DescSize; j++ )
        *Descriptors++ = 0;
  }
}
