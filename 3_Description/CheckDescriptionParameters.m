%% CheckDescriptionParameters
%
%  The CheckDescriptionParameters function process the structure, that
%  contains input parameters for the CalculateDescriptor function of the FIM.
%
%  Author(s)     : Marina Kudinova, Jordi Ferrer
%  e-mail        : { kudinova, jferrerp } @ eia.udg.es
%
%  Working Group : Computer Vision and Robotics Group
%  Homepage      : http://vicorob.udg.es/
%
%  Project       : FIM
%  File          : CheckDescriptionParameters.m
%  Date          : 20/01/2007 - 21/06/2007
%  Compiler      : MATLAB >= 7.0
%
%% FUNCTION

function ...
    DescrParamsChecked = CheckDescriptionParameters ( DescriptionParameters )

error ( nargchk ( 1, 1, nargin ) );
error ( nargoutchk ( 1, 1, nargout ) );

Descriptors = { 'SIFT', 'SURF', 'SURF-128', 'SURF-36', 'U-SURF', ...
                'U-SURF-128', 'U-SURF-36', 'ImagePatch', 'RotatedImagePatch' };

% Default Values

DefaultDescType = 'SURF';
DefaultPatchRadius = 10;

if isstruct ( DescriptionParameters )

    DescrParamsChecked.DescType = DescriptionParameters.DescType;
    if sum(strcmpi(DescrParamsChecked.DescType, Descriptors)) == 0
        error ( [ 'MATLAB:FIM:Input', 'Incorrect Descriptor!'...
            '\nPlease select the correct '...
            'DescriptionParameters.DescType!' ] );
    end

    if isfield ( DescriptionParameters, 'PatchRadius' )
        DescrParamsChecked.PatchRadius = DescriptionParameters.PatchRadius;
    end

elseif isscalar ( DescriptionParameters )

    if DescriptionParameters == 0
        DescrParamsChecked = 0;
        return;
    else % Load Default
        DescrParamsChecked.DescType = DefaultDescType;
        DescrParamsChecked.PatchRadius = DefaultPatchRadius;
    end

else
    error ( 'MATLAB:FIM:Input', ...
          [ 'Incorrect DescriptionParameters input: can be 0, '...
            '1 or structure \n with DescType field. \n'] );
end
