%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.es
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.es
%
%  Module        : Plotting the estimated motion between two images.
%
%  File          : PlotMotion.m
%  Date          : 13/09/2006 - 13/09/2006
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written in ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2006 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  PLOTMOSAIC Plot the Motion between two images.
%
%     PlotMosaic ( SrcImgs, Homographies )
%
%     Input Parameters:
%      SrcImgs: A set of MxNxK graylevel images.
%      Homographies: A set of 3x3xK planar homographies that descrive
%                    the motion between each pair in the set of SrcImgs.
%

function PlotMosaic ( SrcImgs, Homographies, Sx, Sy, MosaicPath, MosaicName )

  % Check Input Ouput Parameters
  if nargin == 6 && nargout == 0
    % Compute the Sizes
    [h, w, d, k] = size ( SrcImgs );
    [hw ww, l] = size ( Homographies );

    % Test the Sizes
    if ( h * w ) > 0 && ( hw == 3 ) && ( ww == 3 ) && ...
       ( k > 1 ) && ( k == l ) && prod ( size ( Sx ) ) == 1 && ...
       prod ( size ( Sy ) ) == 1 && Sx > 0 && Sy > 0

      % Put the initial image in the mosaic
      Mosaic = 255 * ones ( Sy, Sx, d );

      % Movie
 %     MovieMosaic = zeros ( Sy, Sx, d, l );
      AccH = [ 1 0 0; 0 1 0; 0 0 1];
      for i = 1 : l
        % Estimate the Homography      
        H = Homographies(:,:,i);
        AccH = AccH * H;

        for j = 1 : d
          % Transform the Warped Image to the Source one
          T = maketform ( 'projective', AccH' );
          [WiImg, Xd, Yd] = imtransform ( SrcImgs( :, :, j, i), T, 'nearest', 'FillValues', 0 );
          xdata = round(Xd);
          ydata = round(Yd);
          % Last On Top
          Itemp = Mosaic(ydata(1):ydata(2), xdata(1):xdata(2), j);
          ii = ( WiImg ~= 0 );
          Itemp(ii) = WiImg(ii);
          Mosaic(ydata(1):ydata(2), xdata(1):xdata(2), j) = Itemp;
        end
        Mos = double(Mosaic) / 255.0;
        %imwrite ( Mos, sprintf ( '%s/%s%3.3d.png', MosaicPath, MosaicName, i ), 'PNG' );
        imshow ( Mos );
 %       MovieMosaic ( :, :, :, i ) = double(Mosaic) / 255.0;
      end

      % Save mosaic img
%      imwrite (  double(Mosaic) / 255.0, sprintf ( '%s/%s.png', MosaicPath, MosaicName ), 'PNG' );
      % Save Movie
%      Movie = immovie ( MovieMosaic );
%      movie2avi ( Movie, sprintf ( '%s/%s%.avi', MosaicPath, MosaicName ), 'FPS', 1, 'COMPRESSION', 'None' );

 %     s = sprintf ( 'Mosaic: %d Images', k );
 %     title ( s );
 %     set ( gcf, 'Name', s );
    else
      error ( 'MATLAB:PlotMotion:InputOutput', 'Incorrect Input matrix sizes!' );
    end
  else
    error ( 'MATLAB:PlotMotion:InputOutput', ...
            'PlotMotion ( SrcImg, WrpImg, SrcFrames, WrpFrames, FrameMatches )' );
  end
end
