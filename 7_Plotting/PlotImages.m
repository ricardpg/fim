%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.es
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.es
%
%  Module        : Plotting Images function.
%
%  File          : PlotImages.m
%  Date          : 23/08/2006 - 11/09/2006
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written in ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2006 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  PLOTIMAGES Plot images obtained by the warping functions.
%
%     PlotImages ( H, SrcImg, WrpImg, SrcImgCrp, WrpImgCrp, Crop, Motions )
%
%     Input Parameters:
%      SrcImg: Source MxN graylevel image.
%      WrpImg: Warped MxN graylevel image.
%      SrcImgCrp: Source Image SrcImg cropped to KxL with the information
%                 contained in the Crop matrix.
%      WrpImgCrp: Warped Image WrpImg cropped to KxL with the information
%                 contained in the Crop matrix.
%      H: A 3x3 planar homography matrix that explains the motion from
%         SrcImgCrop to WrpImgCrp.
%      Crop: A 2x2 matrix specifying a Region Of Interest. This rectangle
%            is the minimum rectangle that have not masked data inside.
%            It's specifyed using the two corners: Top-Left, Bottom-Right.
%      Motions: Contains the motions computed using the WarpImage()
%      function.
%

function PlotImages ( SrcImg, WrpImg, SrcImgCrp, WrpImgCrp, H, Crop, Motions )

  % Check Input Ouput Parameters
  if nargin == 7 && nargout == 0
    % Compute the Sizes
    sErr = prod ( size ( SrcImg ) ) * prod ( size ( WrpImg ) );

    [hr hc] = size ( H );
    [cr cc] = size ( Crop );
    [mr mc, mz] = size ( Motions );

    % Test the Sizes
    if sErr ~= 0 && hr == 3 && hc == 3 && cr == 2 && cc == 2 && ...
                    mr == 4 && mc == 2 && mz == 2
      % Plot in Same Window if Possible
      SubPlot = 1;

      % Show the Source Image
      figure;
      if SubPlot ~= 0; subplot ( 3, 2, 1 ); end;
      imshow ( SrcImg ); hold on;

      % Draw the rectangular cropping region
      line ( [Crop(1,1) Crop(2,1) Crop(2,1) Crop(1,1) Crop(1,1)], ...
             [Crop(1,2) Crop(1,2) Crop(2,2) Crop(2,2) Crop(1,2)] );

      title ( 'Source Image' );

      % Plot the Warped Image
      if SubPlot ~= 0; subplot ( 2, 2, 2 ); else figure; end;
      imshow ( WrpImg ); hold on;

      % Estimate the Warped Image Corners using the Estimated Homography
      Corners = ApplyHomo ( H, Motions(:,:,1)' );

      % Plot the four Warped Image Estimated Corners
      for i = 1 : 4
        plot ( Corners(1,i), Corners(2,i), 'Marker', 'o', 'MarkerSize', ...
               4, 'MarkerEdgeColor','k','MarkerFaceColor', 'b' );
      end

      % Plot the four Source Image Corners
      for i = 1 : 4
        plot ( Motions(i,1,2), Motions(i,2,2), 'g+' );
      end

      % Plot the rectangular cropping region
      line ( [Crop(1,1) Crop(2,1) Crop(2,1) Crop(1,1) Crop(1,1)], ...
             [Crop(1,2) Crop(1,2) Crop(2,2) Crop(2,2) Crop(1,2)] );

      s = sprintf ( 'Warped Image\nTL(%6.2f,%6.2f)\nTR(%6.2f,%6.2f)\nBR(%6.2f,%6.2f)\nBL(%6.2f,%6.2f)', ...
                    Corners(1,1), Corners(1,2), Corners(1,3), Corners(1,4), ...
                    Corners(2,1), Corners(2,2), Corners(2,3), Corners(2,4) );

      title ( s );

      % Plot the Cropped Images
      %   The Source
      if SubPlot ~= 0; subplot ( 2, 2, 3 ); else figure; end;
      imshow ( SrcImgCrp );
      s = sprintf ( 'Source Cropped Image\nTL(%6.2f,%6.2f) - BR(%6.2f,%6.2f)', ...
                    Crop(1,1), Crop(2,1), Crop(1,2), Crop(2,2) );
      title ( s );

      %   The Warped
      if SubPlot ~= 0; subplot ( 2, 2, 4 ); else figure; end;
      imshow ( WrpImgCrp );
      s = sprintf ( 'Warped Cropped Image\nTL(%6.2f,%6.2f) - BR(%6.2f,%6.2f)', ...
                    Crop(1,1), Crop(2,1), Crop(1,2), Crop(2,2) );
      title ( s );

      set ( gcf, 'Name', 'Image Warping and Cropping' );
    else
      error ( 'MATLAB:PlotImages:InputOutput', 'Incorrect Input matrix sizes!' );

    end
  else
    error ( 'MATLAB:PlotImages:InputOutput', ...
            'PlotImages ( PlotImages ( SrcImg, WrpImg, SrcImgCrp, WrpImgCrp, H, Crop, Motions )' );
  end
end