%% SaveImage --------------------------------------------------------------
%
%  The SaveImage function save the entire contents of the specified axes
%  without the figure background in the specified ImageFormat to the
%  specified place on the disk.
%  
%  Author        : Marina Kudinova
%  e-mail        : kudinova@eia.udg.es
%  Working Group : Computer Vision and Robotics Group
%  Homepage      : http://vicorob.udg.es/
%  File          : SaveImage.m
%  Date          : 15/02/2007 - 25/06/2007
%  Compiler      : MATLAB >= 7.0
%  
% -------------------------------------------------------------------------
% 
% USAGE: 
%
% SaveImage ( FigureHandle, AxesHandle, ImageName, ImageFormat, ...
%             WhereToSave )                                 
% ImageName, ImageFormat and whereToSave should be in the string format.
%
% -------------------------------------------------------------------------

%%
function ...
SaveImage ( FigureHandle, AxesHandle, WhereToSave, ImageName, ImageFormat )    

% set ( AxesHandle, 'OuterPosition', [ 0 0 1 1 ] );
% set ( AxesHandle, 'Position', [ 0 0 1 1 ] );

% Creating the ResultsFIM directory of doesn't exist
if exist(WhereToSave,'dir') ~= 7; mkdir ( WhereToSave ); end

%Detect Platform
Arch = computer;
Arch = Arch(1:5);
if strcmpi ( Arch, 'pcwin' ); Windows = 1; else Windows = 0; end

if strcmpi ( 'eps', ImageFormat )
    
    saveas ( FigureHandle, [ WhereToSave ImageName '.eps' ], 'psc2');

else

    if strcmpi ( 'wmf', ImageFormat ) && Windows
        figure(FigureHandle);
        print ( [ WhereToSave, ImageName, '.wmf' ], '-dmeta' );
    else
        if strcmpi ( 'wmf', ImageFormat ); ImageFormat = 'jpg'; end
        saveas ( FigureHandle, [ WhereToSave, ImageName ], ImageFormat );
    end

end
