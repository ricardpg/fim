
%% PlotFIM ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%  
%  Plotting the Results of FIM algorithm and Saving the displayed figures.
%
%  Author(s)     : Marina Kudinova
%  e-mail        : kudinova@eia.udg.es
%  Working Group : Computer Vision and Robotics Group
%  Homepage      : http://vicorob.udg.es/
%  Project       : FIM
%  File          : PlotFIM.m
%  Date          : 27/09/2006 - 28/06/2007
%  Compiler      : MATLAB > 7.0
%
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function ...
Figs = PlotFIM ( InImg1, InImg2, ProcImg1, ProcImg2, ResultsFIM, PlottingParameters )

error ( nargchk ( 6, 6, nargin ) );
error ( nargoutchk ( 0, 1, nargout ) );

if ~isstruct(PlottingParameters) && isscalar(PlottingParameters) && ...
        PlottingParameters == 0
    return
end

if isfield ( PlottingParameters, 'ResizeCoeff' ) && ...
   PlottingParameters.ResizeCoeff ~= 1
        InImg1 = imresize ( InImg1, 1/PlottingParameters.ResizeCoeff );
        InImg2 = imresize ( InImg2, 1/PlottingParameters.ResizeCoeff );
end;

if isfield ( PlottingParameters, 'PlotInitialImages' ) && ...
   PlottingParameters.PlotInitialImages

   % cropping Initial if to perform clahs the Processed are cropped
    if all ( size(InImg1(:,:,1)) ~= size(ProcImg1(:,:,1)) )
        Img1 = imcrop ( InImg1,[ 0, 0, size(ProcImg1,2), size(ProcImg1,1) ] );
        Img2 = imcrop ( InImg2,[ 0, 0, size(ProcImg1,2), size(ProcImg1,1) ] );
    else
        Img1 = InImg1;
        Img2 = InImg2;
    end

else
    Img1 = ProcImg1;
    Img2 = ProcImg2;
end

% Checking Saving Parameters ----------------------------------------------

if isfield ( PlottingParameters, 'SaveFigures' )
    Save = PlottingParameters.SaveFigures;
else
    Save = 0;
end
if isfield ( PlottingParameters, 'WhereToSave' )
    WhereToSave = PlottingParameters.WhereToSave;
end
if isfield ( PlottingParameters, 'SaveInFormat' )
    Format = PlottingParameters.SaveInFormat;
else
    Format = 'png';
end

% Checking Features Input -------------------------------------------------

if isfield ( ResultsFIM, 'Img1Features')
    Img1Features = ResultsFIM.Img1Features;
else
    Img1Features = [];
end

if isfield ( ResultsFIM, 'Img2Features')
    Img2Features = ResultsFIM.Img2Features;
else
    Img2Features = [];
end

% Percentage of accepted Matches to Display -------------------------------

if isfield ( PlottingParameters, 'PercentageToDisplay' )
    Perc = PlottingParameters.PercentageToDisplay;
else
    Perc = 100;
end

% To Plot Matched Regions
if isfield ( PlottingParameters, 'PlotMatchedRegions' )
    PlotRegsUser = PlottingParameters.PlotMatchedRegions;
else
    PlotRegsUser = 1;
end

%% Initializing Structure of Figure Handles

Figs.Image1              = 0;
Figs.Image2              = 0;
Figs.InitialMatches      = 0;
Figs.RejectedMatches     = 0;
Figs.AcceptedMatches     = 0;
Figs.Motion              = 0;
Figs.Mosaic              = 0;
Figs.AllFollowingMatches = 0;

%% Images with keypoints

iptsetpref('ImshowBorder','tight');

if PlottingParameters.PlotImages
    
% Image1 ------------------------------------------------------------------

    figure; imshow ( Img1 ); hold on; axis off;
    set(gcf, 'Name', 'Image1');
    if ~isempty(Img1Features)
        plot ( Img1Features(1,:), Img1Features(2,:), 'y+' );
        set(gcf, 'Name', 'Image1 Keypoints');
        Figs.Image1 = gcf;
    end
    if Save
        SaveImage ( gcf, gca, WhereToSave, 'Image1Keypoints', Format );
    end

% Image2 ------------------------------------------------------------------

    figure; imshow ( Img2 ); hold on; axis off;
    set(gcf, 'Name', 'Image2');
    if ~isempty(Img2Features)
        plot ( Img2Features(1,:), Img2Features(2,:), 'y+' );
        set(gcf, 'Name', 'Image2 Keypoints');
        Figs.Image2 = gcf;
    end
    if Save
        SaveImage ( gcf, gca, WhereToSave, 'Image2Keypoints', Format );
    end

end

if isempty(ResultsFIM); return; end;

if isempty(Img1Features) || isempty(Img2Features); return; end;

%% MATCHES ----------------------------------------------------------------

if isfield ( ResultsFIM, 'Img1RegionsMSER') && ...
   isfield ( ResultsFIM, 'Img2RegionsMSER')
   PlotRegions = 1 && PlotRegsUser;
else
   PlotRegions = 0;
end

%% Initial Matches

if PlottingParameters.PlotInitialMatches && ...
   isfield ( ResultsFIM, 'InitialMatches') && ...
   ~isempty ( ResultsFIM.InitialMatches )
    figure;
    if PlotRegions
        [ Img1Regions, Img2Regions ] = ...
        MakeRegStruct ( ResultsFIM.InitialMatches, ResultsFIM );
        DisplayMatchedMSER ( Img1, Img2, Img1Regions, Img2Regions, ...
                             'Initial Matches' );
        Figs.InitialMatches = gcf;
    else
    DisplayMatches ( Img1, Img2, ...
                     Img1Features (1:2,ResultsFIM.InitialMatches(1,:)), ...
                     Img2Features (1:2,ResultsFIM.InitialMatches(2,:)), ...
                     'Initial Matches' );
    Figs.InitialMatches = gcf;
    end
    if Save
        SaveImage ( gcf, gca, WhereToSave, 'InitialMatches', Format );
    end
end

%% Rejected Matches

if PlottingParameters.PlotRejectedMatches && ...
   isfield ( ResultsFIM, 'RejectedMatches') && ...
   ~isempty ( ResultsFIM.RejectedMatches )
    figure;
    if PlotRegions
        clear Img1Regions Img2Regions;
        [ Img1Regions, Img2Regions ] = ...
        MakeRegStruct ( ResultsFIM.RejectedMatches, ResultsFIM );
        DisplayMatchedMSER ( Img1, Img2, Img1Regions, Img2Regions, 'Rejected Matches' );
        Figs.RejectedMatches = gcf;
    else
    DisplayMatches ( Img1, Img2, ...
                     Img1Features (1:2,ResultsFIM.RejectedMatches(1,:)), ...
                     Img2Features (1:2,ResultsFIM.RejectedMatches(2,:)), 'Rejected Matches' );
    Figs.RejectedMatches = gcf;
    end
    if Save
        SaveImage ( gcf, gca, WhereToSave, 'RejectedMatches', Format );
    end
end

%% Accepted Matches

% if not all to be displayed, taking randomly the specified percentage

Nall = size ( ResultsFIM.AcceptedMatches, 2 );
if Perc ~= 100;
    k = round ( Nall * Perc / 100 );
    ind = randsample(Nall,k);
    FigureCaption = sprintf ( 'Accepted Matches: %d%% are displayed', Perc );
else
    ind = 1:Nall;
    FigureCaption = 'Accepted Matches';
end

if PlottingParameters.PlotAcceptedMatches && ...
   isfield ( ResultsFIM, 'AcceptedMatches' ) && ...
   ~isempty ( ResultsFIM.AcceptedMatches )
    figure;
    if PlotRegions
        clear Img1Regions Img2Regions;
        [ Img1Regions, Img2Regions ] = ...
        MakeRegStruct ( ResultsFIM.AcceptedMatches(:,ind), ResultsFIM );
        DisplayMatchedMSER ( Img1, Img2, Img1Regions, Img2Regions, FigureCaption );
        Figs.AcceptedMatches = gcf;
    else
    DisplayMatches ( Img1, Img2, ...
                     Img1Features (1:2, ...
                     ResultsFIM.AcceptedMatches(1,ind)), ...
                     Img2Features (1:2, ...
                     ResultsFIM.AcceptedMatches(2,ind)), ...
                     FigureCaption );
    Figs.AcceptedMatches = gcf;
    end
    if Save
        SaveImage ( gcf, gca, WhereToSave, 'AcceptedMatches', Format );
    end
end
    
%% Motion: Accepted and Rejected matches in the single Image1

if PlottingParameters.PlotMotion
    if isfield ( ResultsFIM, 'AcceptedMatches' ) && ...
       ~isempty ( ResultsFIM.AcceptedMatches )
        AcceptedMatches = [ ResultsFIM.AcceptedMatches(1,:); ...
            ResultsFIM.AcceptedMatches(2,:) ];
    else AcceptedMatches = [];
    end
    if isfield ( ResultsFIM, 'RejectedMatches' ) && ...
       ~isempty ( ResultsFIM.RejectedMatches )
        RejectedMatches = [ ResultsFIM.RejectedMatches(1,:); ...
            ResultsFIM.RejectedMatches(2,:) ];
    else RejectedMatches = [];
    end
    if ~isempty(AcceptedMatches) || ~isempty(RejectedMatches)
        PlotMatchFilter ( Img1, ResultsFIM.Img1Features, ...
            ResultsFIM.Img2Features, AcceptedMatches, ...
            RejectedMatches, 'Motion: Green - accepted, Red - rejected' );
        Figs.Motion = gcf;
    end
    if Save && ~isempty ( ResultsFIM.InitialMatches )
        SaveImage ( gcf, gca, WhereToSave, 'Motion', Format );
    end
end

%% Mosaic

if PlottingParameters.PlotMosaic && ...
   isfield ( ResultsFIM, 'Homography' ) && ...
   ~isempty ( ResultsFIM.Homography ) && ResultsFIM.HomoCheck
    PlotMotion ( ProcImg1, ProcImg2, ResultsFIM.Homography );
    Figs.Mosaic = gcf;
    if Save
        SaveImage ( gcf, gca, WhereToSave, 'Mosaic', Format );        
    end
end

%% All Following Matches after Guided Postmatching

Nfm = size ( ResultsFIM.AllFollowingMatches, 2 );

FigureCaption = 'All Matches Following the Homography';

if PlottingParameters.PlotFollowingMatches && ...
   isfield ( ResultsFIM, 'AllFollowingMatches' ) && ...
   ~isempty ( ResultsFIM.AllFollowingMatches )
    figure;
    if PlotRegions
        clear Img1Regions Img2Regions;
        [ Img1Regions, Img2Regions ] = ...
        MakeRegStruct ( ResultsFIM.AllFollowingMatches, ResultsFIM );
        DisplayMatchedMSER ( Img1, Img2, Img1Regions, Img2Regions, FigureCaption );
        Figs.AllFollowingMatches = gcf;
    else
    DisplayMatches ( Img1, Img2, ...
                     Img1Features (1:2, ...
                     ResultsFIM.AllFollowingMatches(1,:)), ...
                     Img2Features (1:2, ...
                     ResultsFIM.AllFollowingMatches(2,:)), ...
                     FigureCaption );
    Figs.AllFollowingMatches = gcf;
    end
    if Save
        SaveImage ( gcf, gca, WhereToSave, 'AllFollowingMatches', Format );
    end
end

end

% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% END

%% ComposeRegionStructure

function ...
[ Img1Regions, Img2Regions ] = MakeRegStruct ( RegMatches, ResultsFIM )

error ( nargchk ( 2, 2, nargin ) );
error ( nargoutchk ( 2, 2, nargout ) );

Ind1 = RegMatches(1,:);
Ind2 = RegMatches(2,:);

Img1Regions = ResultsFIM.Img1RegionsMSER(Ind1);
Img2Regions = ResultsFIM.Img2RegionsMSER(Ind2);

Img1Features = ResultsFIM.Img1Features(1:2,Ind1);
Img2Features = ResultsFIM.Img2Features(1:2,Ind2);

for i = 1:size(RegMatches,2)
    Img1Regions(i).GravityCenter = transpose(Img1Features(:,i));
    Img2Regions(i).GravityCenter = transpose(Img2Features(:,i));
end

end