%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.es
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.es
%
%  Module        : Plotting the estimated motion between two images.
%
%  File          : PlotMotion.m
%  Date          : 28/08/2006 - 09/13/2006
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written in ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2006 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  PLOTMOTION Plot the Motion between two images.
%
%     PlotMotion ( SrcImg, WrpImg, H )
%
%     Input Parameters:
%      Img1: Source MxN graylevel image.
%      Img2: Warped TxO graylevel image.
%      Homography: 3x3 Planar homography.
%

function PlotMotion ( Img1, Img2, Homography )

  % Check Input Ouput Parameters
  if nargin == 3 && nargout == 0
    % Compute the Sizes
    [h1 w1] = size ( Img1 );
    [h2 w2] = size ( Img2 );
    [hh hw] = size ( Homography );

    % Test the Sizes
    if ( h1 * w1 ) > 0 && ( h2 * w2 ) > 0 && hh == hw && hh == 3 

      % Compute the Un-Warped coordinates
      SrcM = [ 1 1; w2 1; w2 h2; 1 h2 ];
      WrpM = ApplyHomo ( Homography, SrcM' );
      % Transform the Warped Image to the Source one
      T = maketform ( 'projective', SrcM, WrpM' );
      [WiImg, Xd, Yd] = imtransform ( Img2, T, 'nearest', 'FillValues', -1 );

      % Size of the Un-Warped Image
      [hi wi] = size ( WiImg );
      % Compute the biggest size using the Width & Height of the Source Image
      % !!! No se !!!
%      if Xd(1) < 0; Sx = max ( wi, w - Xd(1) ); else Sx = max ( wi + Xd(1), w ); end;
%      if Yd(1) < 0; Sy = max ( hi, h - Yd(1) ); else Sy = max ( hi + Yd(1), h ); end;
      if (Xd(1) + eps) < 1; Sx = max ( wi, w1 - Xd(1) ); else Sx = max ( wi + Xd(1), w1 ); end;
      if (Yd(1) + eps) < 1; Sy = max ( hi, h1 - Yd(1) ); else Sy = max ( hi + Yd(1), h1 ); end;

      % Round the final size
      Sx = ceil ( Sx );
      Sy = ceil ( Sy );

      % When Negative Coords -> Move Source Image, otherwise the Un-Warped
      if round ( Xd(1) ) < 1;
        OffsSrcX = -round ( Xd(1) ) + 1; OffsWrpX = 1;
      else
        OffsSrcX = 1; OffsWrpX = round ( Xd(1) );
      end;
      if round  ( Yd(1) ) < 1;
        OffsSrcY = -round ( Yd(1) ) + 1; OffsWrpY = 1;
      else
        OffsSrcY = 1; OffsWrpY = round ( Yd(1) );
      end;

      % Set the new image to -1 to be able to compute the transparency
      FusedImg = -ones ( Sy, Sx );
      % Write the Un-Warped Image
      FusedImg( OffsSrcY : OffsSrcY+h1-1, OffsSrcX : OffsSrcX+w1-1 ) = Img1;

%      % Overlap with the Source Image
%      FusedImg( OffsWrpY : OffsWrpY+hi-1, OffsWrpX : OffsWrpX+wi-1 ) = ...
%           FusedImg( OffsWrpY : OffsWrpY+hi-1, OffsWrpX : OffsWrpX+wi-1 ) + WiImg;
%
%      % Equalize the Final Image
%      FusedImg = FusedImg - min ( FusedImg(:) );
%      FusedImg = FusedImg / max ( FusedImg(:) );

      % Set the warped image to -1 to be able to compute the transparency
      WarpImg = -ones ( Sy, Sx );
      % Write the Warped Image
      WarpImg( OffsWrpY : OffsWrpY+hi-1, OffsWrpX : OffsWrpX+wi-1 ) = WiImg;

      % Create the final image
      for i = 1 : Sy
        for j = 1 : Sx
          % Take the best possible value
          if ( FusedImg(i, j) == -1 ) && ( WarpImg(i, j) == -1 )
            FusedImg(i, j, 1:3) = 1;
          elseif ( FusedImg(i, j) ~= -1 ) && ( WarpImg(i, j) ~= -1 )
            %FusedImg(i, j, 1:3) = WarpImg(i, j);
            FusedImg(i, j, 1) = FusedImg(i, j);
            FusedImg(i, j, 2) = WarpImg(i, j);
            FusedImg(i, j, 3) = (FusedImg(i, j) + WarpImg(i, j)) / 2;
          elseif ( FusedImg(i, j) == -1 ) && ( WarpImg(i, j) ~= -1 )
            FusedImg(i, j, 1:3) = WarpImg(i, j);
          else
            FusedImg(i, j, 2:3) = FusedImg(i, j);
          end              
        end
      end

      % Plot the Final Image
      figure; imshow ( FusedImg ); hold on;

      % Draw the bounding contours
      line ( [ OffsSrcX OffsSrcX+w1-1 OffsSrcX+w1-1 OffsSrcX OffsSrcX ], ...
             [ OffsSrcY OffsSrcY OffsSrcY+h1-1 OffsSrcY+h1-1 OffsSrcY ], 'Color', 'g' );
      line ( [ OffsWrpX OffsWrpX+wi-1 OffsWrpX+wi-1 OffsWrpX OffsWrpX ], ...
             [ OffsWrpY OffsWrpY OffsWrpY+hi-1 OffsWrpY+hi-1 OffsWrpY ], 'Color', 'r' );

      X = WrpM(1,:) + OffsSrcX - 1; Y = WrpM(2,:) + OffsSrcY - 1;
      line ( [ X, X(1) ], [ Y, Y(1) ], 'Color', 'b' );
%       s = sprintf ( 'Overlapped Source and Un-Warped Images\nHomography estimated using Filtered Matches' );
%       title ( s );
      set ( gcf, 'Name', 'Overlapped Source and Un-Warped Images' );
    else
      error ( 'MATLAB:PlotMotion:InputOutput', 'Incorrect Input matrix sizes!' );
    end
  else
    error ( 'MATLAB:PlotMotion:InputOutput', ...
            'PlotMotion ( Img1, Img2, Homography )' );
  end
end
