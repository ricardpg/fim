%
%  Autor(s)      : Jordi Ferrer Plana, Marina Kudinova
%  e-mail        : {jferrerp,kudinova}@eia.udg.es
%  Branch        : Computer Vision
%  Working Group : Underwater Vision Lab
%  Homepage      : http://porcsenglar.udg.es
%  Module        : Plotting Filtered/Unfiltered matches function.
%  File          : PlotMatchFilter.m
%  Date          : 10/09/2006 - 27/02/2007
%  Compiler      : MATLAB >= 7.0
%  Notes         : File written in ISO-8859-1 encoding.
%
% -------------------------------------------------------------------------
%
%  Copyright (C) 2005-2006 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -------------------------------------------------------------------------
%
%  Usage:
%
%  PLOTMATCHFILTER Plot the matches that passed the filter and the matches
%     that did'nt pass.
%
%     PlotMatchFilter ( SrcImg, SrcFrames, WrpFrames, ...
%                       FrameMatches, FFrameMatches, FigureTitle )
%
%     Input Parameters:
%      SrcImg: Source MxN graylevel or color (3 channels) image.
%      SrcFrames: Set of 5xK frames found in the SrcImg. Each row must hold
%                 the X, Y, Scale, Orientation and Octave data.
%      WrpFrames: Set of 5xL frames found in the WrpImg. Each row must hold
%                 the X, Y, Scale, Orientation and Octave data.
%      FrameMatches: Set of 2xO pairs of indices correspoinding to the L
%                    matches between the SrcFrames Keypoints and the
%                    WrpFrames Keypoints. This set passed the filter.
%      FFrameMatches: Set of 2xO pairs of indices correspoinding to the L
%                    matches between the SrcFrames Keypoints and the
%                    WrpFrames Keypoints. This set didn't pass the filter.
%      FigureTitle: String for the title of the displayed figure.
%
% -------------------------------------------------------------------------

function PlotMatchFilter ( SrcImg, SrcFrames, WrpFrames, ...
                           FrameMatches, FFrameMatches, FigureTitle )

% Check Input Output Parameters
error ( nargchk ( 6, 6, nargin ) );
error ( nargoutchk ( 0, 0, nargout ) );

% Compute the Sizes
[ h, w, nc ] = size( SrcImg );
rSf  = size ( SrcFrames, 1 );
rWf  = size ( WrpFrames, 1 );
rm   = size ( FrameMatches, 1 );
rmf  = size ( FFrameMatches, 1 );

% Test the Sizes
if ( h*w == 0 ) || ( rSf < 2 ) || ( rWf < 2 ) || ...
   ( rm ~= 2 && ~isempty ( FrameMatches ) ) || ...
   ( rmf ~= 2 && ~isempty ( FFrameMatches ) )
    error ( 'MATLAB:PlotMatchFilter:Input', ...
            'Incorrect Input matrix sizes!' );
end

% Plot The Source Image ---------------------------------------------------

iptsetpref('ImshowBorder','tight');

if nc == 1
    Img(:,:,1) = SrcImg; Img(:,:,2) = SrcImg; Img(:,:,3) = SrcImg;
elseif nc == 3
    Img = SrcImg;
else
    error ( 'Matlab:PlotMatchFilter:Input', ...
            'Input Images can have 1 or 3 color channels!' );
end

figure; 
imshow ( Img ); 
hold on;
set ( gcf, 'Name', FigureTitle );

% Plot Accepted Matches ---------------------------------------------------

if ~isempty ( FrameMatches )
    % Plot the frames that passed the filter
    n = size ( FrameMatches, 2 );
    Palette = jet ( n );
    % DarkPal = summer ( n ) / 1.7;

    for i = 1 : n

        line ( [ SrcFrames(1, FrameMatches(1,i)); ...
                 WrpFrames(1, FrameMatches(2,i)) ], ...
               [ SrcFrames(2, FrameMatches(1,i)); ...
                 WrpFrames(2, FrameMatches(2,i)) ], ...
                 'Color', 'g' );

        plot ( SrcFrames(1, FrameMatches(1,i)), ...
               SrcFrames(2, FrameMatches(1,i)), ...
               '.', 'Color', Palette(i,:) );
        % text( SrcFrames(1, FrameMatches(1,i))+3, ...
        %       SrcFrames(2, FrameMatches(1,i))-3, ...
        %       sprintf('%d',i), 'Color', 'r');
        
        plot ( WrpFrames(1, FrameMatches(2,i)), ...
               WrpFrames (2, FrameMatches(2,i)), ...
               '*', 'Color', Palette(i,:) );
        % text( WrpFrames(1, FrameMatches(2,i))+3, ...
        %       WrpFrames(2, FrameMatches(2,i))-3, ...
        %       sprintf('%d',i), 'Color', 'r' );
        
    end
    %        NaNs(1:n) = NaN;
    %        x = [ SrcFrames(1, FrameMatches(1,:)); ...
    %              WrpFrames(1, FrameMatches(2,:)); NaNs ];
    %        y = [ SrcFrames(2, FrameMatches(1,:)); ...
    %              WrpFrames(2, FrameMatches(2,:)); NaNs ];
    %        k = line ( x(:), y(:), 'Color', 'g' );
    %        plot ( SrcFrames(1, FrameMatches(1,:)), ...
    %               SrcFrames(2, FrameMatches(1,:)), 'b*' );
    %        plot ( WrpFrames(1, FrameMatches(2,:)), ...
    %               WrpFrames(2, FrameMatches(2,:)), 'gx');
end

% Plot Rejected Matches ---------------------------------------------------

if ~isempty ( FFrameMatches )
    
    n = size ( FFrameMatches, 2 );
    Palette = jet ( n );
    DarkPal = jet ( n ) / 1.7;
    
    for i = 1 : n
        line ( [ SrcFrames(1, FFrameMatches(1,i)); ...
                 WrpFrames(1, FFrameMatches(2,i)) ], ...
               [ SrcFrames(2, FFrameMatches(1,i)); ...
                 WrpFrames(2, FFrameMatches(2,i)) ], ...
                 'Color', 'r' );
        %        'Color', Palette(i,:) );
        plot ( SrcFrames(1, FFrameMatches(1,i)), ...
               SrcFrames(2, FFrameMatches(1,i)), ...
               '.', 'Color', 'r' );
        %      DarkPal(i,:) );
        plot ( WrpFrames(1, FFrameMatches(2,i)), ...
               WrpFrames(2, FFrameMatches(2,i)), ...
               '*', 'Color', 'r' );
    end

    %        NaNs = [];
    %        NaNs(1:size(FFrameMatches,2)) = NaN;
    %        x = [ SrcFrames(1, FFrameMatches(1,:)); ...
    %              WrpFrames(1, FFrameMatches(2,:)); NaNs ];
    %        y = [ SrcFrames(2, FFrameMatches(1,:)); ...
    %              WrpFrames(2, FFrameMatches(2,:)); NaNs ];
    %        line ( x(:), y(:), 'Color', 'r' );
    %        plot ( SrcFrames(1, FFrameMatches(1,:)), ...
    %               SrcFrames(2, FFrameMatches(1,:)), 'b*' );
    %        plot ( WrpFrames(1, FFrameMatches(2,:)), ...
    %               WrpFrames(2, FFrameMatches(2,:)), 'gx');
end
        