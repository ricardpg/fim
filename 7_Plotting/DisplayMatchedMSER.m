%% DisplayMatchedMSER -----------------------------------------------------
%
%  DisplayMatchedMSER function displays two images where MSER regions were
%  detected beside each other. At each image boundaries of the matched 
%  regions are displayed and gravity centers of these regions. Correponding
%  regions are displayed in the same color and they centers are connected
%  via dotted line of their color if the DisplayLines option is activated,
%  this parameter is optional and by default is ste to 1.
%
%  Author        : Marina Kudinova
%  e-mail        : kudinova@eia.udg.es
%  Working Group : Computer Vision and Robotics Group
%  Homepage      : http://vicorob.udg.es/
%  File          : DisplayMatches.m
%  Date          : 08/03/2007 - 09/03/2007
%  Compiler      : MATLAB >= 7.0
%  
% -------------------------------------------------------------------------
% 
% USAGE: 
%
% DisplayMatchedMSER ( Img1, Img2, Img1AcceptedMSER, Img2AcceptedMSER, ...
%                      FigureTitle, N, Margin, DisplayLines, Color )
%
% INPUT:
%
% Img1, Img2       - Input Images, can be grayscale or color with 3 channels.
% Img1AcceptedMSER - Structures containing only Matched MSERs of the Img1 
% Img2AcceptedMSER   and Img2 correspondingly. The field "Boundary" of each 
%                    structure should contain global indices of the region
%                    boundary relative to Img1 and Img2. Number of the 
%                    matched boundaries should correspond in both
%                    structures. The field "GravityCenter" of each
%                    structure contains [x;y] coordinates of the gravity 
%                    center of each region.
% FigureTitle      - Title of the Figure to be displayed, has to be a 
%                    string.
% 
% Optional Parameters:
%
% Can be omitted all or specified all, to use default value for any
% optional parameter, set it to ''.
%
% N            - Number of matches to display.   
% Margin       - Margin between images.
% DisplayLines - If 1 (Default), the lines connecting the gravity centers
%                of the regions are displayed, if 0 - not displayed.
% Color        - color of the connecting lines and keypoints markers, the 
%                only accepted format is RGB triple, e.g. [0.1, 0.4, 0.5].
%
% -------------------------------------------------------------------------

%%
function ...
DisplayMatchedMSER ( Img1, Img2, Img1AcceptedMSER, Img2AcceptedMSER, ...
                     FigureTitle, N, Margin, DisplayLines, Color )

%% Parameters check

error ( nargchk ( 5, 9, nargin ) );

[ h, w1, nc ] = size(Img1);
[ h2, w2, nc2 ] = size(Img2);
Nmatches = size(Img1AcceptedMSER,2);
Nmatches2 = size(Img2AcceptedMSER,2);

if sum ( abs ( [h,w1,nc] - [h2,w2,nc2] ) ) ~= 0
    error ( 'MATLAB:DisplayMatches:Input', ...
            'Incorrect Input Images sizes!' );        
end

if sum ( abs ( Nmatches - Nmatches2 ) ) ~= 0
    error ( 'MATLAB:DisplayMatches:Input', ...
            'Incorrect Input Matches Matrices sizes!' );        
end

% Default parameters when optional are omitted

if nargin == 5
    N = Nmatches;
    Margin = 3;
    colors = colormap ( jet (N) );
    Color = [];
    DisplayLines = 1;
else
    if strcmpi( N, '') || ~isnumeric(N) ||  N>Nmatches 
        warning ( 'MATLAB:DisplayMatches:Input', ...
                  'Incorrect Number of Matches to display!' );
        N = Nmatches;
    end
    if strcmpi( Margin, '')
        Margin = 3;
    end
    if size(Color,2)~=3 || ...
       ( isnumeric(Color) && ( any(Color>1) || any(Color<0) ) )
        warning ( 'MATLAB:DisplayMatches:Input', ...
                [ 'Last input parameter can only be ', ...
                  'RGB triple [0.4,0.2,0.1]!\nOptional parameter!' ] );
        colors = colormap ( jet (N) );
        Color = [];        
    end
    if strcmpi( DisplayLines, '') || ~isnumeric(DisplayLines)
        DisplayLines = 1;
    end
end

%% Composing big image

w = w1 + w2 + Margin;
ResImg = zeros ( h, w, nc );
ResImg ( 1:h, 1:w1, : ) = Img1;
ResImg ( 1:h, w1 + Margin + 1 : end, : ) = Img2;
ResImg = mat2gray ( ResImg );

if nc == 1
    Img = ResImg;
    clear ResImg;
    ResImg = zeros ( h, w, 3 );
    ResImg(:,:,1) = Img;
    ResImg(:,:,2) = Img;
    ResImg(:,:,3) = Img;
end

ResImg ( 1:h, w1 + 1 : w1 + Margin, : ) = 1;
ResImg1 = ResImg(:,:,1);
ResImg2 = ResImg(:,:,2);
ResImg3 = ResImg(:,:,3);

%% Adding Boundaries to images

for i = 1 : N
    
    if ~isempty(Color)
        c = Color;
    else
        c = colors ( i, : );
    end
    
    % Boundary in the Image1 and Image2 for one matched regions pair
    Boundary = [ Img1AcceptedMSER(i).Boundary; ...
                 Img2AcceptedMSER(i).Boundary + h*w1 + h*Margin ];
        
    ResImg1(Boundary) = c(1);
    ResImg2(Boundary) = c(2);
    ResImg3(Boundary) = c(3);
        
end

%% Display

% Image

ResImg(:,:,1) = ResImg1;
ResImg(:,:,2) = ResImg2;
ResImg(:,:,3) = ResImg3;

iptsetpref('ImshowBorder','tight');
imshow ( ResImg );
hold on;

% Matches -----------------------------------------------------------------

for i = 1 : N
    
    if ~isempty(Color)
        c = Color;
    else
        c = colors ( i, : );
    end
    
    if DisplayLines
        line ( [ Img1AcceptedMSER(i).GravityCenter(1); ...
                 Img2AcceptedMSER(i).GravityCenter(1) + w1 + Margin ], ...
               [ Img1AcceptedMSER(i).GravityCenter(2); ...
                 Img2AcceptedMSER(i).GravityCenter(2) ], ...
                 'LineStyle', '-', 'Marker', 'o', ...
                 'MarkerSize', 3, 'Color', c );
    end
       
    plot ( [ Img1AcceptedMSER(i).GravityCenter(1); ...
             Img2AcceptedMSER(i).GravityCenter(1) + w1 + Margin ], ...
           [ Img1AcceptedMSER(i).GravityCenter(2); ...
             Img2AcceptedMSER(i).GravityCenter(2) ], ...
             'LineStyle', 'none', 'Marker', '+', ...
             'MarkerSize', 3, 'MarkerEdgeColor', c );
       
end

% Figure Name -------------------------------------------------------------

if strcmpi ( FigureTitle, '' )
    set ( gcf, 'Name', 'Matched MSER' );
else
    set ( gcf, 'Name', FigureTitle );
end
