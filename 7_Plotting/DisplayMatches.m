%% DisplayMatches ---------------------------------------------------------
%
%  DisplayMatches function displays two matched images beside each other
%  and shows the corresponding features by displaying the connecting them 
%  lines.
%
%  Author        : Marina Kudinova
%  e-mail        : kudinova@eia.udg.es
%  Working Group : Computer Vision and Robotics Group
%  Homepage      : http://vicorob.udg.es/
%  File          : DisplayMatches.m
%  Date          : 22/02/2007 - 09/03/2007
%  Compiler      : MATLAB >= 7.0
%  
% -------------------------------------------------------------------------
% 
% USAGE: 
%
% DisplayMatches ( Img1, Img2, Img1Matches, Img2Matches, FigureTitle, ...
%                  N, Margin, Color )
%
% INPUT:
%
% Img1, Img2   - Input Images, can be grayscale or color with 3 channels.
% Img1Matches  - Matched Keypoints of the Img1 and Img2, row1: X coords, 
% Img2Matches    row2: Y coords. The match to the Img1Matches(:,j) has to 
%                be in the Img2Matches(:,j).
% FigureTitle  - Title of the Figure to be displayed, has to be a string.
% 
% Optional Parameters:
%
% Can be omitted all or specified all, to use default value for any
% optional parameter, set it to ''.
%
% N       - Number of matches to display.   
% Margin  - Margin between images.
% Color   - color of the connecting lines and keypoints markers, can be the
%           name of the fixed Matlab color ('r' for example) or RGB triple 
%           [0.1, 0.4, 0.5]
%
% -------------------------------------------------------------------------

%%
function ...
DisplayMatches ( Img1, Img2, Img1Matches, Img2Matches, FigureTitle, ...
                 N, Margin, Color )

%% Parameters check

error ( nargchk ( 5, 8, nargin ) );

[ h, w1, nc ] = size(Img1);
[ h2, w2, nc2 ] = size(Img2);
Nmatches = size(Img1Matches,2);
Nmatches2 = size(Img2Matches,2);

if sum ( abs ( [h,w1,nc] - [h2,w2,nc2] ) ) ~= 0
    error ( 'MATLAB:DisplayMatches:Input', ...
            'Incorrect Input Images sizes!' );        
end

if sum ( abs ( Nmatches - Nmatches2 ) ) ~= 0
    error ( 'MATLAB:DisplayMatches:Input', ...
            'Incorrect Input Matches Matrices sizes!' );        
end

% Default parameters when optional are omitted

if nargin == 5
    N = size ( Img1Matches, 2 );
    Margin = 3;
    colors = colormap ( jet (N) );
    Color = [];
else
    if strcmpi( N, '') || ~isnumeric(N) ||  N>Nmatches 
        warning ( 'MATLAB:DisplayMatches:Input', ...
                  'Incorrect Number of Matches to display!' );
        N = Nmatches;
    end
    if strcmpi( Margin, '')
        Margin = 3;
    end
    if ( ~ischar(Color) && size(Color,2)~=3 ) || ...
       ( ischar(Color) && isempty(strfind('ymcrgbwk',lower(Color))) ) ||...
       ( isnumeric(Color) && ( any(Color>1) || any(Color<0) ) )
        warning ( 'MATLAB:DisplayMatches:Input', ...
                [ 'Last input parameter can be Fixed Color name ', ...
                  ' (''r'') or RGB triple [0.4,0.2,0.1]!' ] );
        colors = colormap ( jet (N) );
        Color = [];        
    end
end

%% Composing big image

w = w1 + w2 + Margin;
ResImg = zeros ( h, w, nc );
ResImg ( 1:h, 1:w1, : ) = Img1;
ResImg ( 1:h, w1 + Margin + 1 : end, : ) = Img2;
ResImg = mat2gray ( ResImg );

if nc == 1
    Img = ResImg;
    clear ResImg;
    ResImg = zeros ( h, w, 3 );
    ResImg(:,:,1) = Img;
    ResImg(:,:,2) = Img;
    ResImg(:,:,3) = Img;
end

ResImg ( 1:h, w1 + 1 : w1 + Margin, : ) = 1;

%% Display

% Image

iptsetpref('ImshowBorder','tight');
imshow ( ResImg );
hold on;

% Matches -----------------------------------------------------------------

for i = 1 : N
    
    if ~isempty(Color)
        c = Color;
    else
        c = colors ( i, : );
    end
    
    line ( [ Img1Matches(1, i); Img2Matches(1, i) + w1 + Margin ], ...
           [ Img1Matches(2, i); Img2Matches(2, i) ], ...
           'Marker', '+', 'MarkerSize', 3, 'Color', c );
       
    plot ( [ Img1Matches(1, i), Img2Matches(1,i) + w1 + Margin ], ...
           [ Img1Matches(2, i), Img2Matches(2,i) ], ...
           'LineStyle', 'none', 'Marker', 'o', ...
           'MarkerSize', 3, 'MarkerEdgeColor', c );
       
end

% Figure Name -------------------------------------------------------------

if strcmpi ( FigureTitle, '' )
    set ( gcf, 'Name', 'Matches' );
else
    set ( gcf, 'Name', FigureTitle );
end
