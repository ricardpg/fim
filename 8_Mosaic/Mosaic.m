clear; close all;

addpath ./0_3rdParty/sift ./1_Preprocessing ./2_Detection ...
        ./3_Description ./4_Matching ./5_FIM ./6_FIMtests ...
        ./7_Plotting ./8_Mosaic -END;

M = 15;         % Number of Images

% Read and Store the set of images
j = 1111;
for i = 1 : M
  % Source Image
  I = imread ( sprintf ( '../Data/Seq/imgl0%4.4d.jpg', j ) );
  % I = imread ( 'Data/image2.png' );
  [h w d] = size ( I );
  if d > 1; Img = rgb2gray ( I ); else Img = I; end;

  % Convert to Double and Equalize
  Img = double ( Img );
  Img = Img - min ( Img(:) );
  Img = Img / max ( Img(:) );

  % Store in the sequence
  SrcImg(:,:,i)  = Img;
  
  j = j + 5;
end

% ~~~~~~~~~~~ Image Processing

% SingleChannel = { 0 (image has 1 channel), 1 (Default:Grayscale),
%                   2 (Grayscale), 3 (Red channel only), 4-Green, 5-Blue }
ProcessingParameters.SingleChannel = 1; % getting the single-channel image
% Equalization = { 0 -> No equalization, 1 -> Default:Full, 
%                  2 -> Full, 3 -> Clahe }
ProcessingParameters.Equalization = 1;

%ProcessingParameters = 1;   % Default processing: (grayscaling + full equalization)
%ProcessingParameters = 0;   % Do NOT perform preprocessing

% ~~~~~~~~~~~ Detector

Detectors = { 'Sift', 'Harris', 'Laplacian', 'Hessian' };
Detector = Detectors(1);
MaxNumber = 100;           % max number of features to detect
%Mask = imcrop(ceil(fspecial('disk',max(h,w))), [0 0 w h]);
Mask = ones ( h, w );
RadiusNonMaximal = 10;    % radius for non-maximal suppression for detector

% ~~~~~~~~~~~ Descriptor

Descriptors = { 'Sift' }; Descriptor = Descriptors(1);

% ~~~~~~~~~~~ Matcher

Matchers = { 'Sift', 'ICNN' };
Matcher = Matchers(1);
MatchingParameter = 1.5;     % SIFT: ratio of distances, 1.5 - default
%MatchingParameter = 0.0007;  % ICNN: variance of vectors, 0.0001 - default 

% ~~~~~~~~~~ RANSAC

HomographyModels = { 'Projective', 'Affine', 'Euclidean', 'Similarity', 'None' };
HomographyModel = HomographyModels(3);
StandardDeviation = 1;     % stdev of spatial coord of features, in pixels

% Source Image Position
H(:,:,1) = [ 1 0 0; 0 1 0; 0 0 1];
for i = 1 : M - 1
  tic;

  fprintf ( 'Image %d\n', uint32 ( i ) );
  Img1Features = [];
  Img2Features = [];
  InitialMatches = [];
  RejectedMatches = [];
  RansacMatches = [];
%  [ Im1Feat, Im2Feat, Matches, H(:,:,i+1) ] = FIM ( SrcImg(:,:,i), ...
%                                                    SrcImg(:,:,i), ...
%                                                    N, 'Harris', 'Sift', 'ICNN' );

% ~~~~~~~~~~~ RUNNING FIM ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

[ Img1, Img2, Img1Features, Img2Features, ...
  InitialMatches, RejectedMatches, RansacMatches, ...
  H(:,:,i+1) ] = ...                        
  FIM ( SrcImg(:,:,i), SrcImg(:,:,i), ...
        ProcessingParameters, ...
        Detector, MaxNumber, Mask, RadiusNonMaximal, ...
        Descriptor, ...
        Matcher, MatchingParameter, ...
        HomographyModel, StandardDeviation );

  toc
end

H(:,:,1) = [ 1 0 1200; 0 1 1200; 0 0 1];
%PlotMosaic ( SrcImg, H, 800, 1000, 'Results/HarrisICNN', 'Mosaic' );
PlotMosaic ( SrcImg, H, 2400, 2400, 'n:\FIM\Mosaic', 'test' );

