
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%  MosaicFIM - Construction of Mosaic using FIM
%
%  Author(s)     : Marina Kudinova, Jordi Ferrer, Ricard Prados
%  e-mail        : { kudinova, jferrerp, rprados } @ eia.udg.es
%  Working Group : Computer Vision and Robotics Group
%  Homepage      : http://vicorob.udg.es/
%  Project       : FIM
%  File          : MosaicFIM.m
%  Date          : 25/09/2006 - 26/06/2007
%  Compiler      : MATLAB >= 7.0
%
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function MosaicFIM ( MosaicParameters, InputFIM )

error ( nargchk ( 2, 2, nargin ) );

% ~~~~~~~~~~ Parameters of Mosaic

Name             = MosaicParameters.Name; % => MosaicName
jmp              = MosaicParameters.Jump;
SeqLength        = MosaicParameters.SeqLength;
j                = MosaicParameters.FirstImageNumber;  % for reading the image data
PathToImages     = MosaicParameters.PathToImages;
DisplayStat      = MosaicParameters.DisplayStat;
SaveMosaic       = MosaicParameters.SaveMosaic;
WhereSaveResults = MosaicParameters.WhereSaveResults;

if isfield(MosaicParameters,'NumberIms') && ...
        isscalar(MosaicParameters.NumberIms);
    NumberIms = MosaicParameters.NumberIms; %fix ( SeqLength / jmp );
else
    NumberIms = fix ( SeqLength / jmp );
end

% ~~~~~~~~~~~ Checking Parameters of FIM and displaying its configuration

% Display parameters
InputFIM.Display.DisplayConfig  = 1;
InputFIM.Display.DisplayResults = 1;
InputFIM.Display.SepLines       = 0;

% Images and Mask
Image1 = imread ( sprintf ( PathToImages, j ) );
Image2 = imread ( sprintf ( PathToImages, j ) );
Mask   = [];

%Checking FIM input
InFIM = CheckInputParameters ( Image1, Image2, Mask, InputFIM );

if DisplayStat
    fprintf ( [ '\n-- FIM Mosaic ', repmat('-',1,58), '\n' ] );
    DisplayConfiguration ( InFIM );
    fprintf ('Mosaic Length:      %d Images\n', NumberIms );
    fprintf ('\n');
end
InFIM.Display.DisplayConfig = 0;

% ~~~~~~~~~~~ RUNNING FIM ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

iptsetpref('ImshowBorder','tight');

% Initialitze sequence of images and homographies
I1 = imread ( sprintf ( PathToImages, j ) );
ResizeCoeff = InFIM.Processing.ResizeCoeff;
if ResizeCoeff ~= 1
    I1 = imresize ( I1, 1/ResizeCoeff );
end
% Rotating the image if necessary
% I1 = imrotate(I1, -90);

% % Saving the first image
% imshow(I1);
% %SaveImage ( gcf, gca, WhereSaveResults, sprintf('Image%d',j), 'png' );
% saveas ( gcf, [ WhereSaveResults sprintf('Image%d',j) '.eps' ], 'psc2');

[ h, w, d]  = size ( I1 );
%ImagesProc = zeros ( h, w, NumberIms );
Images = zeros ( h, w, d, NumberIms );
H = zeros ( 3, 3, NumberIms );
clear I1;

for i = 2 : NumberIms

    tic;

    fprintf ( [ repmat('-',1,72), '\n', '\nIMAGE %d' ], uint32 ( i ) );
    fprintf ( ' to IMAGE %d :\n', uint32 ( i-1 ) );

    I1 = imread ( sprintf ( PathToImages, j ) );
    %     I1 = imrotate(I1, -90);
    j = j + jmp;
    I2 = imread ( sprintf ( PathToImages, j ) );
    %     I2 = imrotate(I2, -90);
    %     imshow(I2);
    %
    %     % Saving the second image
    %     % SaveImage ( gcf, gca, WhereSaveResults, sprintf('Image%d',j), 'png' );
    %     saveas ( gcf, [ WhereSaveResults sprintf('Image%d',j) '.eps' ], 'psc2');

    %     h = size ( I1, 1 );
    %     w = size ( I1, 2 );
    %     Mask = [];

    if InFIM.Save.Results || InFIM.Save.HomoInASCII
        InFIM.Save.TestName = [ '_' num2str(i-1) ];
    end

    [ Img1, Img2, ResultsFIM ] = FIM ( I1, I2, Mask, InFIM );

    % If processed images were resized by FIM
    if ResizeCoeff ~= 1
        I1 = imresize ( I1, 1/ResizeCoeff );
        I2 = imresize ( I2, 1/ResizeCoeff );
    end

    % If processed images were cropped (CLAHS) by FIM
    if all ( size(I1(:,:,1)) ~= size(Img1(:,:,1)) )
        I1 = imcrop ( I1,[ 0, 0, size(Img1,2), size(Img1,1) ] );
        I2 = imcrop ( I2,[ 0, 0, size(Img2,2), size(Img2,1) ] );
    end

    if i == 2;
        % ImagesProc(:,:,i-1)  = Img1;
        Images(:,:,:,i-1)  = I1;
        H(:,:,1) = eye(3);
    end

    % ImagesProc(:,:,i)  = Img2;
    Images(:,:,:,i)  = I2;
    if isfield ( ResultsFIM, 'Homography' )
        H(:,:,i) = ResultsFIM.Homography;
    else
        H(:,:,i) = zeros(3);
    end
    Configuration = ResultsFIM.Configuration;
    clear ResultsFIM Img1 Img2

    fprintf('\n');
    T = toc;
    format short;
    fprintf('FIM execution Time in sec   =  %-4.2f', T );
    fprintf('\n\n');

end

if sum(sum(abs(H(:,:,2)))) == 0;
    fprintf( '\n!!! FIM was not able to construct MOSAIC !!!' );
    fprintf( [ '\n', repmat('-',1,72), '\n' ] );
    return;
end;

[x1, y1, Mw, Mh] = CalcMosaicSize ( Images, H );
m = mod ( Mw, 4 );
if m, Mw = Mw + 4 - m; end

Mw = Mw + 4;
Mh = Mh + 4;
x1 = x1 + 2;
y1 = y1 + 2;

H(:,:,1) = [ 1 0 x1; 0 1 y1; 0 0 1];

fprintf( [ repmat('-',1,72), '\n' ] );

PlotMosaic ( Images, H, Mw, Mh, strcat('Mosaic',Name), strcat('Mosaic',Name ));

% Saving FIM configuration used to build the mosaic, Mosaic itself and
% Homographies between image pairs

if SaveMosaic
    MosaicFilename = [ WhereSaveResults, 'Mosaic', Name ];
    save ( [ MosaicFilename,'Homographies' ], 'H' );
    %save ( [ MosaicFilename,'Images' ], 'Images' );
    save ( [ MosaicFilename,'ConfigurationFIM' ], 'Configuration' );
    %saveas ( gcf, MosaicFilename, 'jpg' );
    saveas ( gcf, [ MosaicFilename '.eps' ], 'psc2');
end
fprintf ( '\n' );

% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% END
