
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%  Calulation of the Size of the Mosaic to be built
%
%  Author(s)     : Marina Kudinova, Jordi Ferrer, Ricard Prados
%  e-mail        : { kudinova, jferrerp, rprados } @ eia.udg.es
%
%  Working Group : Computer Vision and Robotics Group
%  Homepage      : http://vicorob.udg.es/
%
%  Project       : FIM
%  File          : CalcMosaicSize.m
%  Date          : 25/09/2006 - 26/09/2006
%  Compiler      : MATLAB >= 7.0
%
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function [x1, y1, Mw, Mh] = CalcMosaicSize ( Images, H )

    %AccH(:,:,1) = H(:,:,1);
    AccH = eye(3);
    Xcoord = [];
    Ycoord = [];

    AccH = eye ( 3 );
    %H(:,:,1) = AccH;
    Left = 0;
    Top = 0;
    [ Bottom, Right ] = size ( Images ( :, :, 1 ) );
    n = size ( H, 3 );
    for i = 1 : n
        AccH = AccH * H(:,:,i);
        [h w] = size ( Images ( :, :, 1 ) );
        ImCornProj = AccH * [ 0, w, 0, w;  0, 0, h, h;  1, 1, 1, 1 ];
        XCoord = ImCornProj(1,1:4) ./ ImCornProj(3,1:4);
        YCoord = ImCornProj(2,1:4) ./ ImCornProj(3,1:4);
        x1 = min ( XCoord );
        x2 = max ( XCoord );
        y1 = min ( YCoord );
        y2 = max ( YCoord );
        if x1 < Left, Left = x1; end
        if x2 > Right, Right = x2; end
        if y1 < Top, Top = y1; end
        if y2 > Bottom, Bottom = y2; end
    end

    % Compute Size
    Mw = ceil ( Right - Left + 1 );
    Mh = ceil ( Bottom - Top + 1 );

    % Compute
    if Left < 0, x1 = ceil ( abs ( Left ) ); else x1 = 0; end;
    if Top < 0, y1 = ceil ( abs ( Top ) ); else y1 = 0; end;
end
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% END
