
% RunMosaicFIM ------------------------------------------------------------
%  
%  Running the Construction of Mosaic using FIM with specified below 
%  parameters.
%
%  Author(s)     : Marina Kudinova, Jordi Ferrer, Ricard Prados
%  e-mail        : { kudinova, jferrerp, rprados } @ eia.udg.es
%  Working Group : Computer Vision and Robotics Group
%  Homepage      : http://vicorob.udg.es/
%  Project       : FIM
%  File          : RunMosaicFIM.m
%  Date          : 25/09/2006 - 26/06/2007
%  Compiler      : MATLAB > 7.0
%
% -------------------------------------------------------------------------

clear; close all; clc; warning off all;

%% Adding Pathes

PathToFIM         =  './';                            % from current folder
PathToData        =  './Images/';                     % from current folder
WhereSaveResults  =  './ResultsFIM/';                 % from current folder
ImageNamePattern  =  'img%1.1d.ppm';                  % Example: Trees2345.GIF -> pattern will be: 'Trees%4.4d.GIF', if only 2 last number in the image name change you can write: 'Trees23%2.2d.GIF'

addpath ( PathToFIM );                                % path to the SetPathsFIM function
SetPathsFIM ( PathToFIM );                            % adding all the paths
PathToImages = [ PathToData, ImageNamePattern ];

%% =================== Parameters of Mosaic ===============================

MosaicParameters.Name              = 'Graf';          % used only to Save the results on disk under the name: "MosaicnName"
MosaicParameters.Jump              = 1;               % to jump over several images each step when processing the sequence
MosaicParameters.SeqLength         = 5;               % Number of all images in the PathToImages folder
MosaicParameters.FirstImageNumber  = 1;
MosaicParameters.PathToImages      = PathToImages;
MosaicParameters.DisplayMosaic     = 1;
MosaicParameters.SaveMosaic        = 1;
MosaicParameters.WhereSaveResults  = WhereSaveResults;
MosaicParameters.DisplayStat       = 1;               % to display when procesing the sequence FIM results for every image pair
MosaicParameters.NumberIms         = 5;               % use this parameter for testing, if it is commented the SeqLength is used

% Create the Directory with Results
if MosaicParameters.SaveMosaic; mkdir ( WhereSaveResults ); end

%% ====================== FIM Parameters ==================================

% Image Processing Parameters ---------------------------------------------

InputFIM.Processing.SingleChannel = 4;                % 0-no, 1-red, 2-green, 3-blue, 4-Y channel of YIQ, 5-PCA
InputFIM.Processing.Equalization  = 2;                % 0-no, 1-normalization, 2-equalization, 3-CLAHE
InputFIM.Processing.ResizeCoeff   = 1;                % resize coefficient to decrease sizes of images

% Detection & Description Parameters --------------------------------------

Detectors   = { 'SIFT',   'SURF', ...                 % 1 - 2  <- scale-invariant detectors
                'Harris', 'Hessian', 'Laplacian', ... % 3 - 5  <- non-scaleinvariant detectors
                'MSER' };                             % 6      <- region detector

Descriptors = { 'SIFT', 'SURF', 'SURF-128', ...       % 1 - 3
                'SURF-36', 'U-SURF', ...              % 4 - 5
                'U-SURF-128', 'U-SURF-36', ...        % 6 - 7
                'ImagePatch', 'RotatedImagePatch' };  % 8 - 9      

InputFIM.Detection.DetType          = Detectors(2);
InputFIM.Detection.MaxNumber        = 100000;         % max number of features to detect
InputFIM.Detection.RadiusNonMaximal = 8;              % radius for non-maximal suppression for detector
InputFIM.Detection.Sigma            = 4;              % sigma of the Gaussian used in Harris, Laplacian and Hessian detectors; If = 0, no smoothing is performed
InputFIM.Detection.MinCornerness    = 0;              % [0-1] cornerness threshold
InputFIM.Detection.MinArea          = 5;              % [30] min region area in pix for region detectors
InputFIM.Detection.DescType         = Descriptors(2);            

InputFIM.Description.DescType       = InputFIM.Detection.DescType;
InputFIM.Description.PatchRadius    = 14;             % radius of the image patch

% Matching Parameters -----------------------------------------------------

Matchers = { 'DistRatio', 'Correlation' };

InputFIM.Matching.Matcher               = Matchers(1);
InputFIM.Matching.RatioValue            = 1.5;        % ratio between the next closest and closest nearest neighbour for SIFT matching ; the bigger is the ratio, the smaller is the amount of matches
InputFIM.Matching.CorrelationThreshold  = 0.9;
InputFIM.Matching.Bidirectional         = 0;          % If ~= 0 Intersection between "image1 to image2" and "image2 to image1" matches
InputFIM.Matching.GuidedPostMatching    = 0;
 
% Motion Estimation Parameters --------------------------------------------

HomographyModels  = { 'Euclidean', 'Similarity', 'Affine', ...   % 1 - 3
                      'Projective', 'ProjectiveNLM'  };          % 4 - 5

InputFIM.MotionEstimation.HomographyModel = HomographyModels(5);
InputFIM.MotionEstimation.SpatialStdev    = 8;        % stdev of spatial coord of features, in pixels

% Save Parameters ---------------------------------------------------------

InputFIM.Save.Results     = 0;                        % 1 - save; 0 - do not save
InputFIM.Save.HomoInASCII = 0;                        % 1 - save; 0 - do not save
InputFIM.Save.SaveTo      = WhereSaveResults;
% InputFIM.Save = 0;

%% ====================== RUNNING MosaicFIM ===============================

MosaicFIM ( MosaicParameters, InputFIM );

% =========================================================================
% END
