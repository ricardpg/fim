
%% Compile 
%
%  Autor(s)      : Jordi Ferrer Plana, Kudinova Marina
%  e-mail        : {jferrerp,kudinova}@eia.udg.es
%  Branch        : Computer Vision
%  Working Group : Underwater Vision Lab
%  Homepage      : http://porcsenglar.udg.es
%  Module        : Main
%  File          : Compile.m
%  Date          : 26/01/2007 - 21/06/2007
%  Compiler      : MATLAB >= 7.0
%
%  Notes         : - File written using ISO-8859-1 encoding.
%                  - This script should be run in Matlab from the FIM/
%                    folder.
%                  - After compilation dynamic libraries (i.e. surfWINDLL,
%                    libSurf.o) must be placed in the right Operative 
%                    System dynamic library path.
%                  - In Windows, the script is tested using Visual Studio 7
%                                or 8 Compiler.
%                  - In Linux, the script is tested using g++ 4.x.
%                  - MSER executable (i.e. mser.exe, mser.ln) must be
%                    placed in the right Operative System library path.
%
% -------------------------------------------------------------------------
%
%  Copyright (C) 2005-2006 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -------------------------------------------------------------------------
function Compile

% Detect Platform
Arch = computer;
% Arch = Arch(1:5);

if strcmpi ( Arch, 'pcwin' ); Windows = 1; Nbit = 32;
elseif strcmpi ( Arch, 'pcwin64' ); Windows = 1; Nbit = 64;
else Windows = 0; 
end

% Define right Mex Output Option
if Windows; Output = ' -output '; else Output = ' -o '; end

% List of Sift Files
SiftFiles = { 'imsmooth'; 'siftdescriptor'; 'siftlocalmax'; ...
              'siftmatch'; 'siftormx'; 'siftrefinemx' };
% Sift Subfolder
if ~Windows || Nbit == 32,
  SiftFolder = '0_3rdParty/sift-0.9.8/'; % --> (Ricard Campos) Original FIM version, works on win32
else
  SiftFolder = '0_3rdParty/sift-0.9.19/'; % --> (Ricard Campos) Works on win64
end

% Macro for Windows
if Windows,
  % Change '\' to '/'.
  MatlabPath = strrep ( matlabroot, '\', '/' );
  
  % IMPORTANT:  if you have problems when compiling "siftrefinemx.c":
  % The path '/extern/lib/win64/microsoft" -llibmwlapack ' should be
  % changed according to "win32" or "win64" and actual path to the LAPACK
  % library.
  % If the name of the file is "libmwlapack.lib"  -> 
  % in the path should be "-llibmwlapack"
  
  DMacro = ['-DWINDOWS -I' SiftFolder ' -L"' MatlabPath ...
            '/extern/lib/win' num2str(Nbit) '/microsoft" -llibmwlapack ']; 
        
else
  DMacro = '-llapack ';
end

SepLength = 100;
CapAndLine ('FIM Compilation','=', 2, SepLength );
fprintf ( 'To automatically compile 3rdParty Software:\n' );
fprintf ( '\n1. You must be in the root of the FIM folder (~FIM/)!' );

if Windows,
  fprintf (  '\n2. You must have ''Visual Studio 7 or 8'' compiler!' );
  fprintf ( ['\n   If Visual Studio is installed, use ''mex -setup''\n',...
               '   in the Matlab command window to activate it!'] );
else
  fprintf ( '\n2. You must have "g++ 4.x" compiler or higher!' );
end
fprintf ( '\n\nPress ''ENTER'' to continue or ''CTRL+C'' to abort ... \n' );
pause;

%% FIM compilation

CapAndLine ('FIM mex functions','-', 2, SepLength );
fprintf ( 'Compilation ... \n' )

% In Matching part --------------------------------------------------------

FIMfolder = '4_Matching/';
Cmd = [ 'mex ' FIMfolder 'correl' '.c' Output FIMfolder 'correl' ];
disp ( sprintf ( Cmd ) );
eval ( Cmd );

% In FIM part -------------------------------------------------------------

FIMfolder = '6_FIM/';
Cmd = [ 'mex ' FIMfolder 'mosaic_nodes_overlap' '.c' Output FIMfolder ...
        'mosaic_nodes_overlap' ];
disp ( sprintf ( Cmd ) );
eval ( Cmd );

fprintf ( '... compilation is finished.\n' );

%% SIFT compilation

CapAndLine ('SIFT','-', 2, SepLength );
fprintf ( 'Compilation ... \n' )

for i = 1 : size ( SiftFiles ),
  Cmd = [ 'mex ' DMacro SiftFolder SiftFiles{i} '.c' Output ...
          SiftFolder SiftFiles{i} ];
  disp ( sprintf ( Cmd ) );
  eval ( Cmd );
end
fprintf ( '... SIFT compilation is finished.\n' );

%% Surf Subfolder

SurfFolder = '0_3rdParty/surf';
% Detectors Subfolder
DetFolder = '2_Detection';
% Descriptors Subfolder
DescFolder = '3_Description';
SurfFiles = { [SurfFolder '/SurfMx']; [DetFolder '/SurfFeaturesMx']; ...
              [DetFolder  '/SurfOrientationMx']; ...
              [DescFolder '/SurfDescriptorMx'] };
% Surf library name
if Windows,
  SurfLib = 'surfWINDLL';
  SurfLibExt = '.dll';
else
  SurfLib = 'Surf';
  SurfLibExt = '.so';
end

%% SURF compilation

if ~Windows || Nbit == 32, % Surf does not work on Windows 64...
    CapAndLine ('SURF','-', 2, SepLength );
    fprintf ( 'Compilation ... \n' )

    for i = 1 : size ( SurfFiles ),
      Cmd = ['mex -I' SurfFolder ' ' SurfFiles{i} '.cpp -L' ...
             SurfFolder ' -l' SurfLib Output SurfFiles{i} ];
      disp ( sprintf ( Cmd ) );
      eval ( Cmd );
    end

    fprintf ( '... SURF compilation is finished.\n' );
    fprintf ( '\n!!! IMPORTANT !!!\n' );
    fprintf ( [ '\nPlease copy to one of your SYSTEM Path Folders the ', ...
                'dynamic library ''' SurfLib SurfLibExt ''' from:\n' ] );
    fprintf ( [ '\nFIM/' SurfFolder '/' SurfLib SurfLibExt '\n' ] );
end

%% MSER executable --------------------------------------------------------

CapAndLine ('MSER','-', 2, SepLength );

PathToMSER = [ pwd '/0_3rdParty/mser' ];

if Windows;
    MSERexecutable = 'mser.exe';
    PathToMSER = strrep ( PathToMSER, '\', '/');
else 
    MSERexecutable = 'mser.ln';
    [Status, Str] = system ( [ 'chmod 755 ' PathToMSER '/' MSERexecutable ] );
    if Status == 0;
      disp ( [ MSERexecutable ' binary permissions changed to executable!' ] );
    else
      disp ( [ MSERexecutable ' binary permissions couldn''t be changed to executable! The message was:' ] )
      disp ( Str );
      disp ( 'Please, do it manually!' );
    end
    fprintf ( '\n' );
end

fprintf ( '!!! IMPORTANT !!!\n' );

fprintf ( [ '\nPlease ADD the displayed below path to your SYSTEM ', ...
            'paths OR COPY ''' MSERexecutable '''\n', ...
            'executable from this folder ' ... 
            'to one of your SYSTEM Path Folders.\n\n' ] );
      
disp ( PathToMSER );

%% Restarting Matlab ------------------------------------------------------

CapAndLine ('ATTENTION','-', 2, SepLength );

fprintf ( '!!! PLEASE, RESTART MATLAB WHEN COMPILATION IS FINISHED!!!\n' );


%% SYSTEM and USER Path Folders

if Windows
    CapAndLine ( 'Path Folders (EXE & DLL)','-', 2, SepLength );
    fprintf ( 'Press ''ENTER'' to display ...\n\n' );
    pause;
    [Status, LDPath] = system ( 'echo %PATH%' );
else
    CapAndLine ('SYSTEM Path Folders (if you are root) for Dynamic Libraries', ...
                '-', 2, SepLength );
    fprintf ( 'Press ''ENTER'' to display your SYSTEM Path Folders ...\n\n' );
    pause;
    [Status, LDPath] = system ( 'cat /etc/ld.so.conf' );    
end

Spathes1 = strrep ( LDPath, '\', '\\' );
Spathes = strrep ( Spathes1, ';', ';\n' );

if Status == 0,
  fprintf ( Spathes );
else
  disp ( 'Unable to get the folders!\n' );
end

if ~Windows,
    
  fprintf ( '\nUSER Dynamic Libraries Path Folders:\n\n' );
  [Status, LDPath] = system ( 'echo $LD_LIBRARY_PATH' );  
  Upathes1 = strrep ( LDPath, '\', '\\' );
  Upathes = strrep ( Upathes1, ';', ';\n' );
  if Status == 0,
    fprintf ( Upathes );
  else
    fprintf ( 'Unable to get the folders!' );
  end

  fprintf ( '\nBinary Path Folders:\n\n' );
  [Status, BPath] = system ( 'echo $PATH' );  
  Bpathes1 = strrep ( BPath, '\', '\\' );
  Bpathes = strrep ( Bpathes1, ';', ';\n' );
  if Status == 0,
    fprintf ( Bpathes );
  else
    fprintf ( 'Unable to get the folders!' );
  end
  
end

%% END

fprintf ( '\n... FIM Compilation is finished.\n' );
CapAndLine ('','=', 2, SepLength );

end

%% Function CapAndLine

function CapAndLine ( Name, Symbol, LengthBefore, LengthAfter )
n = size ( Name, 2 );
if all(size(Name)); T = [ ' ' Name ' ' ]; else T = ''; end
fprintf ( [ '\n', repmat(Symbol,1,LengthBefore), T, ...
            repmat(Symbol,1,LengthAfter-n), '\n\n' ] );
end