%
% CorrelationMatch ----------------------------------------------------------
%
%  Function matches patches from two images provided as columns of two
%  input matrices using correlation measure with specified threshold and
%  subsamples the patches pixels according to Step value.
%
%  Author(s)     : Marina Kudinova
%  e-mail        : kudinova@eia.udg.es
%  Working Group : Computer Vision and Robotics Group
%  Homepage      : http://vicorob.udg.es/
%  Project       : FIM
%  File          : CorrelationMatch.m
%  Date          : 20/06/2007 - 21/06/2007
%  Compiler      : MATLAB >= 7.0
%
% -------------------------------------------------------------------------
%
%  U S A G E :
%
%  Matches = CorrelationMatch ( Patches1, Patches2, Threshold, Step )
%
%  I N P U T :
%
%  Patches1   - patches from Image1, each column corresponds to one patch.
%  Patches2   - patches from Image2, each column corresponds to one patch.
%  Threshold  - [0.7] threshold on correlation value.
%  Step       - step for subsampling the patches pixels.
%
%  O U T P U T :
%
%  Matches - matrix, which contains found by correlation matches. First row
%            contains indices to the Frames (Features,Keypoints) from 
%            Image1, second row contains indices to Image2.
%
% -------------------------------------------------------------------------

function ...
Matches = CorrelationMatch ( Patches1, Patches2, Threshold, Step )

% Deafult threshold = 0.7

% Test the input parameters
error ( nargchk ( 3, 4, nargin ) );
error ( nargoutchk ( 1, 2, nargout ) );

if nargin == 3; Step = 1; end;

N1 = size(Patches1,2);
N2 = size(Patches2,2);

Matches = 1:N1;
Matches(2:3,:) = 0;

for i1 = 1 : N1

  patch1 = Patches1(:,i1);
  
  for i2 = 1 : N2
      
      patch2 = Patches2(:,i2);
      CorrCoeff = correl ( patch1, patch2, Step );
      
      if CorrCoeff >= Threshold  &&  CorrCoeff > Matches(3,i1)
          Matches(2,i1) = i2;
          Matches(3,i1) = CorrCoeff;
      end
      
  end
  
end  
 
Matches = Matches(1:2,:);
Matches = Matches(:,Matches(2,:)~=0);

% -------------------------------------------------------------------------
