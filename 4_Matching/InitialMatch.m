% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%  INITIAL MATCHING
%
%  Author(s)     : Marina Kudinova, Jordi Ferrer, Ricard Prados
%  e-mail        : { kudinova, jferrerp, rprados } @ eia.udg.es
%  Working Group : Computer Vision and Robotics Group
%  Homepage      : http://vicorob.udg.es/
%  Project       : FIM
%  File          : InitialMatch.m
%  Date          : 08/09/2006 - 21/06/2006
%  Compiler      : MATLAB >= 7.0
%
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%
%  U s a g e :
%
%  [ initial_matches ] = InitialMatch( Base, ToMatch, MatchingParameters )
%
%  A r g u m e n t s :
%
%    Base    - matrix with descriptors of the FeaturesImg1, column number i
%              contains the descriptor of the i-th feature in Image1.
%    ToMatch - matrix with descriptors of the FeaturesImg2, column number i
%              contains the descriptor of the i-th feature in Image2.
%    MatchingParameters - 'SIFT' nearest neighbour based on (NextClosest/
%              Closest) ratio. See the description. (Input is not case-
%              sensitive.). Can be structure, 1 (default params) or 0 
%              (matching is cancelled). 'Correlation' matcher is used when 
%              instead of descriptors image patches are used, after 
%              rotation or not.
%
%  R e t u r n s :
%
%  initial_matches (single column is described):
%
%    1 row - index of the feature in FeaturesImg1
%    2 row - index of the matched feature from FeaturesImg2
%
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function [ initial_matches ] = InitialMatch ( Base, ToMatch, MatchingParameters )

error ( nargchk ( 3, 3, nargin ) );
error ( nargoutchk ( 1, 1, nargout ) );

MatchParamsChecked = CheckMatchingParameters ( MatchingParameters );

if ~isstruct ( MatchParamsChecked )
    initial_matches = [];
    return;

elseif strcmpi ( MatchParamsChecked.Matcher, 'Correlation' )

    CorrelationThreshold = MatchParamsChecked.CorrelationThreshold;
    initial_matches = CorrelationMatch ( Base, ToMatch, ...
                                         CorrelationThreshold );

elseif strcmpi ( MatchParamsChecked.Matcher, 'DistRatio' )

    RatioValue = MatchParamsChecked.RatioValue;
    if MatchParamsChecked.Bidirectional,
        matches = RobustSiftMatch ( Base, ToMatch, RatioValue );
    else
        matches = siftmatch ( Base, ToMatch, RatioValue );
    end

    nm = size(matches,2);
    d = zeros(1,nm);
    for j = 1:nm
        d(j) = norm ( Base(:,matches(1,j)) - ToMatch(:,matches(2,j)) );
    end
    initial_matches = [ matches; d ];

end

%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
