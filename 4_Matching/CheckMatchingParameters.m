%% CheckMatchingParameters
%  
%  The CheckMatchingParameters function process the structure, that
%  contains input parameters for the InitialMatch function of the FIM.
%
%  Author(s)     : Marina Kudinova, Jordi Ferrer
%  e-mail        : { kudinova, jferrerp } @ eia.udg.es
%
%  Working Group : Computer Vision and Robotics Group
%  Homepage      : http://vicorob.udg.es/
%
%  Project       : FIM
%  File          : CheckMatchingParameters.m
%  Date          : 20/01/2007 - 20/01/2007
%  Compiler      : MATLAB >= 7.0
%
%% FUNCTION

function ...
MatchParamsChecked = CheckMatchingParameters ( MatchingParameters )

Matchers = { 'DistRatio', 'Correlation' };

% MatchingParameters.Matcher = 'Sift';
% MatchingParameters.SiftRatio = 1.5;                % ratio between the next closest and closest nearest neighbour for SIFT matching
% MatchingParameters.Bidirectional = 0;              % If ~= 0 Intersection between "image1 to image2" and "image2 to image1" matches
% MatchingParameters.CorrelationThreshold = 0.7;
% MatchingParameters = 1;                            % <- to use default Matching parameters
% MatchingParameters = 0;                            % <- to cancel Matching, RANSAC is cancelled then by default

DefaultMatcher = 'DistRatio';
DefaultRatioValue = 1.5;
DefaultBidirectional = 1;
DefaultCorrelationThreshold = 0.7;
DefaultGuidedPostMatching = 1;

if isstruct(MatchingParameters)
    
    if isfield(MatchingParameters,'Matcher') && sum(strcmpi(MatchingParameters.Matcher,Matchers))~=0
        MatchParamsChecked.Matcher = MatchingParameters.Matcher;
    else
        MatchParamsChecked.Matcher = DefaultMatcher;
    end
    
    if isfield(MatchingParameters,'RatioValue') && isscalar(MatchingParameters.RatioValue) &&...
       MatchingParameters.RatioValue > 0
        MatchParamsChecked.RatioValue = MatchingParameters.RatioValue;
    else
        MatchParamsChecked.RatioValue = DefaultRatioValue;
    end

    if isfield(MatchingParameters,'Bidirectional') && isscalar(MatchingParameters.Bidirectional) &&...
       ( MatchingParameters.Bidirectional == 0 || strcmpi(MatchParamsChecked.Matcher,'Correlation') )
        MatchParamsChecked.Bidirectional = 0;
    else
        MatchParamsChecked.Bidirectional = DefaultBidirectional;
    end
       
    if isfield(MatchingParameters,'CorrelationThreshold') && isscalar(MatchingParameters.CorrelationThreshold) &&...
       MatchingParameters.CorrelationThreshold == 0
        MatchParamsChecked.CorrelationThreshold = MatchingParameters.CorrelationThreshold;
    else
        MatchParamsChecked.CorrelationThreshold = DefaultCorrelationThreshold;
    end

    if isfield(MatchingParameters,'GuidedPostMatching') && isscalar(MatchingParameters.GuidedPostMatching) &&...
       MatchingParameters.GuidedPostMatching == 0
        MatchParamsChecked.GuidedPostMatching = 0;
    else
        MatchParamsChecked.GuidedPostMatching = DefaultGuidedPostMatching;
    end

elseif isnumeric(MatchingParameters) 
    
    if MatchingParameters == 0
        MatchParamsChecked = 0;
        return;
    else
        MatchParamsChecked.Matcher = DefaultMatcher;
        MatchParamsChecked.SiftRatio = DefaultSiftRatio;
        MatchParamsChecked.Bidirectional = DefaultBidirectional;
        MatchParamsChecked.GuidedPostMatching = DefaultGuidedPostMatching;
    end
else
    error ( 'MATLAB:FIM:Input', ...
          [ 'Incorrect MatchingParameters input: ',...
            'can be 0, 1 or structure \n', ...
            'with Matcher, RatioValue, CorrelationThreshold and \n'...
            'GuidedPostMatching fields. \n' ] );
end

% END
% -------------------------------------------------------------------------
