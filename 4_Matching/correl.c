/*
   Autor(s)      : Marina Kudinova & Jordi Ferrer Plana
   e-mail        : {jferrerp, kudinova}@eia.udg.es
   Branch        : Computer Vision

   Working Group : Underwater Vision Lab
   Project       : -
   Homepage      : http://mosaic.udg.es

   Module        : Improved correlation

   File          : correl.c
   Date          : 03/10/2006 - 03/10/2006
   Encoding      : ISO-8859-1 (Latin-1)

   Compiler      : Matlab Mex 7.2.0.232 (R2006a) (ANSI C++)
   Libraries     :

   Notes         :

  -----------------------------------------------------------------------------

   Copyright (C) 2003-2005 by Jordi Ferrer Plana

   This source code is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This source code is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

   See GNU licenses at http://www.gnu.org/licenses/licenses.html for
   more details.

  -----------------------------------------------------------------------------
*/

/* Input : correl ( I1, I2, [ Step = 1 ] ) */
/* Output: Correlation Coefficient */

#include <math.h>

#include "mex.h"

void mexFunction ( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
   double *out, xi, yi, xi2, yi2, xiyi, r2, dxi, dyi;
   int r, c, n, i, j, step;
   double *IData, *JData, *AIData, *AJData;
   unsigned int imRowAlign, numPointPerRow, numPointPerCol;
	
   /* Parameter checks */
   if ( nrhs != 3 && nrhs != 2 )
   {
     mexErrMsgTxt ( "Usage: corr = correl (A, B [, Step ])\n" );
     return;
   }

   if ( mxGetClassID ( prhs[0] ) != mxDOUBLE_CLASS ||
        mxGetClassID ( prhs[1] ) != mxDOUBLE_CLASS )
   {
      mexErrMsgTxt ( "Input matrices must contain double data!\n" );
      return;
   }

   if ( nrhs == 3 && ( mxGetN ( prhs[2] ) * mxGetM ( prhs[2] ) ) != 1 )
   {
      mexErrMsgTxt ( "Last parameter must be scalar!\n" );
      return;
   }

   if ( ( step = ( nrhs == 3 ) ? mxGetScalar ( prhs[2] ) : 1) < 1 )
   {
     mexErrMsgTxt ( "Last parameter must be greater than 0!\n" );
     return;
   }

   /* Reading the parameters */
   r = mxGetM ( prhs[0] );
   c = mxGetN ( prhs[0] );

   if ( r != mxGetM ( prhs[1] ) || c != mxGetN ( prhs[1] ) )
   {
      mexErrMsgTxt ( "Input matrices dimensions must agree!\n" );
      return;
   }

   /* Compute initial matrices addres */
   AIData = IData = (double *)mxGetPr ( prhs [0] );
   AJData = JData = (double *)mxGetPr ( prhs [1] );

   /* Require memory for the return parameter */
   plhs [0] = mxCreateDoubleMatrix (1, 1, mxREAL);
   out = (double *)mxGetPr ( plhs [0] );

   /* Compute the correlation */
   imRowAlign = c * step;

   /* Compute sizes in both directions */
   numPointPerRow = r / step + ( r % step ? 1 : 0 );
   numPointPerCol = c / step + ( c % step ? 1 : 0 );

   /* Number of elements */
   n = numPointPerRow * numPointPerCol;

   xi = yi = xi2 = yi2 = xiyi = 0.0;

   for ( j = 0; j < numPointPerRow; j++ )
   {
      for ( i = 0; i < numPointPerCol; i++ )
      {
         /* Read the data */
         dxi = *IData;
         dyi = *JData;
         /* Compute accumulated sums */
         xi += dxi;
         yi += dyi;
         xi2 += dxi * dxi;
         yi2 += dyi * dyi;
         xiyi += dxi * dyi;
         /* Following element */
         IData += step;
         JData += step;
      }
      /* Following row */
      IData = AIData += imRowAlign;
      JData = AJData += imRowAlign;
   }

   /* Compute correlation coefficient from accumulated sums */
   out[0] =  ( (double) n * xiyi - xi * yi ) /
               sqrt (( (double) n * xi2 - xi * xi ) * ( (double) n * yi2 - yi * yi ) );

   return;
}
