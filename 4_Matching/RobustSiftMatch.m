%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.es
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.es
%
%  Module        : Filter robust sift matches WITHOUT dupplicates.
%
%  File          : RobustSiftMatch.m
%  Date          : 28/12/2006 - 01/03/2007
%
%  Compiler      : MATLAB
%  Libraries     : -
%
%  Notes         : - File written in ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2006 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  RobustSiftMatch Find the most robust sift matches fusing the Left-Right
%                  and the Right-Left matches.
%
%      Matches = RobustSiftMatch ( Descr1, Descr2 [, Thresh] )
%
%     Input Parameters:
%      Descr1: First set of 128xN sift descriptors.
%      Descr2: Second set of 128xN sift descriptors.
%      Thresh: Threshold used in the Sift matching algorithm.
%
%     Output Parameters:
%      Matches: Set of 2xN matched indices from the Descr1, Descr2 sets of
%               descriptors.
%

function Matches = RobustSiftMatch ( Descr1, Descr2, Thresh )

  % Test the input parameters
  error ( nargchk ( 2, 3, nargin ) );
  error ( nargoutchk ( 1, 1, nargout ) );

  % Compute descriptors from Left-Right and Right-Left, this is scan all
  % the descriptors from the first parameter and try to match in the set of
  % the second parameter.
  % This ends up with a set of matches in which there are no duplicates
  % in the first row of indices.
  % This is done two times, one from Left to Right and the other from Right
  % to left to keep the set of most robust matches: the set that
  % are correctly matched from Left to Right and viceversa.
  if nargin == 2,
    Matchesl = siftmatch ( Descr1, Descr2 );
    Matchesr = siftmatch ( Descr2, Descr1 );
  else
    Matchesl = siftmatch ( Descr1, Descr2, Thresh );
    Matchesr = siftmatch ( Descr2, Descr1, Thresh );
  end

  % Test for empty matched pairs
  if isempty ( Matchesl ) && isempty ( Matchesr ),
    Matches = [];
  elseif isempty ( Matchesl ),
    % In the inverse order as they are Right Matches
    Matches(1,:) = Matchesr(2,:);
    Matches(2,:) = Matchesr(1,:);
  elseif isempty ( Matchesr ),
    Matches = Matchesl;
  else
    % The first row on the Right Matches doesn't contain dupplicates, the
    % last element is the bigest.
    % Compute the mapping from the 1st row from the Left to the 2nd row
    % of the Right
    Matchesx = zeros ( 1, max ( [ Matchesl(2,:) Matchesr(1,end) ] ) );
    % Construct the direct mapping from the first row of Left to the
    % second row of Right
    Matchesx(Matchesr(1,:)) = Matchesr(2,:);
    % Compute the elements that are equal in the 1st row on the Left and the
    % 2nd row on the Right when applying the mapping.
    Matches = Matchesl(:, Matchesl(1,:) == Matchesx(Matchesl(2,:)));
  end
end
