%
% GuidedMatch --------------------------------------------------------
%
%  Function performs guided postmatching of the all detected keypoints
%  using the estimated on the basis of Initial Matches motion.
%
%  Author(s)     : Marina Kudinova, Jordi Ferrer
%  e-mail        : { kudinova, jferrerp } @ eia.udg.es
%  Working Group : Computer Vision and Robotics Group
%  Homepage      : http://vicorob.udg.es/
%  Project       : FIM
%  File          : GuidedMatch.m
%  Date          : 21/06/2007 - 21/06/2007
%  Compiler      : MATLAB > 7.0
%
% -------------------------------------------------------------------------
%
%  U S A G E :
%
%  AddMatches = GuidedMatch ( H, DistThres, Features1, Features2 )
%
%  I N P U T :
%
%  H - estimated homography (motion).
%  DistThres - search radius to look for possible matches.
%  Features1 - features detected in the first image.
%  Features2 - features detected in the second image.
%
%  O U T P U T :
%
%  AddMatches - all the keypoints pairs following the estimated homography
%               according to specified distance threshold.
%
% -------------------------------------------------------------------------

function AddMatches = GuidedMatch ( H, DistThres, Features1, Features2 )

% GTmatches = GTmatch ( H1to2p, sqrt(2), ResultsFIM.Img1Features, ResultsFIM.Features2 )
% Features1 = ResultsFIM.Img1Features;
% Features2 = ResultsFIM.Img2Features;

% load '../Data/VGG/graf/H1to2p.mat' -ASCII;
% H = H1to2p;

Img1F = Features1(1:2,:);
Img2F = Features2(1:2,:);
% Img1S = Features1(4,:);
% Img2S = Features2(4,:);

Img1Fprj = ApplyHomo ( H, Img1F );

N1Fprj = size(Img1Fprj,2);
N2F = size(Img2F,2);

matches = zeros(3,N2F);
matches(1,:) = 1:N2F;
matches(3,:) = inf;

for i = 1 : N1Fprj
    Prj = repmat ( Img1Fprj(:,i), 1, N2F );
    Dist = sqrt ( sum ( ( Prj - Img2F ).^2, 1) );
    [ d , ind2 ] = min(Dist);

    if d <= DistThres
        if d < matches(3,ind2)
            matches(2,ind2) = i;
            matches(3,ind2) = d;
        end
    end
end

% XYmatches = matches ( 1:2, matches(2,:) ~= 0 );

AddMatches = matches ( 1:2, matches(2,:) ~= 0 );

% next the check for sufficient closeness in scale is needed

% Indices = 1 : size(XYmatches,2);
% Sind1 = Indices ( (Img1S(XYmatches(2,:)) ./ Img2S(XYmatches(1,:))) <= sqrt(2) );
% Sind2 = Indices ( (Img2S(XYmatches(1,:)) ./ Img1S(XYmatches(2,:))) <= sqrt(2) );
% Sind  = union ( Sind1, Sind2 );

% GTmatches = XYmatches ( :, Sind );

% PlotMatch ( Img1, Img2, Features1, Features2, GTmatches, 'GT', 'GT Matches');

end
