%
%% RunFIM -----------------------------------------------------------------
%  
%  Author(s)     : Marina Kudinova, Jordi Ferrer Plana, 
%                  Olivier Delaunoy, Ricard Prados
%  e-mail        : { kudinova, jferrerp, delaunoy, rprados } @ eia.udg.es
%  Working Group : Computer Vision and Robotics Group
%  Homepage      : http://vicorob.udg.es/
%  Project       : FIM
%  File          : RunFIM.m
%  Date          : 25/09/2006 - 28/06/2007
%  Compiler      : MATLAB >= 7.0
%
% ------------------------------ RunFIM -----------------------------------
% 
%  RunFIM script contains an adjustable example of how to use FIM software.
%  RunFIM has the following structure:
%
%  1. The beginning and most part of the RunFIM file consists of providing
%     values to fields of the InputFIM structure, which contains all the 
%     parameters FIM needs. You can change any parameter before executing
%     RunFIM. The allowed value and meaning of each parameter are given by
%     comments inside the code near the parameter itself. 
%  2. Then the InPlotFIM structure is filled, which contains parameters 
%     for the PlotFIM function. PlotFIM displays results after executing 
%     FIM. PlotFIM is called independently from FIM.
%  3. Executing FIM and then PlotFIM.
%
%  RunFIM can be tested with currently specified parameters on images from 
%  "Images" subfolder of the main FIM folder. Changing "PairN" parameter in
%  the "Loading Images" section below you can test different image pairs 
%  coming along with FIM software. If you compiled the FIM successfully 
%  ( see the ReadMe.txt file ), set FIM as Current Directory and in the 
%  Matlab Command Window enter:  >> RunFIM
%
%  To use FIM on your images with other InputFIM parameters:
%  1. Copy your pair of images to the "../FIM/Images" folder or specify
%     the path to your images in the "Adding Paths" subsection of the 
%     current RunFIM.m file.
%  2. Add the names of images to the ImageFiles cell array in the "Loading
%     Images" subsection of the current RunFIM.m file and change the PairN
%     value respectively.
%  3. In the "FIM Input Parameters" set the necessary parameters.
%  4. Save RunFIM.m after your modifications and >> RunFIM.m
%
% ------------------------------ F I M ------------------------------------
%
%  FIM = Feature-based Image Matching. FIM takes 2 images as an input,
%  converts each image to grayscale if they are color and enhances their 
%  contrast if specified. Afterwards at each image the salient points are
%  detected. Next a descriptor is calculated for each point. Calculated 
%  descriptors are matched between two images and on the basis of initial 
%  correspondences the motion between images is estimated. The mismatched 
%  correspondences are rejected when estimating the motion. 
%
%  U S A G E :
%
%  [ Img1, Img2, ResultsFIM ] = FIM ( Image1, Image2, Mask, InputFIM );
%
%  I N P U T :
%
%  Image1   - first image to be matched. Image1 and Image2 are to be of the
%             same size.
%  Image2   - second image to be matched.
%  Mask     - matrix of zeros and ones, the same size as both images. 
%             Images areas corresponding to zeros will not be processed.
%  InputFIM - Structure with all FIM parameters. Parameters are divided 
%             into groups, hence InputFIM structure consists of Processing, 
%             Detection, Description, Matching, MotionEstimation, Save and 
%             Display substructures. Setting the whole substructure equal 
%             to zero, for example: "InputFIM.Matching = 0" cancels the 
%             corresponding FIM step.
%
%  O U T P U T :
%
%  Img1, Img2 - processed images where the detection and description were 
%               actually performed.
%  ResultsFIM - structure containing the results of FIM execution. This
%               structure consists of next fields: Img1Features, 
%               Img2Features, Img1Descriptors, Img2Descriptors, 
%               InitialMatches, Homography, AcceptedMatches, 
%               RejectedMatches, AllFollowingMatches, OverlapArea, 
%               HomoCheck. ResultsFIM are saved to the disk according to 
%               the specified InputFIM.Save parameters. If MSER detector
%               was used there are also Img1Regions and Img2Regions fields.
%
%--------------------------------------------------------------------------

warning off all;

%%  Loading Images --------------------------------------------------------

ImageFiles = { ...
'img1.ppm',           'img2.ppm', ...                 % Pair 1:  Graffiti
'img3.ppm',           'img5.ppm', ...                 % Pair 2:  Graffiti
'imgl13800.jpg',      'imgl13820.jpg', ...            % Pair 3:  Underwater Stereo
'imgl01111.jpg',      'imgl01151.jpg' ...             % Pair 4:  Underwater
'sq_wreck02806.jpg',  'sq_wreck02866.jpg' ...         % Pair 5:  Underwater Difficult
'Lava21.bmp',         'Lava22.bmp', ...               % Pair 6:  Lava Grayscale
'im1.png',            'im2.png', ...                  % Pair 7:  Underwater
'image80.png',        'image82.png', ...              % Pair 8:  Poster - slight rotation
'image80.png',        'image90.png', ...              % Pair 9:  Poster - big rotation
'DSCN1699.JPG',       'DSCN1700.JPG' ...              % Pair 10: IceCream
'B11.bmp',            'B12.bmp'                       % Pair 11: Lava
};           

PairN = 3;

%% Adding Paths -----------------------------------------------------------

PathToFIM     =  './';                                % from current folder
PathToImages  =  './Images/';                         % from current folder
SaveResultsTo =  './ResultsFIM/';                     % from current folder

%% ==================== FIM Input Parameters ==============================
Mask = [];

% Image Processing Parameters ---------------------------------------------

InputFIM.Processing.SingleChannel = 4;                % 0-no, 1-red, 2-green, 3-blue, 4-Y channel of YIQ, 5-PCA
InputFIM.Processing.Equalization  = 1;                % 0-no, 1-normalization, 2-equalization, 3-CLAHE
InputFIM.Processing.ResizeCoeff   = 2;                % resize coefficient to decrease sizes of images
% InputFIM.Processing = 0;                            % <- to cancel Preprocessing

%% Detection & Description Parameters -------------------------------------

Detectors   = { 'SIFT',   'SURF', ...                 % 1 - 2  <- scale-invariant detectors
                'Harris', 'Hessian', 'Laplacian', ... % 3 - 5  <- non-scaleinvariant detectors
                'MSER' };                             % 6      <- region detector

Descriptors = { 'SIFT', 'SURF', 'SURF-128', ...       % 1 - 3
                'SURF-36', 'U-SURF', ...              % 4 - 5
                'U-SURF-128', 'U-SURF-36', ...        % 6 - 7
                'ImagePatch', 'RotatedImagePatch' };  % 8 - 9      

InputFIM.Detection.DetType          = Detectors(1);
InputFIM.Detection.MaxNumber        = 100000;         % max number of features to detect
InputFIM.Detection.RadiusNonMaximal = 8;              % radius for non-maximal suppression for detector
InputFIM.Detection.Sigma            = 0.5;            % sigma of the Gaussian used in Harris, Laplacian and Hessian detectors; If = 0, no smoothing is performed
InputFIM.Detection.MinCornerness    = 0;              % [0-1] cornerness threshold
InputFIM.Detection.MinArea          = 30;             % [30] min region area in pix for region detectors
InputFIM.Detection.DescType         = Descriptors(1);            

InputFIM.Description.DescType       = InputFIM.Detection.DescType;
InputFIM.Description.PatchRadius    = 14;             % radius of the image patch
% InputFIM.Detection = 0;                             % cancels Detection
% InputFIM.Description = 0;                           % <- to cancel description 

%% Matching Parameters ----------------------------------------------------

Matchers = { 'DistRatio', 'Correlation' };

InputFIM.Matching.Matcher               = Matchers(1);
InputFIM.Matching.RatioValue            = 1.5;        % ratio between the next closest and closest nearest neighbour for SIFT matching ; the bigger is the ratio, the smaller is the amount of matches
InputFIM.Matching.CorrelationThreshold  = 0.9;
InputFIM.Matching.Bidirectional         = 1;          % If ~= 0 Intersection between "image1 to image2" and "image2 to image1" matches
InputFIM.Matching.GuidedPostMatching    = 1;
% InputFIM.Matching = 0;                              % cancels Matching
 
%% Motion Estimation Parameters -------------------------------------------

MotionModels = { '2D', ... % Use an homography
                 '3D' } ;  % Use a fundamental matrix

InputFIM.MotionEstimation.MotionModel = MotionModels{1} ;

HomographyModels  = { 'Euclidean', 'Similarity', 'Affine', ...   % 1 - 3
                      'Projective', 'ProjectiveNLM'  };          % 4 - 5

InputFIM.MotionEstimation.HomographyModel = HomographyModels{5};
InputFIM.MotionEstimation.SpatialStdev    = 2;        % stdev of spatial coord of features, in pixels
% InputFIM.MotionEstimation = 0;                      % cancels motion estimation

%% Save Parameters --------------------------------------------------------

InputFIM.Save.Results     = 1;                        % 1 - save; 0 - do not save
InputFIM.Save.HomoInASCII = 1;                        % 1 - save; 0 - do not save
InputFIM.Save.SaveTo      = SaveResultsTo;
InputFIM.Save.TestName    = '';
% InputFIM.Save           = 0;

%% Display Parameters -----------------------------------------------------

InputFIM.Display.DisplayConfig  = 1;                  % 1 -> display FIM configuretion; 0 -> not display
InputFIM.Display.DisplayResults = 1;                  % 1 -> display statistics of FIM execution results; 0 -> not display
InputFIM.Display.SepLines       = 1;                  % 1 -> display separation lines; 0 -> not display
% InputFIM.Display = 0;                               % <- to cancel display output of FIM

%% Plotting Parameters ----------------------------------------------------

InPlotFIM.PlotInitialImages    = 1;                   % 1 -> initial images are displayed; 0 -> the processed grayscale images are displayed;
InPlotFIM.SaveFigures          = 1;             
InPlotFIM.SaveInFormat         = 'png';               % supported: 'jpg', 'tif', 'eps', 'png', 'ppm', 'wmf' ('wmf' only for Windows)
InPlotFIM.PlotImages           = 1;                   % 1 -> display images with keypoints; 0 -> not display
InPlotFIM.PlotInitialMatches   = 1;                   % 1 -> display initial matches; 0 -> not display
InPlotFIM.PlotRejectedMatches  = 1;                   % 1 -> display rejected by RANSAC matches; 0 -> not display
InPlotFIM.PlotAcceptedMatches  = 1;                   % 1 -> display accepted by RANSAC matches; 0 -> not display
InPlotFIM.PlotMotion           = 1;                   % 1 -> display matched pairs in the first image; 0 -> not display;
InPlotFIM.PlotMosaic           = 1;                   % 1 -> display mosaic of two images; 0 -> not display;
InPlotFIM.PlotFollowingMatches = 1;                   % 1 -> display all following matches;
InPlotFIM.WhereToSave          = SaveResultsTo; 
InPlotFIM.PlotMatchedRegions   = 1;                   % 1 -> MSER only, regions borders will be displayed for Initial, Rejected and Accepted matches
InPlotFIM.PercentageToDisplay  = 100;                 % percentage of accepted mathces to display, useful when there is lots of matches
if isstruct(InputFIM.Processing)
InPlotFIM.ResizeCoeff          = InputFIM.Processing.ResizeCoeff;
end
% InputPlotFIM = 0;                                   % suppress plotting

%% ========================== FIM Execution ===============================

% Addition of Paths
addpath ( PathToFIM );
SetPathsFIM ( PathToFIM );

% Loading Images
Image1 = imread ( [ PathToImages char(ImageFiles(2*PairN-1)) ] );
Image2 = imread ( [ PathToImages char(ImageFiles(2*PairN)) ] );

% Executing FIM
[ Img1, Img2, ResultsFIM ] = FIM ( Image1, Image2, Mask, InputFIM );

%% ====================== Plotting the FIM results ========================

% Figs contains handles of Figures
Figs = PlotFIM ( Image1, Image2, Img1, Img2, ResultsFIM, InPlotFIM );

% -------------------------------------------------------------------------
% END
