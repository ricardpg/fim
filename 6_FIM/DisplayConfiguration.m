
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%  DisplayConfiguration - displaying FIM configuration
%
%  Author(s)     : Marina Kudinova, Jordi Ferrer, Ricard Prados
%  e-mail        : { kudinova, jferrerp, rprados } @ eia.udg.es
%
%  Working Group : Computer Vision and Robotics Group
%  Homepage      : http://vicorob.udg.es/
%
%  Project       : FIM
%  File          : DisplayConfiguration.m
%  Date          : 16/09/2006 - 25/06/2007
%  Compiler      : MATLAB >= 7.0
%
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function DisplayConfiguration ( InFIM )

% Reading the Parameters structures ---------------------------------------
                            
ProcessingParameters   =  InFIM.Processing;
DetectionParameters    =  InFIM.Detection;
DescriptionParameters  =  InFIM.Description;
MatchingParameters     =  InFIM.Matching;
RansacParameters       =  InFIM.MotionEstimation;
DisplayParameters      =  InFIM.Display;

% ~~~~~~~~~~ Checking Whether Display Configuration is cancelled

if ~isstruct(DisplayParameters) || DisplayParameters.DisplayConfig == 0
    return;
end

% ~~~~~~~~~~ Image Preprocessing Parameters

SingleChannels = { '', ...
    'RED Channel Only, ', ...
    'GREEN Channel Only, ', ...
    'BLUE Channel Only, ', ... 
    'Conversion to YIQ, Y Channel Only, ', ...
    'PCA, ' };

Equalizations = { 'NO Enhancement', ...
    'Normalization', ...
    'Equalization', ...
    'CLAHE' };

%% ~~~~~~~~~~ Displaying the Configuration ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

% fprintf ( [ '\n-- FIM ', repmat('-',1,65), '\n' ] );

fprintf ( '\nC o n f i g u r a t i o n :\n' );

%% Displaying the Preprocessing Parameters

fprintf ( '\nPreprocessing:      ' );

if ~isstruct(ProcessingParameters)
    fprintf ( 'NO Preprocessing of Images\n' );
else
    fprintf ( [ char(SingleChannels(ProcessingParameters.SingleChannel+1)), ...
                char(Equalizations(ProcessingParameters.Equalization+1)) ] );
end

%% Displaying the Detection Parameters

fprintf ( '\nDetection:          ' );

if ~isstruct(DetectionParameters)
    cancelled;
    return;
else
    fprintf ( upper ( char ( DetectionParameters.DetType ) ) );
    if sum(strcmpi(DetectionParameters.DetType, ...
       {'harris','hessian','laplacian'})) ...
       && DetectionParameters.Sigma ~= 0 
        fprintf ( ', Sigma %1.1f', DetectionParameters.Sigma );
    end
    fprintf ( ', Max %d Keypoints', DetectionParameters.MaxNumber );
    fprintf ( ', Radius %d pixel', DetectionParameters.RadiusNonMaximal );
end

%% Displaying the Description Parameters

fprintf ( '\nDescription:        ' );

if ~isstruct(DescriptionParameters)
    cancelled;
    return;
else
    fprintf ( char ( DescriptionParameters.DescType ) );
    if ~isempty ( findstr ('ImagePatch', char ( DescriptionParameters.DescType ) ) )
        fprintf ( ', Circular, Radius %d pix', DescriptionParameters.PatchRadius );
    end
    if ~isempty ( findstr ('U-', char ( DescriptionParameters.DescType ) ) )
        fprintf ( ', Upright');
    end
    if ~isempty ( findstr ('-128', char ( DescriptionParameters.DescType ) ) )
        fprintf ( ', Extended');
    end
    if ~isempty ( findstr ('-36', char ( DescriptionParameters.DescType ) ) )
        fprintf ( ', Short');
    end
end

%% Displaying the Matching Parameters

fprintf ( '\nMatching:           ' );

if ~isstruct(MatchingParameters)
    cancelled;
    return;
else
    fprintf ( char ( MatchingParameters.Matcher ) );
    if strcmpi ( 'DistRatio', MatchingParameters.Matcher )
      fprintf ( ' >= %1.1f', MatchingParameters.RatioValue );
    end
    if strcmpi ( 'Correlation', MatchingParameters.Matcher )
        fprintf ( ' >= %1.1f', MatchingParameters.CorrelationThreshold );
    end
    if MatchingParameters.Bidirectional
      fprintf ( ', Bidirectional: Img1 <=> Img2' );
    else
      %% Each Img2 feature is tried to be matched against all Img1 features
      %% Set of Img1 features is considered as a Database
      fprintf ( ', Unidirectional: Img1 => Img2' );
    end
end

%% Displaying the MotionEstimation Parameters

fprintf ( '\nMotion Estimation:  ' );

if ~isstruct(RansacParameters)
    cancelled;
    return;
else
    if strcmpi( RansacParameters.MotionModel, '2D' ), 
        fprintf ( ['2D, RANSAC, ', ...
            upper(char(RansacParameters.HomographyModel)),' Homography' ] );
        fprintf ( ', StDev %d pixel\n', RansacParameters.SpatialStdev );
    else
        fprintf ( '3D, RANSAC, Fundamental matrix, StDev %d pixel\n', RansacParameters.SpatialStdev );
    end
end

%% Guided Matching

    
if isstruct(MatchingParameters) && MatchingParameters.GuidedPostMatching
  if strcmpi( RansacParameters.MotionModel, '3D' )
    fprintf ( 'Guided Matching:    Not available for 3D motion\n' );
  else
    fprintf ( 'Guided Matching:    ' );
    fprintf ( 'Executed, Search Radius %d pixel\n', RansacParameters.SpatialStdev );
  end
end

end

%% Additional functions used in the function above

function cancelled
fprintf ( 'CANCELLED, subsequent FIM steps are NOT executed!\n' );
end
