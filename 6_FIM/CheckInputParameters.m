%% CheckInputParameters
%  
%  The CheckInputParameters function process the structures, that are input
%  for the FIM algorithm before executing the algorithm itself.
%
%  Author(s)     : Marina Kudinova, Jordi Ferrer
%  e-mail        : { kudinova, jferrerp } @ eia.udg.es
%
%  Working Group : Computer Vision and Robotics Group
%  Homepage      : http://vicorob.udg.es/
%
%  Project       : FIM
%  File          : CheckInputParameters.m
%  Date          : 17/01/2007 - 05/05/2007
%  Compiler      : MATLAB > 7.0
%
%% FUNCTION

function InFIM = CheckInputParameters (  Image1, Image2, Mask, InputFIM )

error ( nargchk ( 4, 4, nargin ) );
error ( nargoutchk ( 1, 1, nargout ) );

%% Reading the InputFIM structure

if sum(abs(size(Image1(:,:,1)) - size(Image2(:,:,1)))) ~= 0
    error ( 'MATLAB:FIM:Input', 'Incorrect Input Images sizes!' );        
end

if ~isempty(Mask) && ~all ( size( Image1(:,:,1) ) == size(Mask) )
    error ( 'MATLAB:FIM:Input', 'Incorrect sizes of the Mask!' );
end

%% Processing Parameters

if isfield ( InputFIM, 'Processing' )
    InFIM.Processing = CheckProcessingParameters ( InputFIM.Processing );
else
    InFIM.Processing = CheckProcessingParameters ( 1 );
end

Im1nc = size ( Image1, 3 );    % number of color channels in Image1
Im2nc = size ( Image2, 3 );

if Im1nc == 1 && Im2nc == 1;   % if both images have one color channel no SingleChannel processing is needed
    InFIM.Processing.SingleChannel = 0;
end

if ( Im1nc ~= 1 || Im2nc ~= 1 ) && InFIM.Processing.SingleChannel == 0
    error ( 'MATLAB:FIM:Input', ...
          [ 'One or both images has more than 1 color channel!\n', ...
            'DETECTION and subsequent FIM steps are CANCELLED automatically!\n', ...
            'Please specify the correct ProcessingParameters.SingleChannel value.\n' ] );
end
            
%% Detection & Description Parameters

% Detection
if isfield ( InputFIM, 'Detection' )
    InFIM.Detection = CheckDetectionParameters ( InputFIM.Detection );
else
    InFIM.Detection = CheckDetectionParameters ( 1 );
end

% Description
if isfield ( InputFIM, 'Description' )
    InFIM.Description = CheckDescriptionParameters ( InputFIM.Description );
else
    InFIM.Description = CheckDescriptionParameters ( 1 );
end

if isstruct(InFIM.Detection) && isstruct(InFIM.Description)
    if strcmpi(InFIM.Detection.DetType,'sift') && ...
            ~strcmpi(InFIM.Description.DescType,'sift') || ...
            strcmpi(InFIM.Detection.DetType,'surf') && ...
            strcmpi(InFIM.Description.DescType,'sift')
        error ( 'MATLAB:FIM:Input', ...
            [ 'Incorrect Description Parameters: \n', ...
            upper(char(InFIM.Detection.DetType)) ...
            ' Detector' ' and ' ...
            upper(char(InFIM.Description.DescType)) ...
            ' Descriptor are Incompatible!\n'] );
    end
end

%% Matching

if isfield ( InputFIM, 'Matching' )
    InFIM.Matching = CheckMatchingParameters ( InputFIM.Matching );
else
    InFIM.Matching = CheckMatchingParameters ( 1 );
end

%% Motion Estimation

if isfield ( InputFIM, 'MotionEstimation' )
    InFIM.MotionEstimation = CheckMotionParameters ( InputFIM.MotionEstimation );
else
    InFIM.MotionEstimation = CheckMotionParameters ( 1 );
end

%% Display

if isfield ( InputFIM, 'Display' )
    InFIM.Display = CheckDisplayParameters ( InputFIM.Display );
else
    InFIM.Display = CheckDisplayParameters ( 1 );
end

%% Save

if isfield ( InputFIM, 'Save' )
    InFIM.Save = InputFIM.Save;
else
    InFIM.Save = 0;
end

end

