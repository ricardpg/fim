
%%  DisplayResultsFIM ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%
%  Displaying statistics for the results of FIM
%
%  Author(s)     : Marina Kudinova, Jordi Ferrer
%  e-mail        : { kudinova, jferrerp } @ eia.udg.es
%
%  Working Group : Computer Vision and Robotics Group
%  Homepage      : http://vicorob.udg.es/
%
%  Project       : FIM
%  File          : DisplayResultsFIM.m
%  Date          : 21/09/2006 - 20/03/2007
%  Compiler      : MATLAB >= 7.0
%
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%% FUNCTION

function DisplayResultsFIM ( DisplayParameters, ResultsFIM )

% ~~~~~~~~~~ Checking Whether Display Results is cancelled

if ~isstruct(DisplayParameters) || ~DisplayParameters.Display || ...
   ~DisplayParameters.DisplayResults
    return;
end

% If Detection was cancelled
if ~DisplayParameters.Detection
    return;
end

fprintf ( '\nR e s u l t s :\n' );

%% Displaying the Results of Detection

if isempty(ResultsFIM.Img1Features)
    fprintf ( '\nImage1: NO Keypoints are Detected!' );
else
    fprintf ( '\nNumber of Image1 Keypoints  =  %d', ...
              size(ResultsFIM.Img1Features,2) );
end

if isempty(ResultsFIM.Img2Features)
    fprintf ( '\nImage2: NO Keypoints are Detected!' );
    return
else
    fprintf ( '\nNumber of Image2 Keypoints  =  %d', ...
              size(ResultsFIM.Img2Features,2) );
    if isempty(ResultsFIM.Img1Features); return; end;
end

%% Displaying the Results of Description

if ~DisplayParameters.Description % if description was cancelled
    fprintf('\n');
    return
end

if isempty(ResultsFIM.Img1Descriptors) || ...
   isempty(ResultsFIM.Img2Descriptors)
    fprintf ( [ '\nFIM was not able to calculate ', ...
                'Descriptors for detected Keypoints!\n' ] );
    return
end

%% Displaying the Results of Initial Matching

if ~DisplayParameters.Matching % when cancelled
    fprintf('\n');
    return
end

if isempty(ResultsFIM.InitialMatches)
    fprintf ( '\nInitial Matching: NO Matches are Found!\n\n' );
    return
else
    fprintf ( '\nNumber of Initial Matches   =  %d', ...
              size(ResultsFIM.InitialMatches,2) );
end

%% Displaying the RANSAC output results

if ~DisplayParameters.MotionEstimation % when cancelled
    fprintf('\n');
    return;
end

if isfield( ResultsFIM, 'Homography' ) && isempty ( ResultsFIM.Homography ),   
    fprintf ( '\nRANSAC was not able to estimate Homography!\n' );
    return;
end

if isfield( ResultsFIM, 'Fundamental' ) && isempty( ResultsFIM.Fundamental ),
    fprintf ( '\nRANSAC was not able to estimate the Fundamental matrix!\n' );
    return;
end

fprintf ( '\nAccepted by RANSAC Matches  =  %d', ...
          size(ResultsFIM.AcceptedMatches,2) );
fprintf ( '\nRejected by RANSAC Matches  =  %d', ...
          size(ResultsFIM.RejectedMatches,2) );

%% Displaying the results of Guided Post-matching

if ~isempty ( ResultsFIM.AllFollowingMatches )
fprintf ( '\nAll following Matches       =  %d', ...
          size ( ResultsFIM.AllFollowingMatches, 2 ) );
end

%% Displaying the Homography Check Results & Area

if ~isfield( ResultsFIM, 'Fundamental' ),
    if isfield( ResultsFIM, 'Homography' ) && ~ResultsFIM.HomoCheck
        fprintf ( '\nHomography FAILED to pass the Check!\n' );
        return;
    else
        fprintf ( '\nOverlapping image Area      =  %u%%', ...
                  fix(ResultsFIM.OverlapArea*100) );
        fprintf ( '\nHomography passed the Check!' );
    end
end
fprintf('\n');