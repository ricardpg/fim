%  SaveFIM ----------------------------------------------------------------
%
%  Function used inside FIM to save output of FIM: Configuration structure, 
%  ResultsFIM structure and estimated Homography in ASCII format.
%
%  Author(s)     : Marina Kudinova
%  e-mail        : kudinova@eia.udg.es
%  Working Group : Computer Vision and Robotics Group
%  Homepage      : http://vicorob.udg.es/
%  Project       : FIM
%  File          : SaveFIM.m
%  Date          : 04/06/2007 - 05/06/2007
%  Compiler      : MATLAB >= 7.0
%
% -------------------------------------------------------------------------

function SaveFIM ( ResultsFIM, SavePrs )

% Checking input and output
error ( nargchk ( 2, 2, nargin ) );
error ( nargoutchk ( 0, 0, nargout ) );

if ~isstruct(SavePrs); return; end;
    
% To save displayed images set 2 as PlottingParameters
SaveResultsFIM        = SavePrs.Results;              % 1 -> save; 0 -> not save
SaveHomographyInASCII = SavePrs.HomoInASCII;          % 1 -> save; 0 -> not save
if isfield(SavePrs,'TestName');
    TestName = SavePrs.TestName;
else
    TestName = '';
end
WhereSaveResults      = SavePrs.SaveTo;

% Create the Directory with Results

if SaveResultsFIM   

    if exist(WhereSaveResults,'dir') ~= 7; mkdir ( WhereSaveResults ); end

    save ( [ WhereSaveResults, 'ResultsFIM', TestName ], ...
             '-struct', 'ResultsFIM' );
    
    % saving the estimated homography separately in ASCII format
    if isfield ( ResultsFIM, 'Homography' ) && SaveHomographyInASCII
        save ( [ WhereSaveResults, 'Homography', TestName ], '-struct', ...
                 'ResultsFIM', 'Homography', '-ascii' );
    end
end

end