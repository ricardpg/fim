%% CheckDisplayParameters
%  
%  The CheckDisplayParameters function process the structure, that
%  contains input parameters for the DisplayResultsFIM function of the FIM.
%
%  Author(s)     : Marina Kudinova, Jordi Ferrer
%  e-mail        : { kudinova, jferrerp } @ eia.udg.es
%
%  Working Group : Computer Vision and Robotics Group
%  Homepage      : http://vicorob.udg.es/
%
%  Project       : FIM
%  File          : CheckDisplayParameters.m
%  Date          : 20/01/2007 - 21/01/2007
%  Compiler      : MATLAB >= 7.0
%
%% FUNCTION

function ...
DisplayParamsChecked = CheckDisplayParameters ( DisplayParameters )

% DisplayParameters.DisplayConfig = 1;                 % 1 -> display FIM configuretion; 0 -> not display
% DisplayParameters.DisplayResults = 1;                % 1 -> display statistics of FIM execution results; 0 -> not display
% DisplayParameters = 1;                               % <- to use default display parameters
% DisplayParameters = 0;                               % <- to cancel display output of FIM

DefaultDisplayConfig = 1;
DefaultDisplayResults = 1;
DefaultSepLines = 1;

if isstruct(DisplayParameters)
       
    if isfield(DisplayParameters,'DisplayConfig') && isscalar(DisplayParameters.DisplayConfig)
        DisplayConfig = DisplayParameters.DisplayConfig;
    else
        DisplayConfig = DefaultDisplayConfig;
    end
    
    if isfield(DisplayParameters,'DisplayResults') && isscalar(DisplayParameters.DisplayResults)
        DisplayResults = DisplayParameters.DisplayResults;
    else
        DisplayResults = DefaultDisplayResults;
    end
    
    if isfield(DisplayParameters,'SepLines') && isscalar(DisplayParameters.SepLines)
        SepLines = DisplayParameters.SepLines;
    else
        SepLines = DefaultSepLines;
    end

elseif isnumeric(DisplayParameters) 
    
    if DisplayParameters == 0
        DisplayParamsChecked = 0;
        return;
    else
        DisplayConfig = DefaultDisplayConfig;
        DisplayResults = DefaultDisplayResults;
        SepLines = DefaultSepLines
    end
else
    error ( 'MATLAB:FIM:Input', ...
        [ 'Incorrect DisplayParameters input: can be 0, 1 or structure \n', ...
        'with DisplayConfig and DisplayResults fields. \n'] );
end

DisplayParamsChecked.DisplayConfig  = DisplayConfig;
DisplayParamsChecked.DisplayResults = DisplayResults;
DisplayParamsChecked.SepLines       = SepLines;
