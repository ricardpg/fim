%  DefaultParameters ------------------------------------------------------
%
%  Function fills the input structure of FIM with default parameters. 
%
%  Author(s)     : Marina Kudinova
%  e-mail        : kudinova@eia.udg.es
%  Working Group : Computer Vision and Robotics Group
%  Homepage      : http://vicorob.udg.es/
%  Project       : FIM
%  File          : SaveFIM.m
%  Date          : 05/06/2007 - 05/06/2007
%  Compiler      : MATLAB >= 7.0
%
% -------------------------------------------------------------------------

function InputFIM = DefaultParameters

% Preprocessing -----------------------------------------------------------

InputFIM.Processing.SingleChannel = 4;                % 0-no, 1-red, 2-green, 3-blue, 4-Y channel of YIQ, 5-PCA
InputFIM.Processing.Equalization  = 1;                % 0-no, 1-normalization, 2-equalization, 3-CLAHE
InputFIM.Processing.ResizeCoeff   = 1;                % resize coefficient to decrease sizes of images

% Detection Parameters ----------------------------------------------------

Detectors = { 'SIFT',   'SURF', ...                   % 1 - 2  <- scale-invariant detectors
              'Harris', 'Hessian', 'Laplacian', ...   % 3 - 5  <- non-scaleinvariant detectors
              'MSER' };                               % 6      <- region detector
          
Descriptors = { 'SIFT', 'SURF', 'SURF-128', ...       % 1 - 3
                'SURF-36', 'U-SURF', ...              % 4 - 5
                'U-SURF-128', 'U-SURF-36' };          % 6 - 7

InputFIM.Detection.DetType          = Detectors(2);
InputFIM.Detection.DescType         = Descriptors(2); % choose the descriptor.
InputFIM.Detection.MaxNumber        = 5000;           % max number of features to detect
InputFIM.Detection.RadiusNonMaximal = 10;             % radius for non-maximal suppression for detector
InputFIM.Detection.Sigma            = 1.2;            % sigma of the Gaussian used in Harris, Laplacian and Hessian detectors; If = 0, no smoothing is performed
InputFIM.Detection.Threshold        = 0;              % [0-1] cornerness threshold; [30] MSER min region area in pix

% Descriptor Parameters ---------------------------------------------------

InputFIM.Description.DescType = Descriptors(2); 

% Matching Parameters -----------------------------------------------------

InputFIM.Matching.Matcher       = 'Sift';
InputFIM.Matching.SiftRatio     = 1.5;               % ratio between the next closest and closest nearest neighbour for SIFT matching ; the bigger is the ratio, the smaller is the amount of matches    
InputFIM.Matching.Bidirectional = 1;                 % If ~= 0 Intersection between "image1 to image2" and "image2 to image1" matches

% Motion Estimation Parameters -------------------------------------------

HomographyModels = { 'Euclidean', 'Similarity', ...  % 1 - 2
                     'Affine', 'Projective' };       % 3 - 4
                 
InputFIM.MotionEstimation.HomographyModel = HomographyModels(4);
InputFIM.MotionEstimation.SpatialStdev    = 4;       % stdev of spatial coord of features, in pixels

% Save Parameters ---------------------------------------------------------

InputFIM.Save.Results       = 1;                     % 1 - save; 0 - do not save
InputFIM.Save.HomoInASCII   = 1;                     % 1 - save; 0 - do not save
InputFIM.Save.TestName      = '';
InputFIM.Save.SaveTo        = './ResultsFIM/';

% Display Parameters ------------------------------------------------------

InputFIM.Display.DisplayConfig  = 1;                 % 1 -> display FIM configuretion; 0 -> not display
InputFIM.Display.DisplayResults = 1;                 % 1 -> display statistics of FIM execution results; 0 -> not display
InputFIM.Display.SepLines       = 1;                 % 1 -> display separation lines; 0 -> not display
