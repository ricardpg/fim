
#include <math.h>
#include "mex.h"

void mexFunction ( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] ){
	
	double *out, *H, z, coorx, coory;
    	float  h, w;
	double contain = 0;
	double total = 0;
	int x, y, stepx, stepy;
   
	/* parameter checks */
	if ((nrhs != 3) || (nlhs != 1)) {
		mexErrMsgTxt ("Usage: [ overlap ] = mosaic_nodes_overlap( H, h, w )\n\n");
		return;
	}
	
	/* reading the parameters */
	H = (double *) mxGetPr (prhs [0]);
	h = mxGetScalar(prhs[1]);
	w = mxGetScalar(prhs[2]);
	
	/* require memory for return */
	plhs [0] = mxCreateDoubleMatrix (1, 1, mxREAL);
	out = (double *) mxGetPr (plhs [0]);

	/* printf("%f %f %f %f %f %f %f %f",H[0],H[1],H[2],H[3],H[4],H[5],H[6],H[7]);*/
		
	/* Compute the overlap */ 
	stepx = (int) w/20;
	stepy = (int) h/20;
	for (x=1; x<=w; x++) /*x+=stepx)*/
        for (y=1; y<=h; y++) /*y+=stepy)*/
		{
			z=(H[2]*x) + (H[5]*y) + 1;
			coorx=((H[0]*x) + (H[3]*y) + H[6])/z;
			coory=((H[1]*x) + (H[4]*y) + H[7])/z;
			if (coorx<=w && coorx>=1 && coory<=h && coory>=1)
				++contain;
			++total;
            /* printf("\n %d %d %f %f",x,y,z,coorx); */
		}

   	out[0] =  contain/total;

   	return;
}

