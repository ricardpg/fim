%
% F I M -------------------------------------------------------------------
%
%  FIM = Feature-based Image Matching. FIM takes 2 images as an input,
%  converts each image to grayscale if they are color and enhances their 
%  contrast if specified. Afterwards at each image the salient points are
%  detected. Next a descriptor is calculated for each point. Calculated 
%  descriptors are matched between two images and on the basis of initial 
%  correspondences the motion between images is estimated. The mismatched 
%  correspondences are rejected when estimating the motion. 
%
%  U S A G E :
%
%  [ Img1, Img2, ResultsFIM ] = FIM ( Image1, Image2, Mask, InputFIM );
%
%  I N P U T :
%
%  Image1   - first image to br matched. Image1 and Image2 are to be of the
%             same size.
%  Image2   - second image to br matched.
%  Mask     - matrix of zeros and ones, the sam esize as both images. 
%             Images areas corresponding to zeros will not be processed.
%  InputFIM - Structure with all FIM parameters. Parameters are divided 
%             into groups, hence InputFIM structure consists of Processing, 
%             Detection, Description, Matching, MotionEstimation, Save and 
%             Display substructures. Setting the whole substructure equal 
%             to zero, for example: "InputFIM.Matching = 0" cancels the 
%             corresponding FIM step.
%
%  O U T P U T :
%
%  Img1, Img2 - processed images where the detection and description were 
%               actually performed.
%  ResultsFIM - structure containing the results of FIM execution. This
%               structure consists of next fields: Img1Features, 
%               Img2Features, Img1Descriptors, Img2Descriptors, 
%               InitialMatches, Homography, AcceptedMatches, 
%               RejectedMatches, AllFollowingMatches, OverlapArea, 
%               HomoCheck. ResultsFIM are saved to the disk according to 
%               the specified InputFIM.Save parameters.
%
%--------------------------------------------------------------------------

function [ Img1, Img2, ResultsFIM ] = FIM ( Image1, Image2, Mask, InputFIM )

% Check Input Output Arguments --------------------------------------------

error ( nargchk ( 3, 4, nargin ) );
error ( nargoutchk ( 2, 3, nargout ) );

% Process the InputFIM structure of parameters ----------------------------

if nargin == 3 || ~isstruct(InputFIM)
    InFIM = DefaultParameters;
else
    InFIM = CheckInputParameters ( Image1, Image2, Mask, InputFIM );
end

ProcessingPrs        = InFIM.Processing;
DetectionPrs         = InFIM.Detection;
DescriptionPrs       = InFIM.Description;
MatchingPrs          = InFIM.Matching;
MotionEstimationPrs  = InFIM.MotionEstimation;
DisplayPrs           = InFIM.Display;
SavePrs              = InFIM.Save;

% Displaying FIM Configuration --------------------------------------------

EndLine = 0;
if isstruct(DisplayPrs)
   DisplayPrs.Display = 1;
    if DisplayPrs.SepLines
        fprintf ( [ '\n-- FIM ', repmat('-',1,65), '\n' ] );
        EndLine = 1;
    end
    DisplayConfiguration ( InFIM );
else
    DisplayPrs.Display = 0;
end

% Preprocessing of Images -------------------------------------------------

Img1 = ProcessImage ( Image1, ProcessingPrs );
Img2 = ProcessImage ( Image2, ProcessingPrs );

% Initializing output ResultsFIM ------------------------------------------

ResultsFIM.Configuration        =  InFIM;
ResultsFIM.Img1Features         =  [];
ResultsFIM.Img2Features         =  [];
ResultsFIM.Img1Descriptors      =  [];
ResultsFIM.Img2Descriptors      =  [];
ResultsFIM.InitialMatches       =  [];
if strcmpi ( MotionEstimationPrs.MotionModel, '2D' ) 
    ResultsFIM.Homography       =  [];
else
    ResultsFIM.Fundamental      =  [];
end
ResultsFIM.AcceptedMatches      =  [];
ResultsFIM.RejectedMatches      =  [];
ResultsFIM.AllFollowingMatches  =  [];
ResultsFIM.OverlapArea          =  0;
ResultsFIM.HomoCheck            =  0;

% Detection of Keypoints --------------------------------------------------

% Checking the Parameters
if ~isstruct(DetectionPrs)
    DisplayPrs.Detection = 0;
    DisplayResultsFIM ( DisplayPrs, ResultsFIM );
    if EndLine; fprintf(['\n',repmat('-',1,72),'\n\n']); end;
    SaveFIM ( ResultsFIM, SavePrs );
    return;
else
    DisplayPrs.Detection = 1;
end

% Detecting the keypoints
[ Img1Features, Img1RegionsMSER ] = DetectFeatures ( Img1, Mask, DetectionPrs );
[ Img2Features, Img2RegionsMSER ] = DetectFeatures ( Img2, Mask, DetectionPrs );

% Putting the Detected keypoints to the FIM output structure
if ~isempty(Img1Features);
    ResultsFIM.Img1Features = Img1Features;
end

if ~isempty(Img2Features);
    ResultsFIM.Img2Features = Img2Features;
end

if ~isempty ( Img1RegionsMSER )
    ResultsFIM.Img1RegionsMSER = Img1RegionsMSER;
end

if ~isempty ( Img2RegionsMSER )
    ResultsFIM.Img2RegionsMSER = Img2RegionsMSER;
end

if isempty(Img1Features) || isempty(Img2Features)
    DisplayResultsFIM ( DisplayPrs, ResultsFIM );
    if EndLine; fprintf(['\n',repmat('-',1,72),'\n\n']); end;
    SaveFIM ( ResultsFIM, SavePrs );
    return;
end;

% Computing the Keypoint Descriptors --------------------------------------

% Check if Description is cancelled
if ~isstruct(DescriptionPrs)
    DisplayPrs.Description = 0;
    DisplayResultsFIM ( DisplayPrs, ResultsFIM );
    if EndLine; fprintf(['\n',repmat('-',1,72),'\n\n']); end;
    SaveFIM ( ResultsFIM, SavePrs );
    return;
else
    DisplayPrs.Description = 1;
end

% Caluclation of the Descriptors
Img1Descriptors = CalculateDescriptor ( Img1, Img1Features, DescriptionPrs );
Img2Descriptors = CalculateDescriptor ( Img2, Img2Features, DescriptionPrs );

% Filling the FIM output structure
ResultsFIM.Img1Descriptors = Img1Descriptors;
ResultsFIM.Img2Descriptors = Img2Descriptors;

if isempty(Img1Descriptors) || isempty(Img2Descriptors)
    DisplayResultsFIM ( DisplayPrs, ResultsFIM );
    if EndLine; fprintf(['\n',repmat('-',1,72),'\n\n']); end;
    SaveFIM ( ResultsFIM, SavePrs );
    return
end

% Initial Matching of Descriptors -----------------------------------------

% Check if matching is cancelled
if ~isstruct(MatchingPrs)
    DisplayPrs.Matching = 0;
    DisplayResultsFIM ( DisplayPrs, ResultsFIM );
    if EndLine; fprintf(['\n',repmat('-',1,72),'\n\n']); end;
    SaveFIM ( ResultsFIM, SavePrs );
    return
else
    DisplayPrs.Matching = 1;
end

% Performing Initial Matching
InitialMatches = InitialMatch ( Img1Descriptors, Img2Descriptors, MatchingPrs );

% Filling the FIM output structure
if ~isempty(InitialMatches)
    ResultsFIM.InitialMatches = InitialMatches ( 1:2,: );
else
    DisplayResultsFIM ( DisplayPrs, ResultsFIM );
    if EndLine; fprintf(['\n',repmat('-',1,72),'\n\n']); end;
    SaveFIM ( ResultsFIM, SavePrs );
    return
end

% HOMOGRAPHY estimation & Rejection of outliers (using RANSAC) ------------

% Check if Motion Estimation is cancelled
if ~isstruct(MotionEstimationPrs)
    DisplayPrs.MotionEstimation = 0;
    DisplayResultsFIM ( DisplayPrs, ResultsFIM );
    if EndLine; fprintf(['\n',repmat('-',1,72),'\n\n']); end;
    SaveFIM ( ResultsFIM, SavePrs );
    return
else
    DisplayPrs.MotionEstimation = 1;
end
    
% Running Motion Estimation
Homography = [] ;
Fundamental = [] ;
if strcmpi ( MotionEstimationPrs.MotionModel, '2D' ) 
    [ Homography, inliers ] = EstimateMotion2D ( Img1Features, ...
                                                 Img2Features, ...
                                                 InitialMatches, ...
                                                 MotionEstimationPrs );
elseif strcmpi ( MotionEstimationPrs.MotionModel, '3D' ) 
    [ Fundamental, inliers ] = EstimateMotion3D ( Img1Features, ...
                                                  Img2Features, ...
                                                  InitialMatches, ...
                                                  MotionEstimationPrs );
else
    error( '' ) ;
end
  

% Filling the FIM output structure
if ~isempty ( Homography ),
    ResultsFIM.Homography = Homography;
elseif ~isempty ( Fundamental ),
    ResultsFIM.Fundamental = Fundamental;
else
    DisplayResultsFIM ( DisplayPrs, ResultsFIM );
    if EndLine; fprintf(['\n',repmat('-',1,72),'\n\n']); end;
    SaveFIM ( ResultsFIM, SavePrs );
    return
end

if isempty(inliers)
    ResultsFIM.RejectedMatches = InitialMatches;
else
    outliers = setdiff (1:size(InitialMatches,2), inliers);
    ResultsFIM.AcceptedMatches = InitialMatches ( :, inliers );
    ResultsFIM.RejectedMatches = InitialMatches ( :, outliers );
end

if strcmpi ( MotionEstimationPrs.MotionModel, '2D' ),

  % Homography Check & Calculation of the overlapping Area
  HomoCheck = homographyLooksGood(Homography);
  OverlapArea = mosaic_nodes_overlap(Homography,size(Img1,1),size(Img1,2));
  ResultsFIM.HomoCheck = (HomoCheck && (OverlapArea>0.2) && (OverlapArea<2));
  ResultsFIM.OverlapArea = OverlapArea * ResultsFIM.HomoCheck;

  % Guided PostMatching
  % Homography projects features from Image2 to Image1
  if HomoCheck && MatchingPrs.GuidedPostMatching
     ResultsFIM.AllFollowingMatches = GuidedMatch ...
     (Homography,MotionEstimationPrs.SpatialStdev,Img2Features,Img1Features);
  end
  
end

% Displaying the Statistics of the Results
DisplayResultsFIM ( DisplayPrs, ResultsFIM );
if EndLine; fprintf(['\n',repmat('-',1,72),'\n\n']); end;

% Saving the Results of FIM 
SaveFIM ( ResultsFIM, SavePrs );

end
