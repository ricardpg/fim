%% SetPathsFIM ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%  
%  SetPathsFIM function adds paths to all FIM components and functions at
%  the end of the Matlab search path. Input of the function is the path to
%  the main FIM folder from the directory, from which FIM is called.
%
%  Author        : Marina Kudinova
%  e-mail        : kudinova@eia.udg.es
%  Working Group : Computer Vision and Robotics Group
%  Homepage      : http://vicorob.udg.es/
%  File          : SetPathsFIM.m
%  Project       : FIM
%  Date          : 25/01/2007 - 25/01/2007
%  Compiler      : MATLAB > 7.0
%
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function SetPathsFIM ( PathToFIM )

error ( nargchk ( 0, 1, nargin ) );

if nargin == 0, 
    PathToFIM = '.'; 
    fprintf ( '   By default the current folder is used as FIM folder.' )
    fprintf ( [ '\n   To run FIM from external folder specify its path as '...
                'an input of SetPathsFIM function: '...
                '\n   >> SetPathsFIM(''D:/Software/FIM'')\n' ])
end
    
% Detect Platform
Arch = computer;

if strcmpi ( Arch, 'pcwin' ); Windows = 1; Nbit = 32;
elseif strcmpi ( Arch, 'pcwin64' ); Windows = 1; Nbit = 64;
else Windows = 0; 
end

if Windows && Nbit == 64, 
  PtSIFT         =  [ PathToFIM, '/0_3rdParty/sift-0.9.19' ]; % --> (Ricard Campos): Version of SIFT that works on win64
else
  PtSIFT         =  [ PathToFIM, '/0_3rdParty/sift-0.9.8' ]; % --> (Ricard Campos): Original FIM's SIFT that works on win32
end
PtSURF           =  [ PathToFIM, '/0_3rdParty/surf' ];
PtRANSAC         =  [ PathToFIM, '/0_3rdParty/ransac' ];
PtPreprocessing  =  [ PathToFIM, '/1_Preprocessing' ];
PtDetection      =  [ PathToFIM, '/2_Detection' ];
PtDescription    =  [ PathToFIM, '/3_Description' ];
PtMatching       =  [ PathToFIM, '/4_Matching' ];
PtMotion2D       =  [ PathToFIM, '/5_MotionEstimation/2D' ];
PtMotion3D       =  [ PathToFIM, '/5_MotionEstimation/3D' ];
PtFIM            =  [ PathToFIM, '/6_FIM' ];
PtPlotting       =  [ PathToFIM, '/7_Plotting' ];
PtMosaic         =  [ PathToFIM, '/8_Mosaic' ];

%% Paths at the end of the list
% addpath ( PtSIFT,   PtPreprocessing,  PtMatching,   PtFIM,       ...
%           PtSURF,   PtDetection,      PtMotion2D,   PtPlotting,  ...
%           PtRANSAC, PtDescription,    PtMotion3D,   PtMosaic,    ...
%           '-END' );
%% Paths at the top of the list
addpath ( PtSIFT,   PtPreprocessing,  PtMatching,   PtFIM,       ...
          PtSURF,   PtDetection,      PtMotion2D,   PtPlotting,  ...
          PtRANSAC, PtDescription,    PtMotion3D,   PtMosaic ) ;
      
savepath ;
      
end