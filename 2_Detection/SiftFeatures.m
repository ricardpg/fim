% SiftFeatures ------------------------------------------------------------
%
%  SiftFeatures detecs SIFT keypoints.
%
%  Autor(s)      : Jordi Ferrer Plana, Olivier Delaunoy, Marina Kudinova
%  e-mail        : {jferrerp,delaunoy,kudinova}@eia.udg.es
%  Branch        : Computer Vision
%  Working Group : Underwater Vision Lab
%  Project       : FIM
%  Homepage      : http://porcsenglar.udg.es
%  Module        : Detect Sift Keypoints.
%  File          : SiftFeatures.m
%  Date          : 06/10/2006 - 19/06/2007
%  Compiler      : MATLAB >= 7.0
%
% -------------------------------------------------------------------------
%
%  Copyright (C) 2005-2006 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -------------------------------------------------------------------------
%
%  Usage:
%
%      Frames = SiftFeatures ( InputImg, Mask, N, Threshold, Radius )
%
%  Input Parameters:
%
%      InputImg:  Source MxN graylevel image.
%      Mask:      Source MxN or empty matrix masking with 0 the non wanted 
%                 x,y locations.
%      MaxNumber: Maximum number of features.
%      Threshold: Threshold used to reject poorer features.
%      Radius:    Radius for the non-maximal suppression.
%
%  Output Parameters:

%      Frames:    6xN Matrix containg the keypoints (X, Y, Scale, 
%                 Orientation, Octave and Goodness).
%
% -------------------------------------------------------------------------

function Frames = SiftFeatures ( InputImg, Mask, MaxNumber, ...
                                 Threshold, Radius )

% Test the input parameters
error ( nargchk ( 5, 5, nargin ) );
error ( nargoutchk ( 1, 1, nargout ) );

[h w] = size ( InputImg );

if ~isempty ( Mask ),
    if any ( size ( InputImg ) - size ( Mask ) ),
        error ( 'MATLAB:SiftFeatures:Input', ...
            'If Mask is used, size must be the same than the image!' );
    end
else
    Mask = true(h, w);
end

% SIFT ( Andrea Vedaldi Source -
%        http://www.cs.ucla.edu/~vedaldi/code/sift/sift.html )
% Call with Lowe Implementation compatibility

% -- modification --
% OctMin = 0;
% Oct = 4;  % Up to Images of 8x8 pixels
% [FramesSift, DescS, GssS, DoGssS ] = sift ( InputImg, ...
%                                            'firstoctave', OctMin, ...
%                                            'numoctaves', Oct, ...
%                                            'Threshold', Threshold, ...
%                                            'Verbosity', 0 );
% -- modification --

% -- original --
% Set Lowe Threshold
[FramesSift, DescS, GssS, DoGssS ] = sift ( InputImg, ...
                                           'Threshold', Threshold, ...
                                           'Verbosity', 0 );
% -- original --

FramesS(1:4,:) = FramesSift(1:4,:);
FramesS(5,:)   = FramesSift(6,:);     % put goodness as row 5
FramesS(6,:)   = FramesSift(5,:);     % put octave as row 6

% Spreading the Keypoints
Frames = SpreadFeatures ( FramesS, 5, h, w, Mask, Radius, MaxNumber, ...
                          Threshold );                      

% -------------------------------------------------------------------------

