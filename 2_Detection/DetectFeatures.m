
% DetectFeatures
%
%  Autors        : Jordi Ferrer Plana, Kudinova Marina
%  e-mail        : {jferrerp,kudinova}@eia.udg.es
%  Branch        : Computer Vision
%  Working Group : Underwater Vision Lab
%  Project       : FIM
%  Homepage      : http://porcsenglar.udg.es
%  Module        : Detect Keypoints in an input image.
%  File          : DetectFeatures.m
%  Date          : 13/08/2006 - 26/06/2007
%  Compiler      : MATLAB >= 7.0
%  Notes         : - File writen in ISO-8859-1 encoding.
%
% -------------------------------------------------------------------------
%
%  Copyright (C) 2005-2006 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -------------------------------------------------------------------------
%
%  Usage:
%
%  DETECTFEATURES Detect points of interest in an Image using a particular
%  detector.
%
%  Frames = DetectFeatures ( InputImg, MaskIn, DetectionParameters )
%
%  Input Parameters:
%
%  InputImg: Source MxN graylevel image.
%  Mask: MxN or empty mask matrix. If Mask is empty the whole image is
%        processed, otherwise each position indicates whether a pixel
%        is processed or not.
%  DetectionParameters: Structure with the configuration for the detection:
%  MaxNumber: Is used as the maximum number of keypoints when is
%             allowed by the detector specifyed by DetType.
%  DetType: Detector type: ( 'Harris', 'Laplacian', 'Hessian', 'Sift' ).
%  RadiusNonMaximal: Radius of the non-maximal suppression window.
%
%  Output Parameters:
%  Structure Features with the default field Frames for all the detectors
%  and optional field RegionsMSER if MSER detector is used.
%  Frames: Set of 5xK frames computed in the source image InputImg.
%          this frames will store the X, Y, Scale, Orientation and Octave.
%  RegionsMSER: Structure with the field Boundary containg Boundary for
%               each detected MSER in order they gravity centers are in the
%               Frames matrix. Boundary is stored as a global index to the
%               image array.
%
% -------------------------------------------------------------------------

function [ Frames, Regions ] = DetectFeatures ( InputImg, MaskIn, ...
                                                DetectionParameters )

% Test the input parameters
error ( nargchk ( 3, 3, nargin ) );
error ( nargoutchk ( 1, 2, nargout ) );

Regions = [];

% Check the DetectionParameters structure

if nargin == 2
    DetectionParameters = 1;
end

DetectParamsChecked = CheckDetectionParameters ( DetectionParameters );

if ~isstruct ( DetectParamsChecked )
    Frames = [];
    return;
else
    % Load Parameters from Input structure
    MaxNumber        = DetectParamsChecked.MaxNumber;
    DetType          = DetectParamsChecked.DetType;
    Sigma            = DetectParamsChecked.Sigma;
    RadiusNonMaximal = DetectParamsChecked.RadiusNonMaximal;
    MinCornerness    = DetectParamsChecked.MinCornerness;
    Descriptor       = DetectParamsChecked.DescType;
    MinArea          = DetectParamsChecked.MinArea;
end

Descriptors = { 'SIFT', 'SURF', 'SURF-128', ...       % 1 - 3
                'SURF-36', 'U-SURF', ...              % 4 - 5
                'U-SURF-128', 'U-SURF-36', ...        % 6 - 7
                'ImagePatch', 'RotatedImagePatch' };  % 8 - 9      

% Check input image dimensions
sErr = ndims ( InputImg ) ~= 2;

ImSize = size ( InputImg );
MaskSize = size ( MaskIn );

% Adjusting Mask to an image if it is Bigger
if all ( ( MaskSize - ImSize ) >= 0 )
    Mask = imcrop ( MaskIn, [ 0, 0, ImSize(2), ImSize(1) ] );
else
    Mask = MaskIn;
end

% Compute the Sizes
if sErr || ~isempty(Mask) && ...
        ( ndims(Mask)~=2 || any(size(InputImg)-size(Mask)) );
    error ( 'MATLAB:DetectFeatures:Input', ...
        'Incorrect Input matrix sizes!' );
end

% HARRIS ------------------------------------------------------------------
 
if strcmpi ( DetType, 'harris' )
    
    % Search for Harris Corners in the Source Image
    FramesS = HarrisFeatures ( InputImg, Mask, Sigma, MaxNumber, ...
                               MinCornerness, RadiusNonMaximal, 1 );
    % Calculate orientation for Harris corners
    Frames = CalculateOrientation ( InputImg, FramesS, Descriptor );
        
% LAPLACIAN ---------------------------------------------------------------
    
elseif strcmpi ( DetType, 'laplacian' )
    
    % Search for Laplacian Features in the Source Image
    FramesS = LaplacianFeatures ( InputImg, Mask, Sigma, MaxNumber, ...
                                  MinCornerness, RadiusNonMaximal, 1 );
    % Calculate orientation
    Frames = CalculateOrientation ( InputImg, FramesS, Descriptor );

% HESSIAN -----------------------------------------------------------------

elseif strcmpi ( DetType, 'hessian' )

    % Search for Laplacian Features in the Source Image
    FramesS = HessianFeatures ( InputImg, Mask, Sigma, MaxNumber, ...
                                MinCornerness, RadiusNonMaximal, 1 );
    % Calculate orientation
    Frames = CalculateOrientation ( InputImg, FramesS, Descriptor );

% MSER --------------------------------------------------------------------
    
elseif strcmpi ( DetType, 'mser' ),
    % Search for MSER Features in the Source Image
    [ FramesS, Regions ] = MserFeatures ( InputImg, Mask, MaxNumber, ...        
                                         MinArea, RadiusNonMaximal );
    % Calculate orientation
    Frames = CalculateOrientation ( InputImg, FramesS, Descriptor );

% SIFT --------------------------------------------------------------------
                           
elseif strcmpi ( DetType, 'sift' ),
    % Don't allow combination sift & surf
    if strcmpi ( Descriptor, 'sift' ),
        % Search for Sift Features in the Source Image
        Frames = SiftFeatures ( InputImg, Mask, MaxNumber, ...
                                MinCornerness, RadiusNonMaximal );
    else
        error ( 'MATLAB:DetectFeatures:Input', ...
              [ 'Invalid combination of a SIFT detector and other ', ...
                'descriptor (Scale is not yet compatible).' ] );
    end

% SURF --------------------------------------------------------------------

elseif strcmpi ( DetType, 'surf' ),

    % If the descrriptor is one of the SURF descriptors
    if sum ( strcmpi ( Descriptor, Descriptors(2:size(Descriptors,2)) ) )         
        % Search for Surf Features in the Source Image
        Frames = SurfFeatures ( InputImg, Mask, MaxNumber, ...
                                MinCornerness, RadiusNonMaximal );
    else
        error ( 'MATLAB:DetectFeatures:Input', ...
              [ 'Invalid combination of a SURF detector and other ', ...
                'descriptor (Scale is not yet compatible).' ] );
    end
    
% Unknown detector --------------------------------------------------------
    
else
    error ( 'MATLAB:DetectFeatures:Input', ...
          [ 'Invalid type of detector. \n',...
            'Available: HARRIS, LAPLACIAN, HESSIAN, SIFT, SURF, MSER' ] );
end

% -------------------------------------------------------------------------
