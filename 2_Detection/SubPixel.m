%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.es
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.es
%
%  Module        : Compute subpixel accuracy using the neighborhood.
%
%  File          : SubPixel.m
%  Date          : 18/10/2006 - 02/03/2007
%
%  Compiler      : MATLAB
%  Libraries     : -
%
%  Notes         : - File written in ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2006 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  SUBPIXEL Compute subpixel accuracy using the neighborhood.
%
%     [ROffs, COffs] = SubPixel ( C, Rows, Columns, Offset )
%
%     Input Parameters:
%      C: Source NxM input matrix with the function C(y,x).
%      Rows: Vector Kx1 values representing the y coordinate of the central
%            point to be interpolated.
%      Columns: Vector Kx1 values representing the x coordinate of the
%               central point to be interpolated.
%      Offset: Scalar value representing the the distance from the central
%              point that will be used to take the window of source data
%              to compute the interpolation.
%
%     Output Parameters:
%      [ROffs, COffs]: Two vectors Kx1 representing the y and x offsets
%                      from the central points represented by the column
%                      vectors [Rows, Columns].
%

function [ROffs, COffs] = SubPixel ( C, Rows, Columns, Offset )
% Test the input parameters
error ( nargchk ( 4, 4, nargin ) );
error ( nargoutchk ( 2, 2, nargout ) );

% Check sizes
[rr, cr] = size ( Rows );
[rc, cc] = size ( Columns );
[Crows, Ccols] = size (C);

if ( cr ~= 1 || cc ~= 1 || rr == 0 || rr ~= rc  )
    error ( 'MATLAB:SubPixel:InputOutput', ...
        'Input matrices sizes does not match!' );
end

% f(x,y) = Ax^2 + By^2 + Cxy + Dx + Ey
%
% NOTES: - The F coefficient is not used, the data is translated.
%
% For instance, a 3x3 neighborhood:
%
%    ( -1, -1 ) (  0, -1 ) (  1, -1 )
%    ( -1,  0 ) (  0,  0 ) (  1,  0 )
%    ( -1,  1 ) (  0,  1 ) (  1,  1 )
%
% Pixels are taken in rows:
%
%                         | 1  1  1 -1 -1 |
%                         | 0  1  0  0 -1 |   | A |
%      | A |              | 1  1 -1  1 -1 |   | B |
%      | B |              | 1  0  0 -1  0 | � | C | = C(x,y)
%  M � | C | = C(x,y)     | 1  0  0  1  0 |   | D |
%      | D |              | 1  1 -1 -1  1 |   | E |
%      | E |              | 0  1  0  0  1 |
%                         | 1  1  1  1  1 |

% Compute window range
k = -Offset : Offset;

% Create a meshgrid of offsets (x, y) coordinates are inverted to avoid
% further transpose computations
[y, x] = meshgrid ( k, k );

% Compute the variables: x^2, y^2, xy, x, y
%
x2 = x .* x;
y2 = y .* y;
xy = x .* y;

% Compute half of M size
h = floor ( (2*Offset+1)^2 / 2 );

% Compute the offset matrix of the neighborhood
%
M = [ x2(:) y2(:) xy(:) x(:) y(:) ];

% Suppress the midle point
M = [ M(1:h,:); M(h+2:end,:) ];

for i = 1 : rr

    if (Rows(i)-Offset) > 0 && (Rows(i)+Offset) <= Crows && ...
            (Columns(i)-Offset) > 0 && (Columns(i)+Offset) <= Ccols

        % Get the neighborhood and transpose to be able to use linear indices
        % Center the data (there is no F coeficient in the Surface equation)
        b = C ( Rows(i)-Offset : Rows(i)+Offset, ...
            Columns(i)-Offset : Columns(i)+Offset )' ...
            - C( Rows(i), Columns(i));

        % Delete Middle point
        b = [b(1:h) b(h+2:end)];

        % Now, compute the vector of coeficients using (Least-Squares)
        w = M \ b(:);

        % Solve maximum of the 3D surface:
        %
        %  d f(x,y)        d f(x,y)
        %  -------- = 0;   -------- = 0;
        %     dx              dy
        %
        %            | 2Ax + Cy + D |
        %  f'(x,y) = |              |
        %            | 2By + Cx + E |
        %
        %  2Ax +  Cy + D = 0 \    | 2A   C |   | x |   | -D |
        %   Cx + 2By + E = 0 /    |  C  2B | � | y | = | -E |

        % Solve the maximum of f(x,y)
        %
        K = [ 2*w(1) w(3); w(3) 2*w(2) ];
        c = [ -w(4); -w(5) ];
        Y = K \ c;

        % Protect against errors
        if abs ( Y(1) ) > 0.5 || abs ( Y(2) ) > 0.5
            Y = [0; 0];
        end

        ROffs(i,1) = Y(2);
        COffs(i,1) = Y(1);
        
    else
        
        ROffs(i,1) = 0;
        COffs(i,1) = 0;

    end
end
