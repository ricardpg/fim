function [ feat nb ] = ReadFeaturesMSER(file)
error ( nargchk ( 1, 1, nargin ) );
error ( nargoutchk ( 1, 2, nargout ) );
fid = fopen(file, 'r');
dim = fscanf(fid, '%f',1);
if dim == 1
dim = 0;
end
nb = fscanf ( fid, '%d', 1 );
feat = fscanf ( fid, '%f', [ 5+dim, inf ] );
fclose(fid);
end
