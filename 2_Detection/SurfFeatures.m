% -------------------------------------------------------------------------
%
%  Autor(s)      : Jordi Ferrer Plana, Olivier Delaunoy, Marina Kudinova
%  e-mail        : {jferrerp,delaunoy,kudinova}@eia.udg.es
%  Branch        : Computer Vision
%  Working Group : Underwater Vision Lab
%  Project       : FIM
%  Homepage      : http://porcsenglar.udg.es
%  Module        : Detect Surf Keypoints
%  File          : SurfFeatures.m
%  Date          : 15/01/2007 - 19/06/2007
%  Compiler      : MATLAB >= 7.0
%
% -------------------------------------------------------------------------
%
%  Copyright (C) 2005-2006 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -------------------------------------------------------------------------
%
%  Usage:
%
%  SurfFeatures Detect surf keypoints.
%
%     Frames = SurfFeatures ( InputImg, Mask, MaxNumber, Threshold, Radius )
%
%  Input Parameters:
%
%      InputImg:  Source MxN graylevel image.
%      Mask:      Source MxN or empty matrix masking with 0 the non wanted 
%                 x,y locations.
%      MaxNumber: Maximum number of features.
%      Threshold: Threshold used to reject poorer features.
%      Radius:    Radius for the non-maximal suppression.
%
%  Output Parameters:
%
%      Frames:    4xN Matrix containg the keypoints: X, Y, Scale and 
%                 Strength (Goodness).
%
% -------------------------------------------------------------------------

function Frames = SurfFeatures ( InputImg, Mask, MaxNumber, Threshold, Radius )

% Test the input parameters
error ( nargchk ( 5, 5, nargin ) );
error ( nargoutchk ( 1, 1, nargout ) );

[h w] = size ( InputImg );

if ~isempty ( Mask ),
    if any ( size ( InputImg ) - size ( Mask ) ),
        error ( 'MATLAB:SurfFeatures:Input', ...
            'If Mask is used, size must be the same than the image!' );
    end
else
    Mask = true(h, w);
end

% Calling the mex file --------------------------------------------------
% thres = 4.0;
% doubleImageSize = false;
% initLobe = 3;
% sampligStep = 2;
% octaves = 3;
% upright = false;
% extended = false;
% indexSize = 4;
% FramesS = SurfFeaturesMx ( InputImg, thres, doubleImageSize,...
%                           initLobe, sampligStep, octaves,...
%                           upright, extended, indexSize );



% Frames = SurfFeaturesMx ( InputImg, thres = 4.0, doubleImageSize = false,
%                           initLobe = 3, sampligStep = 2, octaves = 4,
%                           upright = false, extended = false, indexSize = 4 );

FramesS = SurfFeaturesMx ( InputImg );

% Spreading the Keypoints
Frames = SpreadFeatures ( FramesS, 5, h, w, Mask, Radius, MaxNumber, ...
                          Threshold );

% -------------------------------------------------------------------------
