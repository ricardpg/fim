%% MserFeatures -----------------------------------------------------------
%
%  Author        : Marina Kudinova
%  e-mail        : kudinova@eia.udg.es
%  Working Group : Computer Vision and Robotics Group
%  Homepage      : http://vicorob.udg.es/
%  File          : PlotMSER.m
%  Project       : FIM
%  Date          : 13/02/2007 - 01/03/2007
%  Compiler      : MATLAB >= 7.0
%  
% -------------------------------------------------------------------------
% 
% USAGE: 
%
% Regions = MserFeatures ( Image, Mask, MaxNumber, Threshold, Radius )
%
% INPUT:
%
% Image      - Initial Image, where MSERs will be detected.
% Threshold  - Minimal size of the MSER region.
% PathToMSER - Path to folder where mser.exe (WIN) or mser.ln (LINUX) is.
%
% OUTPUT:
%
% Rows - Y coords of the ellipse center of detected MSERs, in subpixel.
% Cols - X coords of the ellipse center of detected MSERs, in subpixel.
% Area - Area of the MSER region.
%
% -------------------------------------------------------------------------
%
%%
function ...
[ Frames, RegionsMSER ] = MserFeatures ( Image, Mask, MaxNumber, ...
                                         Threshold, Radius )

% Test the input parameters
error ( nargchk ( 5, 5, nargin ) );
error ( nargoutchk ( 1, 2, nargout ) );

% [ ImW, ImH ] = size(Image);
[ ImH, ImW ] = size(Image);

% Creation of the temorary Dir and writing the Image temporary to disk
if ~exist('TempMSER','dir'); mkdir TempMSER; end
ImName = 'TempMSER/Image_MSER_FIM.jpg';
imwrite ( Image, ImName );

%% MSER

% PathToMSER = [ PathToFIM, '0_3rdParty/mser/' ];

% Detect Platform
Arch = computer;
Arch = Arch(1:5);
if strcmpi ( Arch, 'pcwin' ); Windows = 1; else Windows = 0; end

% Define right MSER options
if Windows;
    Executable = 'mser.exe';
    ImName = strrep ( ImName, '/', '\' );
    Output2 = 'TempMSER\Ellipses.aff';
    Output1 = 'TempMSER\Boundaries.extbound';
    Output0 = 'TempMSER\Regions.rle';
else 
    Executable = 'mser.ln';
    Output2 = 'TempMSER/Ellipses.aff';
    Output1 = 'TempMSER/Boundaries.extbound';
    Output0 = 'TempMSER/Regions.rle';
end

%% MSER patrameters --------------------------------------------------------

MaxRelArea   = '0.01';                      % -per (0.010) [0.010] maximum relative area | Relative to image area
EllipseScale = '1';                         % -es  (1.000) [1.000] ellipse scale, (output types 2 and 4)
MinRegSize   = mat2str ( Threshold );       % -ms  (30) [30] minimum size of output region (12400)
MinMargin    = '10';                        % -mm  (10) [10] minimum margin
OutType2     = '2';                         % t['0'(Region),'1'(Boundary),'2'(Ellipse)]
OutType1     = '1';                         % t['0'(Region),'1'(Boundary),'2'(Ellipse)]
OutType0     = '0';                         % t['0'(Region),'1'(Boundary),'2'(Ellipse)]

%% Executing MSER ----------------------------------------------------------

% Detecting Ellipses ------------------------------------------------------

Parameters = [  ' -i '   ImName         ' -o '  Output2   ...
                ' -per ' MaxRelArea     ' -es ' EllipseScale  ...
                ' -ms '  MinRegSize     ' -mm ' MinMargin ...
                ' -t '   OutType2 ];

SystemCmd = [ Executable Parameters ];
[ Status ] = system ( SystemCmd );

if Status ~= 0
    error ( 'FIM:MserFeatures:Output', 'Ellipses: MSER was not executed!' );
end

% Detecting Boundaries ----------------------------------------------------

Parameters = [  ' -i '   ImName         ' -o '  Output1   ...
                ' -per ' MaxRelArea     ' -es ' EllipseScale  ...
                ' -ms '  MinRegSize     ' -mm ' MinMargin ...
                ' -t '   OutType1 ];

SystemCmd = [ Executable Parameters ];
[ Status ] = system ( SystemCmd );

if Status ~= 0
    error ( 'FIM:MserFeatures:Output', 'Boundaries: MSER was not executed!' );
end

% Detecting Regions ----------------------------------------------------

Parameters = [  ' -i '   ImName         ' -o '  Output0   ...
                ' -per ' MaxRelArea     ' -es ' EllipseScale  ...
                ' -ms '  MinRegSize     ' -mm ' MinMargin ...
                ' -t '   OutType0 ];

SystemCmd = [ Executable Parameters ];
[ Status ] = system ( SystemCmd );

if Status ~= 0
    error ( 'FIM:MserFeatures:Output', 'Regions: MSER was not executed!' );
end

%% Reading MSER Features from files

% Reading Detected Ellipse from the file on disk --------------------------
Feat = ReadFeaturesMSER ( Output2 );
RegionsMSER_All = ReadBoundariesMSER ( Output1, ImH, ImW );

% Clearing

rmdir ( 'TempMSER', 's' );

% Sorting the MSER features in the Area decreasing order ------------------
% Ellipse equation: a(x-u)(x-u)+2b(x-u)(y-v)+c(y-v)(y-v)=1
% Area = 2*pi / sqrt(4ac-b^2);

Area = 2*pi .* ( 4*Feat(3,:).*Feat(5,:) - Feat(4,:).^2 ) .^ (-1/2);

Features = Feat ( 1:2, : );
Features(3,:) = Area; % X, Y, Area

%% Spreading the Keypoints

[ Frames, Ind ] = SpreadFeatures ( Features, 3, ImH, ImW, Mask, ...
                                   Radius, MaxNumber, Threshold );

RegionsMSER = RegionsMSER_All(Ind);

% -------------------------------------------------------------------------

