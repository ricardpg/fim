/*
  Autor(s)      : Marina Kudinova, Jordi Ferrer Plana
  e-mail        : kudinova@eia.udg.es, jferrerp@eia.udg.es
  Branch        : Computer Vision

  Working Group : Underwater Vision Lab
  Project       : -

  Homepage      : http://porcsenglar.udg.es

  Module        : Detect Surf Features wrapper.

  File          : SurfFeaturesMx.cpp
  Date          : 11/01/2007 - 01/02/2007

  Compiler      : MATLAB >= 7.0 & g++ >= 4.x
  Libraries     : -

  Notes         : - File written using ISO-8859-1 encoding.

 -----------------------------------------------------------------------------

  Copyright (C) 2005-2006 by Jordi Ferrer Plana

  This source code is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This source code is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
  more details.

 -----------------------------------------------------------------------------
*/

#include <mex.h>          // Matlab Mex Functions

#include <image.h>        // SURF Code
#include <ipoint.h>
#include <fasthessian.h>
#include <surf.h>

#define ERROR_HEADER       "MATLAB:SurfFeaturesMx:Input\n"

using namespace surf;
using namespace std;

// Identify the input parameters by it's index
enum { I=0, THRES, DOUBLEIMAGESIZE, INITLOBE, SAMPLINGSTEP, OCTAVES, UPRIGHT, EXTENDED, INDEXEDSIZE };
// Identify the output parameters by it's index
enum { FRAMES=0 };

void mexFunction ( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  // Parameter checks
  if ( ( nlhs != 1 ) || ( nrhs != 9 && nrhs != 1 ) )
    mexErrMsgTxt ( ERROR_HEADER
                   "Usage: Frames = SurfFeaturesMx ( Image, thres = 4.0, doubleImageSize = false,\n"
                   "                                 initLobe = 3, sampligStep = 2, octaves = 4,\n"
                   "                                 upright = false, extended = false, indexSize = 4 ] )");

  // Default values
//  double thres = 4.0;
  double thres = 0.0;
  bool doubleImageSize = true;
  int initLobe = 3;
  int samplingStep = 1;
  int octaves = 4;
  bool upright = false;
  bool extended = false;
  int indexSize = 4;

  // Use the provided parameters
  if ( nrhs == 9 )
  {
    if ( mxIsDouble ( prhs[THRES] ) && mxIsLogical ( prhs[DOUBLEIMAGESIZE] ) &&
         mxIsDouble ( prhs[INITLOBE] ) && mxIsDouble ( prhs[SAMPLINGSTEP] ) &&
         mxIsDouble ( prhs[OCTAVES] ) && mxIsLogical ( prhs[UPRIGHT] ) &&
         mxIsLogical ( prhs[EXTENDED] ) && mxIsDouble ( prhs[INDEXEDSIZE] ) )
    {
      thres = *(double *)mxGetPr ( prhs[THRES] );
      doubleImageSize = *(bool *)mxGetPr ( prhs[DOUBLEIMAGESIZE] );
      initLobe = (int)(*(double *)mxGetPr ( prhs[INITLOBE] ));
      samplingStep = (int)(*(double *)mxGetPr ( prhs[SAMPLINGSTEP] ));
      octaves = (int)(*(double *)mxGetPr ( prhs[OCTAVES] ));
      upright = *(bool *)mxGetPr ( prhs[UPRIGHT] );
      extended = *(bool *)mxGetPr ( prhs[EXTENDED] );
      indexSize = (int)(*(double *)mxGetPr ( prhs[INDEXEDSIZE] ));
    }
    else
      mexErrMsgTxt ( ERROR_HEADER "All the parameters must be scalars!" );
  }

  if ( mxGetNumberOfDimensions ( prhs[I] ) != 2 || !mxIsDouble ( prhs[I] ) )
    mexErrMsgTxt ( ERROR_HEADER "Image must be a NxM double matrix!" );

  // Image Dimensions
  int h = mxGetM ( prhs[I] );
  int w = mxGetN ( prhs[I] );

  // Construct initial image
  Image SrcImage ( w, h );

  // Pointer to the input Image
  double *Mi = (double *)mxGetPr ( prhs[I] );

  // Copy from Input image to the Surf Image
  for ( int x = 0; x < SrcImage.getWidth ( ); x++ )
    for ( int y = 0; y < SrcImage.getHeight ( ); y++ )
      SrcImage.setPix ( x, y, *Mi++ );

  // Create the integral image
  Image iimage ( &SrcImage, doubleImageSize );

  // Vector of Keypoints
  std::vector<Ipoint> ipts;

  // Extract interest points with Fast-Hessian
  FastHessian fh ( &iimage,         // pointer to integral image
                   ipts,            // interest point vector to be filled
                   thres,           // blob response threshold
                   doubleImageSize, // double image size flag
                   initLobe * 3,    // 3 times lobe size equals the mask size
                   samplingStep,    // subsample the blob response map
                   octaves );       // number of octaves to be analysed

  // Extract the interest points
  fh.getInterestPoints ( );

  // Create the resulting matrix X, Y, Scale and Strength (Goodness)
  // Each column is a frame
  plhs[FRAMES] = mxCreateDoubleMatrix ( 5, ipts.size ( ), mxREAL );
  double *Frames = (double *)mxGetPr ( plhs[FRAMES] );
   
  // Initialise the SURF descriptor
  Surf des ( &iimage,         // pointer to integral image
             doubleImageSize, // double image size flag
             upright,         // rotation invariance or upright
             extended,        // use the extended descriptor
             indexSize );     // square size of the descriptor window (default 4x4)
  
  // Compute the orientation and the descriptor for every interest point
  for (unsigned int n=0; n<ipts.size(); n++)     
  {
    *Frames++ = ipts[n].x;          // X, Y, Scale and Strength (Goodness)
    *Frames++ = ipts[n].y;
    *Frames++ = ipts[n].scale;
    
    // set the current interest point
    des.setIpoint ( &ipts[n] );
    // assign reproducible orientation
    des.assignOrientation ( );
    
    *Frames++ = ipts[n].ori;        // Get the orientation
    *Frames++ = ipts[n].strength;   // The Goodness
  }
}
