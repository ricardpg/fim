%  SpreadFeatures ---------------------------------------------------------
%
%  The function subsamples features detected at the image using non-maxima
%  suppression with some predefined radius, threshold on cornerness, limit
%  on the maximum number of features to keep and masking the image.
%
%  ------------------------------------------------------------------------
%
%  Author        : Olivier Delaunoy, Marina Kudinova
%  e-mail        : {delaunoy,kudinova}@eia.udg.es
%  Working Group : Computer Vision and Robotics Group
%  Homepage      : http://vicorob.udg.es/
%  Project       : FIM
%  File          : SpreadFeatures.m
%  Date          : 18/06/2007 - 19/06/2007
%  Compiler      : MATLAB >= 7.0
% 
%  ------------------------------------------------------------------------
%
%  U S A G E :
%
%  Frames = SpreadFeatures ( FramesS, CornNum, ImHeight, ImWidth, ...
%                            Mask, Radius, MaxNumber, Threshold )
%
%  I N P U T :
% 
%  FramesS    - matrix containing the list of detected features, each 
%               column corresponds to one feature. X (row1), Y (row2), 
%               Cornerness is in the row, which number is specified by 
%               CornNum.
%  CornNum    - number of the column, where Cornerness is in the FramesS
%               matrix.
%  ImHeight   - height of the image in pixels, where input features were
%               detected.
%  ImWidth    - width of the image in pixels, where input features were
%               detected.
%  Mask       - mask of the image.
%  Radius     - non-maxima suppression radius.
%  MaxNumber  - maximum number of features to preserve.
%  Threshold  - optional, threshold on Cornerness value.
% 
%  O U T P U T : 
%
%  Frames - matrix with preserved features.
%  IndGood - indices of preserved FramesS. Optional ouput parameter.
%
% -------------------------------------------------------------------------

function ...
[ Frames, IndGood ] = SpreadFeatures ( FramesS, CornNum, ImHeight, ...
                      ImWidth, Mask, Radius, MaxNumber, Threshold )

error ( nargchk ( 7, 8, nargin ) );
error ( nargoutchk ( 1, 2, nargout ) );

if nargin == 5; Threshold = 0; end

if isempty(Mask); Mask = true( ImHeight, ImWidth ); end

% preparation of circular mask for suppression
if Radius > 0
    fmask = ~ceil(fspecial('disk',Radius));
else
    fmask = 0;
end
 
% sorting according to cornerness
[ignore,ind] = sort(abs(FramesS(CornNum,:)));

% matrix of points that can be taken
C = true ( ImHeight, ImWidth );

% Take only the good points (non-maxima, threshold, MaxNumber and Mask)
Frames = zeros(size(FramesS,1),MaxNumber);
IndGood = zeros(1,MaxNumber);
count = 0;
i=length(ind);
while i>0 && count < MaxNumber && FramesS(CornNum,ind(i)) >= Threshold
    x = round(FramesS(1,ind(i)));
    y = round(FramesS(2,ind(i)));
    if C(y,x) && Mask(y,x) && x > Radius && y > Radius && ...
       x <= ImWidth-Radius && y <= ImHeight-Radius
        count = count + 1;
        Frames(:,count) = FramesS(:,ind(i));
        IndGood(count) = ind(i);
        C(y-Radius:y+Radius, x-Radius:x+Radius) = ...
        C(y-Radius:y+Radius, x-Radius:x+Radius) .* fmask;
    end
    i=i-1;
end
Frames = Frames(:,1:count);
IndGood = IndGood(:,1:count);

% -------------------------------------------------------------------------
