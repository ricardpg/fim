% LaplacianFeatures -------------------------------------------------------
%
%  LaplacianFeatures function detects blob-like structures in the image 
%  using Laplacian, that is trace of Hessian as a measure of features 
%  "Goodness" or "Cornerness".
%
%  Autor(s)      : Marina Kudinova, Jordi Ferrer Plana
%  e-mail        : {kudinova,jferrerp}@eia.udg.es
%  Branch        : Computer Vision
%  Working Group : Underwater Vision Lab
%  Project       : FIM
%  Homepage      : http://porcsenglar.udg.es
%  Module        : Laplacian Blob Detector
%  File          : LaplacianFeatures.m
%  Date          : 01/04/2006 - 19/06/2007
%  Compiler      : MATLAB
%
% -------------------------------------------------------------------------
%
%  Copyright (C) 2005-2006 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -------------------------------------------------------------------------
%
%  U S A G E :
%
%  Frames = LaplacianFeatures ( I, Mask, Sigma, MaxNumber, Threshold, 
%                               Radius, SubPixel )
%
%  I N P U T :
%
%    I:          Source MxN graylevel image.
%    Mask:       A MxN mask image. If the size is the same than I, it will 
%                be used for masking (with a 0) the pixels that have not 
%                been used.
%    Sigma:      Is the variance for the Gaussian filters.
%    Threshold:  Is a threshold for the cornerness measure.
%    Radius:     Is the radius for the Non-Maximal suppression.
%    SubPixel:   Is it is different from 0, the subpixel offsets ROffs and
%                COffs will be computed.
%
%  O U T P U T :
%
%    Frames:  Matrix with Harris keypoints, each column corresponds to one 
%             keypoint: X ( 1 row ), Y ( 2 row ), Goodness ( normalized - 
%             3 row ).
%
% -------------------------------------------------------------------------

function Frames = LaplacianFeatures ( I, Mask, Sigma, MaxNumber, ...
                                      Threshold, Radius, DoSubPixel )
     
% Test the input parameters
error ( nargchk ( 7, 7, nargin ) );
error ( nargoutchk ( 1, 1, nargout ) );

% Grey scale real values image conversion (if necessary)
d = size ( I, 3 );
if ( d > 1 ), I = rgb2gray ( I ); end
if ( ~isa ( I, 'double' ) ), I = double ( I ); end

% Pre-smoothing the image with Gaussian kernel
if Sigma ~= 0
    
    % Gaussian filter mask build
    hsize = max ( 1, fix ( 6 * Sigma ) );
    G = fspecial ( 'gaussian', hsize, Sigma );

    % Smoothing of the image with Gausiian
    I = conv2 ( I,  G, 'same' );
    
end

% Prewit / Sobel derivative filters
dx = [ -1  0  1 ; 
       -2  0  2 ; 
       -1  0  1 ];
dy = dx';

% First-order Image Derivatives
Idx = conv2 ( I, dx, 'same' );
Idy = conv2 ( I, dy, 'same' );

% Second-order Image Derivatives
Id2x  = conv2 ( Idx, dx, 'same' );
Id2y  = conv2 ( Idy, dy, 'same' );

% Laplacian Cornerness: 
% Trace of Hessian, abs detects strong neg & pos responses
C = abs ( Id2x + Id2y );

% Scale-normalization of Laplacian
if Sigma ~= 0
    C = Sigma^2 * C;
end

% Normalization of cornernes to [0,1] for thresholding
C = C ./ max(max(C));

% Forming the Frames; take only with Cornerness >= Threshold
FramesInd = find ( C >= Threshold );
[ FramesY, FramesX ] = ind2sub ( size(C), FramesInd );
FramesS = transpose(sortrows([FramesX,FramesY,C(FramesInd)],-3));

% Spreading the features --------------------------------------------------

[h,w] = size(I);
Frames = SpreadFeatures ( FramesS, 3, h, w, Mask, Radius, ...
                          MaxNumber, Threshold );

% Perform Sub-Pixel Accuracy if required ----------------------------------

Rows = Frames(2,:);
Columns = Frames(1,:);

if DoSubPixel > 0 && ~isempty(Rows) && ~isempty(Columns)
    [ROffs, COffs] = SubPixel ( C, Rows', Columns', 1 );
    Frames(1,:) = Frames(1,:) + COffs';
    Frames(2,:) = Frames(2,:) + ROffs';
end

% -------------------------------------------------------------------------
