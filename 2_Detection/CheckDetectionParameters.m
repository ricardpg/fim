%% CheckDetectionParameters
%  
%  The CheckDetectionParameters function process the structure, that
%  contains input parameters for the DetectFeatures function of the FIM.
%
%  Author(s)     : Marina Kudinova, Jordi Ferrer
%  e-mail        : { kudinova, jferrerp } @ eia.udg.es
%
%  Working Group : Computer Vision and Robotics Group
%  Homepage      : http://vicorob.udg.es/
%
%  Project       : FIM
%  File          : CheckDetectionParameters.m
%  Date          : 19/01/2007 - 21/06/2007
%  Compiler      : MATLAB >= 7.0
%
%% FUNCTION

function ...
DetectParamsChecked = CheckDetectionParameters ( DetectionParameters )

Detectors   = { 'SIFT', 'SURF', 'Harris', 'Laplacian', 'Hessian', 'MSER' };
Descriptors = { 'SIFT', 'SURF', 'SURF-128', 'SURF-36', 'U-SURF', ...
                'U-SURF-128', 'U-SURF-36', 'ImagePatch', ...
                'RotatedImagePatch' };

% DetectionParameters.DetType = Detectors(2);
% DetectionParameters.MaxNumber = 300;               % max number of features to detect
% DetectionParameters.RadiusNonMaximal = 12;         % radius for non-maximal suppression for detector
% DetectionParameters.Sigma = 1;                     % sigma of the Gaussian used in Harris and Laplacian detectors
% DetectionParameters.Threshold = 0;                 % Threshold to reject poorer features.
% DetectionParameters.DescType = Descriptors(2);     % Chose the descriptor.
% DetectionParameters = 1;                           % <- to use default Detection parameters
% DetectionParameters = 0;                           % <- to cancel Detection; Description, Matching and RANSAC are cancelled then automatically

%% Default Settings
DefaultDetType          = 'SURF';      
DefaultMaxNumber        = 300;
DefaultRadiusNonMaximal = 6;
DefaultSigma            = 1;
DefaultMinCornerness    = 0;
DefaultMinArea          = 30;
DefaultDescType         = 'SURF';

%% Processing the parameters

if isstruct ( DetectionParameters ),
    % Check individually for the default value

% Detector Type

    if strcmpi ( DetectionParameters.DetType, '' )
        DetType = DefaultDetType;
    else
        DetType = DetectionParameters.DetType;
        if sum(strcmpi(DetType, Detectors)) == 0
            error ( 'MATLAB:FIM:Input', 'Incorrect Detector! \nPlease select the correct ProcessingParameters.DetType value!' );   
        end
    end

% Maximum Number of Features

    if ~isnumeric(DetectionParameters.MaxNumber) || DetectionParameters.MaxNumber <= 0
        error ( 'MATLAB:FIM:Input', 'Incorrect Input Maximum Number of features to detect. \nPlease specify the correct ProcessingParameters.MaxNumber value!' );
    elseif strcmpi ( DetectionParameters.MaxNumber, '' )
        MaxNumber = DefaultMaxNumber;
    else
        MaxNumber = round(DetectionParameters.MaxNumber);
    end

% Radius of Non-maximal suppression for the detector

    if ~isnumeric(DetectionParameters.RadiusNonMaximal) || DetectionParameters.RadiusNonMaximal < 0
        error ( 'MATLAB:FIM:Input', 'Incorrect Radius for Non-maximal Suppression. \nPlease specify the correct ProcessingParameters.RadiusNonMaximal value!' );        
    elseif  strcmpi ( DetectionParameters.RadiusNonMaximal, '' )
        RadiusNonMaximal = DefaultRadiusNonMaximal;
    else
        RadiusNonMaximal = DetectionParameters.RadiusNonMaximal;
    end

% Sigma of the Harris and Laplacian detecors 

    if ~isfield(DetectionParameters,'Sigma') || ...
            DetectionParameters.Sigma < 0 ||... 
            strcmpi ( DetectionParameters.Sigma, '' ),
        Sigma = DefaultSigma;
    else
        Sigma = DetectionParameters.Sigma;
    end

% Threshold on Cornerness for corner detectors

    if ~isfield(DetectionParameters,'MinCornerness') || ...
        strcmpi ( DetectionParameters.MinCornerness, '' ),
        MinCornerness = DefaultMinCornerness;
    else
        MinCornerness = DetectionParameters.MinCornerness;
    end

% Threshold on Region Area for region detectors

    if ~isfield(DetectionParameters,'MinArea') || ...
        strcmpi ( DetectionParameters.MinArea, '' ),
        MinArea = DefaultMinArea;
    else
        MinArea = DetectionParameters.MinArea;
    end

% Descriptor    

    if ~isfield(DetectionParameters,'DescType'),
        DescType = DefaultDescType;
    else
        if sum(strcmpi(DetectionParameters.DescType, Descriptors))
            DescType = DetectionParameters.DescType;
        else
            warning ( 'MATLAB:DetectFeatures:Input', 'Incorrect descriptor type, SURF will be used!' );
            DescType = DefaultDescType;
        end
    end

elseif isnumeric ( DetectionParameters ) 
    
    if DetectionParameters == 0 
        DetectParamsChecked = 0;            % Detection Is Cancelled
        return;
    else                                    
        MaxNumber = DefaultMaxNumber;       % Load default parameters
        DetType = DefaultDetType;
        Sigma = DefaultSigma;
        RadiusNonMaximal = DefaultRadiusNonMaximal;
        MinCornerness = DefaultMinCornerness;
        DescType = DefaultDescType;
    end

else
    error ( 'DetectFeatures:Input', ...
        [ 'Incorrect DetectionParameters input: can be 0, 1 or structure \n', ...
        'with MaxNumber, DetType, Sigma and RadiusNonMaximal fields.\n'] );
end

%% SIFT and SURF can not be used together

if strcmpi(DetType,'sift') && ~strcmpi(DescType,'sift') || ...
   strcmpi(DetType,'surf') &&  strcmpi(DescType,'sift')
    error ( 'MATLAB:FIM:Input', ...
          [ 'Incorrect Description Parameters: \n', ...
            upper(char(DetType)) ' Detector' ' and ' ...
            upper(char(DescType)) ' Descriptor are Incompatible!\n'] );          
end           

%% Output Params

    DetectParamsChecked.DetType = DetType;
    DetectParamsChecked.MaxNumber = MaxNumber;
    DetectParamsChecked.Sigma = Sigma;
    DetectParamsChecked.RadiusNonMaximal = RadiusNonMaximal;
    DetectParamsChecked.MinCornerness = MinCornerness;
    DetectParamsChecked.DescType = DescType;
    DetectParamsChecked.MinArea = MinArea;
end

