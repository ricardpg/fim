% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%  ProcessImage - Image processing
%
%  Author(s)     : Marina Kudinova, Jordi Ferrer, Ricard Prados
%  e-mail        : { kudinova, jferrerp, rprados } @ eia.udg.es
%
%  Working Group : Computer Vision and Robotics Group
%  Homepage      : http://vicorob.udg.es/
%
%  Project       : FIM
%  File          : ProcessImage.m
%  Date          : 10/10/2006 - 04/06/2007
%  Compiler      : MATLAB >= 7.0
%
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%
%  D e s c r i p t i o n :
%  
%  ProcessImage function can perform convertation to grayscale, preserving
%  just reg, green or blue channel, full or Clahe normalization depending
%  on the input ProcessingParameters contents.
%
%  U s a g e :
%  
%  OutputImg = ProcessImage ( InputImg, ProcessingParameters ) 
%     
%  ProcessingParameters = 0 (No processing) or 
%                         1 (Default parameters) or
%                         ProcessingParameters (structure with 2 fields):   
%  
%     ProcessingParameters.SingleChannel = 
%         { 0 -> The image has single channel, '' -> Default: 4,
%         1 -> Red, 2 -> Green, 3 -> Blue, 
%         4 -> Y channel after convertation to YIQ color space, 5 -> PCA }
%
%     ProcessingParameters.Equalization = 
%         { 0 -> No equalization, '' -> Default: 1, 
%         1 -> Normalization, 2 -> Equalization to 255, 3 -> CLAHE }
%
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function OutputImg = ProcessImage ( InputImg, ProcessingParameters )

error ( nargchk ( 2, 2, nargin ) );
error ( nargoutchk ( 1, 1, nargout ) );

% ~~~~~~~~~~ Checking the Parameters ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

if ndims(InputImg)<2 || ndims(InputImg)>3
    error ( 'PreprocessImages:Input', 'Incorrect Input Images sizes!' );
end

if ~isa ( InputImg, 'double' ); 
    InputImg = im2double ( InputImg ); 
end

ProcParamsChecked = CheckProcessingParameters ( ProcessingParameters );
SingleChannel     = ProcParamsChecked.SingleChannel;
Equalization      = ProcParamsChecked.Equalization;
ResizeCoeff       = ProcParamsChecked.ResizeCoeff;

% ~~~~~~~~~~ Resizing the Image ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

if ResizeCoeff ~= 1;
    InputImg = imresize ( InputImg, 1/ResizeCoeff );
end

% ~~~~~~~~~~ Multichannel -> Single Channel ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

if size(InputImg,3) == 1; SingleChannel = 0; end
switch SingleChannel
    case 0; ImgSC = InputImg;             % No convertation        
    case 4; ImgSC = rgb2gray(InputImg);   % Default (grayscale) convertion
    case 5; 
        [GrayImgs, Vt, Dt, mk] = pca ( InputImg, [] );
        ImgSC = GrayImgs(:,:,3);
    case {1,2,3}; 
        ImgSC = InputImg(:,:,SingleChannel); % Red channel
    otherwise; error ( 'FIM:ProcessImage:Input', ...
                       'Incorrect SingleChannel Parameter.' );
end

% ~~~~~~~~~~ Equalization ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ImgEQ = zeros(size(ImgSC));

switch Equalization
        
    case 0; ImgEQ = ImgSC; %*** No processing *****************************       
        
    case 1;                %*** Normalization *****************************
        for j = 1:size(ImgSC,3)
            ImgEQ(:,:,j) = ImgSC(:,:,j);
            ImgEQ(:,:,j) = ImgEQ(:,:,j) - min ( min ( ImgEQ(:,:,j) ) );
            m = max ( max ( ImgEQ(:,:,j) ) );
            if m ~= 0; ImgEQ(:,:,j) = ImgEQ(:,:,j) / m; end
        end
        
    case 2;               %*** Equalization *******************************
        for j = 1:size(ImgSC,3)
            ImgEQ(:,:,j) = histeq ( ImgSC(:,:,j), 255 );
        end

    case 3;               %*** CLAHE, default parameters ******************
        for j = 1:size(ImgSC,3)
            ImgEQ(:,:,j) = adapthisteq ( ImgSC(:,:,j) );
        end
    otherwise; error ( 'FIM:ProcessImage:Input', ...
                       'Incorrect Equalization Parameter.' );
end

% ~~~~~~~~~~ Output

OutputImg = ImgEQ;

end

%END
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

%     case 3;               %*** CLAHS Contrast Enhancement ***************
%         for j = 1:size(ImgSC,3)
%             NrY = 16;    % number of CLAHS subregions along Y axis, def=8
%             NrX = 16;    % number of CLAHS subregions along X axis, def=8
%             Clim = 2;     % Normalized clip limit, (higher values = 
%                           % more contrast); value <= 1 = standard AHE
%             [ ImH, ImW ] = size ( ImgSC(:,:,j) );
%             ImProc = uint16 ( 65535 * imcrop ( ImgSC(:,:,j), ...
%                     [ 0, 0, NrX*fix(ImW/NrX), NrY*fix(ImH/NrY) ] ) );
%             %fprintf('To perform CLAHS images were cropped!');
%             ImgEQ(:,:,j) = mat2gray ( double ( ...
%                        clahs ( ImProc, NrY, NrX, 'cliplimit', Clim ) ) );
%        end

%     case 6;
%         YIQ = rgb2ntsc(InputImg);
% %       ImgSC = uint16(YIQ(:,:,1)*65535);
%         ImgSC = YIQ(:,:,1);
