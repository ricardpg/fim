%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.es
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.es
%
%  Module        : RGB Principial Component Analisys.
%
%  File          : pca.m
%  Date          : 04/11/2006 - 28/12/2006
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%                  - Info: http://www.cis.hut.fi/jhollmen/dippa/node30.html
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2006 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  PCA Principal Component Analisys to RGB images.
%
%     [GrayImgs, V, D] = pca ( InputImg [, Mask] )
%
%     Input Parameters:
%      InputImg: Source MxNx3 color image.
%      Mask: MxN or empty mask matrix. If Mask is empty the whole image is
%            processed, otherwise each position indicates whether a pixel
%            is processed or not.
%
%     Output Parameters:
%      GrayImgs: Resulting MxNx3 graylevel images sorted by the goodness of
%                the eigen analysis projection.
%      
%      Vt: 3x3 Matrix with the eigenvectors in each column.
%      Dt: 3x3 Diagonal matrix with the eigenvalues corresponding to each
%          eigenvector.
%

function [GrayImgs, Vt, Dt, mk] = pca ( InputImg, Mask )
  % Test the input parameters
  error ( nargchk ( 1, 2, nargin ) );
  error ( nargoutchk ( 4, 4, nargout ) );

  % Check input image dimensions
  sErr = ndims ( InputImg ) ~= 3;

  % Don't use mask initally 
  UseMask = false;

  % Test the Sizes and Masks
  if sErr
    if nargin == 2
      if ~isempty ( Mask )
        if ndims ( Mask ) ~= 2
          sErr = true;
        else
          sErr = any ( size ( InputImg(:,:,1) ) - size ( Mask ) );
          UseMask = true;
        end
      end
    end
  end

  % There is an error
  if sErr
    error ( 'MATLAB:pca:Input', 'Incorrect Input matrix sizes!' );
  end

  [m, n, s] = size ( InputImg );

  % If mask wanted: get the right values from the image
  if UseMask
    Index = find ( Mask ~= 0 );
    K = zeros ( 3, size ( Index, 1 ) );
  else
    K = zeros ( 3, m * n );
  end

  mk = zeros ( 3, 1 );
  % For each channel: Compute InputImage - mean
  for i = 1 : 3
    M = double( InputImg(:,:,i) );

    % If mask copy the unmasked values
    if UseMask
       K(i,:) = M(Index);
    else
       K(i,:) = M(:);
    end
    mk(i, 1) = mean ( K(i,:) );
    K(i,:) = K(i,:) - mk(i, 1);
  end
  Cv = cov ( K' );
  % Perform eigen analisys to the covariance matrix
  [Vt, Dt] = eig ( Cv );

  % Compute final image:
  %
  %   yk = Ak * ( x - x_mean )  Where x = [R, G, B] and Ak = V1, V2, V3
  %
  % Inverse:
  %   xk = Ak^t * yk + y_mean    Where yk = [Rp, Gp, Bp] and Ak = V1, V2, V3
  %
  GrayImgs = zeros ( m, n, s );
  for i = 1 : 3
    % Eigenvector with higher eigenvalue and resample the data
    GrayImgs(:,:,i) = reshape ( Vt(:,i)' * K, m, n );
  end
end
