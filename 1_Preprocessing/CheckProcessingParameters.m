%% CheckProcessingParameters
%  
%  The CheckProcessingParameters function process the structure, that
%  contains input parameters for the ProcessImage function of the FIM.
%
%  Author(s)     : Marina Kudinova, Jordi Ferrer
%  e-mail        : { kudinova, jferrerp } @ eia.udg.es
%
%  Working Group : Computer Vision and Robotics Group
%  Homepage      : http://vicorob.udg.es/
%
%  Project       : FIM
%  File          : CheckProcessingParameters.m
%  Date          : 19/01/2007 - 04/06/2007
%  Compiler      : MATLAB >= 7.0
%
%% FUNCTION

function ...
ProcParamsChecked = CheckProcessingParameters ( ProcessingParameters )

error ( nargchk ( 1, 1, nargin ) );
error ( nargoutchk ( 1, 1, nargout ) );

% ProcessingParameters.SingleChannel = '';  % 0-no, ''-default, 1-red, 2-green, 3-blue, 4-grayscale, 5-PCA, 6-Ychannel of YIQ
% ProcessingParameters.Equalization = 1;    % 0-no, ''-default, 1-full, 2-Clahe
% ProcessingParameters = 1;                 % <- to use default Processing parameters
% ProcessingParameters = 0;                 % <- to cancel Preprocessing of images: Images must have single color channel!

% Default Settings
DefaultSingleChannel = 4;      % Convertation to grayscale
DefaultEqualization  = 1;      % Full equalization
DefaultResizeCoeff   = 1;

% Processing the parameters
if isstruct(ProcessingParameters)
    
    SingleChannel = ProcessingParameters.SingleChannel;
    if strcmpi ( SingleChannel, '' ) || ~isscalar(SingleChannel) || ...
            ~ismember(SingleChannel,[0,1,2,3,4,5])
        SingleChannel = DefaultSingleChannel;
    end    
    Equalization = ProcessingParameters.Equalization;
    if strcmpi ( Equalization, '' ) || ~isscalar(Equalization) || ...
       ~ismember(Equalization,[0,1,2,3])
        Equalization = DefaultEqualization;
    end
    ResizeCoeff = ProcessingParameters.ResizeCoeff;
    if strcmpi ( ResizeCoeff, '' ) || ~isscalar(ResizeCoeff)
        ResizeCoeff = DefaultResizeCoeff;
    end 
    
elseif isnumeric(ProcessingParameters)
    
    if ProcessingParameters == 0;
        SingleChannel = 0;
        Equalization = 0;
        ResizeCoeff = DefaultResizeCoeff;
    else
        SingleChannel = DefaultSingleChannel;
        Equalization = DefaultEqualization;
        ResizeCoeff = DefaultResizeCoeff;
    end

else
   error ( 'MATLAB:FIM:Input', ...
        [ 'Incorrect ProcessingParameters input: can be 0, 1 or structure \n', ...
          'with SingleChannel and Equalization fields.\n'] );
end

ProcParamsChecked.SingleChannel = SingleChannel;
ProcParamsChecked.Equalization  = Equalization;
ProcParamsChecked.ResizeCoeff   = ResizeCoeff;