
% EstimateMotion3D --------------------------------------------------------
%
%  Rejecting of outliers using RANSAC an features coordinates.
%
%  Author(s)     : Ricard Campos
%  e-mail        : { rcampos } @ eia.udg.es
%  Working Group : Computer Vision and Robotics Group
%  Homepage      : http://vicorob.udg.es/
%  Project       : FIM
%  File          : EstimateMotion3D.m
%  Date          : 09/11/2013
%  Compiler      : MATLAB > 7.0
%
% -------------------------------------------------------------------------

function [ Fundamental, Inliers ] = EstimateMotion3D (  Img1Features, ... 
                                                        Img2Features, ...
                                                        Matches, ...
                                                        Parameters )

% Checking Input and Output parameters

error ( nargchk ( 4, 4, nargin ) );
error ( nargoutchk ( 2, 2, nargout ) );

Params = CheckMotionParameters ( Parameters );

if ~isstruct(Params)
    Homography = [];
    Inliers = [];
    return;
end

SpatialStdev      = Params.SpatialStdev;

% Defining the Input for RANSAC

Features = [];
Features(1:2,:) = Img2Features ( 1:2, Matches(2,:) );
Features(3:4,:) = Img1Features ( 1:2, Matches(1,:) );

fittingfn  = @ComputeFundamentalMatrixFIM;
distfn   = @DetectInliers;
degenfn  = @FindSamePoints;
t        = sqrt(5.99) * SpatialStdev;

% Checking if there is enough matches for Homography estimation

if size(Features,2) < 8;
    Fundamental = [];
    Inliers = [];
    return;
end

% Executing RANSAC

[ Fundamental, Inliers ] = ransac ( Features, fittingfn, distfn, degenfn, 8, t );

if isempty ( Fundamental ); return; end;

% !!! Reestimation of the Homography using the whole set of the inliers

AcceptedMatches = Matches ( :, Inliers );
GoodFeatures(1:2,:) = Img2Features ( 1:2, AcceptedMatches(2,:) );
GoodFeatures(3:4,:) = Img1Features ( 1:2, AcceptedMatches(1,:) );
Fundamental = fittingfn ( GoodFeatures );

end

% distfn for RANSAC: DetectInliers ----------------------------------------

function [ inliers, M ] = DetectInliers ( F, x, t )

error ( nargchk ( 3, 3, nargin ) );
error ( nargoutchk ( 2, 2, nargout ) );

x1 = x( 1:2, : ) ;
x1( end+1, : ) = 1 ; % Homogeneous coordinates
x2 = x( 3:4, : ) ;
x2( end+1, : ) = 1 ; % Homogeneous coordinates

x2tFx1 = zeros(1,length(x1));
for n = 1:length(x1)
    x2tFx1(n) = x2(:,n)'*F*x1(:,n);
end

Fx1 = F*x1;
Ftx2 = F'*x2;     

% Evaluate distances
d =  x2tFx1.^2 ./ ...
     (Fx1(1,:).^2 + Fx1(2,:).^2 + Ftx2(1,:).^2 + Ftx2(2,:).^2);

% inliers = find ( abs(d) < sqrt(5.99) * t ); % Multiple View Geometry, p.123
inliers = find ( abs(d) < t ); % Multiple View Geometry, p.123
M = F;

end

% degenfn for RANSAC: FindSamePoints --------------------------------------

function r = FindSamePoints ( x )
error ( nargchk ( 1, 1, nargin ) );
error ( nargoutchk ( 1, 1, nargout ) );
s = size ( x, 2 );
k = 0;
for i = 1 : s
    k = k + sum ( ismember( x(1:2,:)', x(1:2,i)','rows') );
    k = k + sum ( ismember( x(3:4,:)', x(3:4,i)','rows') );
end
r = ( k ~= 2*s );
end

function F = ComputeFundamentalMatrixFIM ( P )
  F = ComputeFundamentalMatrix( P(1:2,:), P(3:4,:) );
end