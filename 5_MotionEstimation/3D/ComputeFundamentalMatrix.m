% Based on Peter Kovesi's "fundmatrix.m" and "normalise2dpts.m" function.
%
% Source: 
%   --> http://www.csse.uwa.edu.au/~pk/research/matlabfns/Projective/fundmatrix.m
%   Copyright (c) 2002-2005 Peter Kovesi
%   School of Computer Science & Software Engineering
%   The University of Western Australia
%   http://www.csse.uwa.edu.au/

function [ F, e1, e2 ] = ComputeFundamentalMatrix ( P, M )

  % Test the input parameters
  error ( nargchk ( 2, 2, nargin ) );
  error ( nargoutchk ( 1, 1, nargout ) );

  % Compute Sizes of Input Data
  [pr np] = size ( P );
  [mr nm] = size ( M );

  % Test the Input Matrices
  if ( pr ~= mr ) || ( np ~= nm ) || ( mr ~= 2 )
    error ( 'MATLAB:ComputeEuclideanTransformation:Input', ...
            'Matrix dimensions do not agree!' );
  end
  
  % Normalise each set of points so that the origin 
  % is at centroid and mean distance from origin is sqrt(2). 
  % normalise2dpts also ensures the scale parameter is 1.
  P(end+1, : ) = 1 ;
  M(end+1, : ) = 1 ;
  [ x1, T1 ] = Normalize2D(P);
	[ x2, T2 ] = Normalize2D(M);

  % Build the constraint matrix
  A = [x2(1,:)'.*x1(1,:)'   x2(1,:)'.*x1(2,:)'  x2(1,:)' ...
       x2(2,:)'.*x1(1,:)'   x2(2,:)'.*x1(2,:)'  x2(2,:)' ...
       x1(1,:)'             x1(2,:)'            ones( np, 1 ) ];       

  
  [U,D,V] = svd(A,0); % Under MATLAB use the economy decomposition
  

  % Extract fundamental matrix from the column of V corresponding to
  % smallest singular value.
  F = reshape(V(:,9),3,3)';
    
  % Enforce constraint that fundamental matrix has rank 2 by performing
  % a svd and then reconstructing with the two largest singular values.
  [U,D,V] = svd(F,0);
  F = U*diag([D(1,1) D(2,2) 0])*V';
    
  % Denormalise
  F = T2'*F*T1;

  if nargout == 3  	% Solve for epipoles
    [U,D,V] = svd(F,0);
    e1 = hnormalise(V(:,3));
    e2 = hnormalise(U(:,3));
  end