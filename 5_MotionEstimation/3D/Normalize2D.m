function [ Mout, T ] = normalize2D( M )
    cx1=sum(M(1,:))/size(M,2);
    cy1=sum(M(2,:))/size(M,2);
    
    dx1=M(1,:)-cx1;
    dy1=M(2,:)-cy1;
    
    d1=sum((dx1.^2+dy1.^2).^(1/2))/size(M,2);
    
    Mout(1,:)=sqrt(2)./d1.*(M(1,:)-cx1);
    Mout(2,:)=sqrt(2)./d1.*(M(2,:)-cy1);
    
    
    T=[sqrt(2)/d1 0 -(sqrt(2)/d1*cx1); 0 sqrt(2)/d1 -(sqrt(2)/d1*cy1); 0 0 1];