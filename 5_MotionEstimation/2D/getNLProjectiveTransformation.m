function  H = getNLProjectiveTransformation(corr)

% H = getProjectiveTransformation(corr)
%
%     corr - the correspondences between the images
%                column 1 : point x
%                column 2 : point y
%                column 3 : cornerness   or match x
%                column 4 : match x      or match y
%                column 5 : match y      or doesn't exist
%                column 6 : correlscore  or doesn't exist
%       H  - resulting transformation matrix
%
% Compute a projective transformation from points and matches with a non
% linear method
%
% Developed by Tudor Nicosevici, Rafael Garcia and Ricard Prados
% Dep. of Electronics, Informatics and Automation, University of Girona

% initH = getProjectiveTransformation(corr);

P = corr';
initH = ComputeProjectiveTransformation ( P(1:2,:), P(3:4,:) );

if size(corr,1)>4,
    points = corr(:,1:2);
    matches = corr(:,3:4);

    % Make conditioners to normalize
    m=mean(points,1);
    s=std(points,0,1);
    PConditioner = [diag(sqrt(2)./s) -diag(sqrt(2)./s)*m'; 0 0 1];
    m=mean(matches,1);
    s=std(matches,0,1);
    MConditioner = [diag(sqrt(2)./s) -diag(sqrt(2)./s)*m'; 0 0 1];

    H_cond = MConditioner * initH * inv(PConditioner);

    % opt = optimset( optimset('lsqnonlin') , 'LargeScale','off', 'Display','off' ); % Works only on old matlab versions...
    opt = optimset( optimset('lsqnonlin') , 'LargeScale','off', 'Display','off', 'Algorithm', 'levenberg-marquardt' ) ;

    H_cond = lsqnonlin(@(H_cond) lsq_func(H_cond, points, matches, PConditioner, MConditioner),H_cond,[],[],opt);

    H = inv(MConditioner) * H_cond * PConditioner;
    H = H/H(3,3);
else
    H = initH;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function r = lsq_func(H, points, matches, PConditioner, MConditioner)

  H_decond = inv(MConditioner) * H * PConditioner;
  r = computeResiduals([points matches], H_decond);
