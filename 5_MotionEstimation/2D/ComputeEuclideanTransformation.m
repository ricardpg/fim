%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.es
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.es
%
%  Module        : Compute the Euclidian Transformation.
%
%  File          : ComputeEuclideanTransformation.m
%  Date          : 29/07/2006 - 01/03/2007
%
%  Compiler      : MATLAB
%  Libraries     : -
%
%  Notes         : - File written in ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2006 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  ComputeEuclideanTransformation Compute the Euclidean Transformation from a 
%                                 set of points and their correspondences.
%
%     H = ComputeEuclideanTransformation ( P, M )
%
%     Input Parameters:
%      P: 2xn matrix representing the 2D points.
%      M: 2xn matrix representing the 2D correspondences.
%
%     Output Parameters:
%      H: A 3x3 planar homography representing the estimated motion between
%         the set of points r and the set of points t.
%
%
%     Using: Closed-form solution of absolute orientation using
%            orthonormal matrices (1988 by Horn, Hilden, Negahdaripour)

function H = ComputeEuclideanTransformation ( P, M )
  
  % Test the input parameters
  error ( nargchk ( 2, 2, nargin ) );
  error ( nargoutchk ( 1, 1, nargout ) );

  % Compute Sizes of Input Data
  [pr np] = size ( P );
  [mr nm] = size ( M );

  % Test the Input Matrices
  if ( pr ~= mr ) || ( np ~= nm ) || ( mr ~= 2 )
    error ( 'MATLAB:ComputeEuclideanTransformation:Input', ...
            'Matrix dimensions do not agree!' );
  end
  
  % Find centroid of the two sets of points
  rm = mean ( P, 2 );
  tm = mean ( M, 2 );

  % Center the two sets of source data
  Pc = P - repmat ( rm, 1, np );
  Mc = M - repmat ( tm, 1, np );

  % Compute Rotation
  %     | Sum[i..n] xi xi'   Sum[i..n] xi yi' |   | a  b |
  % M = |                                     | = |      |
  %     | Sum[i..n] yi xi'   Sum[i..n] yi yi' |   | c  d |
  M = Mc * Pc';

%  a = M(1,1); b = M(1,2); c = M(2,1); d = M(2,2);

%  j = a * a + c * c; k = a * b + c * d; l = k; m = b * b + d * d;

  %        | a  c | | a  b |   | a^2+c^2  ab+cd  |   | j  k |
  % Mt M = |      | |      | = |                 | = |      |
  %        | b  d | | c  d |   |  ab+cd  b^2+d^2 |   | l  m |

  % EigenValues:
  %
  % det ( Mt M - Lambda I ) = 0
  %
  %      | j  k |   | Lambda   0    |   | j - Lambda     k      |
  % det  |      | - |               | = |                       | =
  %      | l  m |   |   0    Lambda |   |    l       m - Lambda |
  %
  %  = ( j - Lambda ) * ( m - Lambda ) - l k = j m - j Lambda - m Lambda + 
  %  + Lambda^2 = Lambda^2 - (j+m) Lambda + jm - lk
  %
  %  Lambda = ( (j+m) +- sqrt ( (j+m)^2 - 4 (jm - lk) ) ) / 2
  %
%  s = sqrt ( (j+m) * (j+m) - 4 * (j*m - l*k) );
%  L1 = ( j + m + s ) / 2;
%  L2 = ( j + m - s ) / 2;

  % EigenVectors:
  %
  % ( Mt M - Lambdai I ) ui = 0
  %
  %  | j - Li   k    | | x |         | r  k | | x |
  %  |               | |   | = 0     |      | |   | = 0
  %  |   l    m - Li | | y |         | l  s | | y |
  %
  %  (j-Li) x + k y = 0
  %                        x = t
  %  y = -(j-Li) / k
  %                        v = [ 1, (Li-l)/k ]
  % Columnes de V:
%  k2 = k * k;
%  Li = L1 - j;
%  v1 = [ 1, Li/k ] / sqrt ( 1 + Li*Li/k2 );
%  Li = L2 - j;
%  v2 = [ 1, Li/k ] / sqrt ( 1 + Li*Li/k2 );

%  V = -[ v1; v2 ];

  % The same for the M Mt
  %
  %        | a  b | | a  c |   | a^2+b^2  ac+bd  |   | j  k |
  % M Mt = |      | |      | = |                 | = |      |
  %        | c  d | | b  d |   |  ac+bd  c^2+b^2 |   | l  m |

%  j = a * a + b * b; k = a * c + b * d; l = k; m = c * c + b * b;
%  s = sqrt ( (j+m) * (j+m) - 4 * (j*m - l*k) );
%  L1 = ( j + m + s ) / 2;
%  L2 = ( j + m - s ) / 2;
%  k2 = k * k;
%  Li = L1 - j; 
%  v1 = [ 1, Li/k ] / sqrt ( 1 + Li*Li/k2 );
%  Li = L2 - j; 
%  v2 = [ 1, Li/k ] / sqrt ( 1 + Li*Li/k2 );

%  U = -[ v1; v2 ];

%  [V1 Ud1] = eig ( M' * M );
%  [U1 Vd1] = eig ( M * M' );

  [U, S, V] = svd ( M );
  R = U * V;

  %
  %       Cos Phi -Sin Phi
  %   R = Sin Phi  Cos Phi
  %
  % det ( R ) = Cos^2 Phi + Sin^2 Phi = 1
  %
  % det ( R ) < 0 => Mirror Effect include by the euclidean transformation 
  %
  if ( det ( R ) < 0 )
    % R = U * [1 0; 0 -1] * V;
    R = U * [ V(1,1) V(1,2);
             -V(2,1) -V(2,2) ];
  end

  % Angle
  Phi = atan2 ( R(2, 1), R(2, 2) );

  % Compute Translation
  T = ( tm - R * rm )';

  cPhi = cos ( Phi );
  sPhi = sin ( Phi );

  % Construct the Euclidean Homography
  H = [ cPhi -sPhi T(1);
        sPhi  cPhi T(2);
          0     0    1 ];
end
