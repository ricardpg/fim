%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.es
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.es
%
%  Module        : Apply an Homography to a set of 2D Input Points.
%
%  File          : ApplyHomo.m
%  Date          : 29/07/2006 - 23/08/2006
%
%  Compiler      : MATLAB
%  Libraries     : -
%
%  Notes         : - File writen in ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2006 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  APPLYHOMO Apply a planar transformation (Homography) to a set of points.
%
%     M = ApplyHomo ( H, P )
%
%     Input Parameters:
%      H: A 3x3 planar Homography matrix that holds the motion that will be
%      applyed to the set of points P.
%      P: A 2xK set of 2D points. If a third row is specified it will be
%      used to divide the other components to obtain this points in
%      homogeneous coordinates.
%
%     Output Parameters:
%      M: A 2xK set of 2D points resulting from applying to the set of
%         points P, the motion hold in the homography H.
%

function M = ApplyHomo ( H, P )

  % Check Input Output Arguments
  if nargin == 2 && nargout == 1

    % Compute Sizes of Input Data
    [hr hc] = size ( H );
    [pr pc] = size ( P );

    % Test Input Matrices
    if ( hr == 3 ) && ( hc == 3 ) && ( pr == 2 || pr == 3 )

      % Set to Homogeneous Coordinates if needed
      if ( pr == 2 )
        % Add the unit
        P(3,:) = 1;
      else
        % Compute in Homogenous Coordinates
        P(1,:) = P(1,:) ./ P(3,:);
        P(2,:) = P(2,:) ./ P(3,:);
      end

      % Compute the Homogeneous Transformation
      Mp = H * P;

      % Compute in Homogenous Coordinates
      M(1,:) = Mp(1,:) ./ Mp(3,:);
      M(2,:) = Mp(2,:) ./ Mp(3,:);
    else
       error ( 'MATLAB:ApplyHomo:Input', 'Matrix dimensions must agree!' );
    end
  else
    error ( 'MATLAB:ApplyHomo:InputOutput', 'Usage: M = ApplyHomo ( H, P )' );
  end
end
