%
% CheckMotionParameters ---------------------------------------------------
%  
%  The CheckMotionParameters function process the structure, that
%  contains input parameters for the EstimateMotion2D function of the FIM.
%
%  Author(s)     : Marina Kudinova, Jordi Ferrer
%  e-mail        : { kudinova, jferrerp } @ eia.udg.es
%  Working Group : Computer Vision and Robotics Group
%  Homepage      : http://vicorob.udg.es/
%  Project       : FIM
%  File          : CheckMotionParameters.m
%  Date          : 20/01/2007 - 21/06/2007
%  Compiler      : MATLAB >= 7.0
%
% -------------------------------------------------------------------------

function ...
MotionParamsChecked = CheckMotionParameters ( MotionParameters )

% EstimationMethods = { 'RANSAC', 'LMedS' };

HomographyModels = { 'Euclidean', 'Similarity', 'Affine', ...
                     'Projective', 'ProjectiveNLM' };
                  
% DefaultEstimationMethod = 'RANSAC';
DefaultHomographyModel = 'PROJECTIVE';
DefaultSpatialStdev = 1;

if isstruct(MotionParameters)
    
%     if sum(strcmpi(MotionParameters.EstimationMethod,EstimationMethods))~=0
%         MotionParamsChecked.EstimationMethod = ...
%         char(MotionParameters.EstimationMethod);
%     else
%         MotionParamsChecked.EstimationMethod = DefaultEstimationMethod;
%     end
       
    if sum(strcmpi(MotionParameters.HomographyModel,HomographyModels))~=0
        MotionParamsChecked.HomographyModel = ...
        char(MotionParameters.HomographyModel);
    else
        MotionParamsChecked.HomographyModel = DefaultHomographyModel;
    end
    
    if strcmpi(MotionParameters.SpatialStdev,'') || ...
       ~isnumeric(MotionParameters.SpatialStdev) || ...
       MotionParameters.SpatialStdev < 0 
        MotionParamsChecked.SpatialStdev = DefaultSpatialStdev;
    else
        MotionParamsChecked.SpatialStdev = MotionParameters.SpatialStdev;
    end
    
    MotionParamsChecked.MotionModel = ...
        char(MotionParameters.MotionModel);
    
elseif isnumeric ( MotionParameters )    
    if MotionParameters == 0
        MotionParamsChecked = 0;
        return;
    else
        MotionParamsChecked.EstimationMethod = DefaultEstimationMethod;
        MotionParamsChecked.HomographyModel = DefaultHomographyModel;
        MotionParamsChecked.SpatialStdev = DefaultSpatialStdev;
    end
else
    error ( 'MATLAB:FIM:Input', ...
          [ 'Incorrect MotionParameters input: can be 0, 1 or ' ...
            'structure \nwith EstimationMethod, HomographyModel and ' ...
            'SpatialStdev fields. \n'] );
end

% END
% -------------------------------------------------------------------------