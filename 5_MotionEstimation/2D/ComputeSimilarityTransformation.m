%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.es
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.es
%
%  Module        : Compute the Similarity Transformation.
%
%  File          : ComputeSimilarityTransformation.m
%  Date          : 29/07/2006 - 01/03/2007
%
%  Compiler      : MATLAB
%  Libraries     : -
%
%  Notes         : - File written in ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2006 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  ComputeSimilarityTransformation. Compute the Similarity Transformation
%                                   from a set of points and their correspondences.
%
%     H = ComputeSimilarityTransformation ( P, M )
%
%     Input Parameters:
%      P: 2xn matrix representing the 2D points.
%      M: 2xn matrix representing the 2D correspondences.
%
%     Output Parameters:
%      H: A 3x3 planar homography representing the estimated motion between
%         the set of points r and the set of points t.
%
%
%     Using: Closed-form solution of absolute orientation using
%            orthonormal matrices (1988 by Horn, Hilden, Negahdaripour)

function H = ComputeSimilarityTransformation ( P, M )

  % Test the input parameters
  error ( nargchk ( 2, 2, nargin ) );
  error ( nargoutchk ( 1, 1, nargout ) );

  % Compute Sizes of Input Data
  [pr np] = size ( P );
  [mr nm] = size ( M );

  % Test the Input Matrices
  if ( pr ~= mr ) || ( np ~= nm ) || ( mr ~= 2 )
    error ( 'MATLAB:ComputeSimilarityTransformation:Input', ...
            'Matrix dimensions do not agree!' );
  end

  % Find centroid of the two sets of points
  rm = mean ( P, 2 );
  tm = mean ( M, 2 );

  % Center the two sets of source data
  Pc = P - repmat ( rm, 1, np );
  Mc = M - repmat ( tm, 1, np );

  % Compute Rotation
  %     | Sum[i..n] xi xi'   Sum[i..n] xi yi' |   | a  b |
  % M = |                                     | = |      |
  %     | Sum[i..n] yi xi'   Sum[i..n] yi yi' |   | c  d |
  Scale = sum(sum( Mc .* Mc, 2 )) / sum(sum( Pc .* Pc, 2));

  M = Mc * Pc' / Scale;

  [U, S, V] = svd ( M );
  R = U * V;
  if ( det ( R ) < 0 )
    R = U * [ V(1,1) V(1,2);
             -V(2,1) -V(2,2) ];
  end

  % Angle
  Phi = atan2 ( R(2, 1), R(2, 2) );

  % Compute Translation
  T = ( tm - Scale * R * rm )';

  cPhi = Scale * cos ( Phi );
  sPhi = Scale * sin ( Phi );

  % Construct the Similarity Homography
  H = [ cPhi -sPhi T(1);
        sPhi  cPhi T(2);
          0     0    1 ];
end
