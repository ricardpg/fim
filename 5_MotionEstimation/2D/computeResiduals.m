function residuals = computeResiduals(corr, H)

% residualMedian = computeResiduals(corr, H)
% 
%     corr        - the correspondences between the images
%                       column 1 : point x
%                       column 2 : point y
%                       column 3 : cornerness   or match x
%                       column 4 : match x      or match y
%                       column 5 : match y      or doesn't exist
%                       column 6 : correlscore  or doesn't exist
%     H           - the transformation homography
%     residuals   - residuals of the correspondences wrt H
%
% Computes euclidean distances between the points and the projection of the
% matches and the matches and the projection of the points
%
% Developed by Delaunoy Olivier (delaunoy@eia.udg.es)
% Dep. of Electronics, Informatics and Automation, University of Girona

switch(size(corr,2))
    case 4
        match = corr(:,3:4)';
        point = corr(:,1:2)';
    case 6
        match = corr(:,4:6)';
        point = corr(:,1:3)';
    otherwise
        error('the correspondences should contain 4 or 6 columns');
end

% euclidean distance
match(3,:) = 1;
point(3,:) = 1;
proj_point = inv(H) * match;
proj_point(1,:) = proj_point(1,:) ./ proj_point(3,:);
proj_point(2,:) = proj_point(2,:) ./ proj_point(3,:);
proj_point = (proj_point - point).^2;
dist_proj_point = sqrt(proj_point(1,:) + proj_point(2,:));
proj_match = H * point;
proj_match(1,:) = proj_match(1,:) ./ proj_match(3,:);
proj_match(2,:) = proj_match(2,:) ./ proj_match(3,:);
proj_match = (proj_match - match).^2;
dist_proj_match = sqrt(proj_match(1,:) + proj_match(2,:));
residuals = (dist_proj_point + dist_proj_match)/2';
