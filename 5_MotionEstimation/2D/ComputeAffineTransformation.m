function H = ComputeAffineTransformation ( P, M, Svd )
% H = ComputeAffineTransformation ( P, M [, Svd] )
%
%       matches - array of points and matches
%       P       - row 1 : point x
%                 row 2 : point y
%       M       - row 1 : match x
%                 row 2 : match y
%       H       - resulting transformation matrix
%
%       Svd     - 0 Least-Squares, ~= 0 Svd
%       
%
% Compute the Affine Transformation from a set of points and their
% correspondences
%
% Developed by Tudor Nicosevici, Rafael Garcia and Ricard Prados, Marina
% Kudinava, Jordi Ferrer
% Dep. of Electronics, Informatics and Automation, University of Girona

  % Test the input parameters
  error ( nargchk ( 2, 3, nargin ) );
  error ( nargoutchk ( 1, 1, nargout ) );

  if nargin == 2, Svd = 1; end;

  % Compute Sizes of Input Data
  [pr np] = size ( P );
  [mr nm] = size ( M );

  % Test the Input Matrices
  if ( pr ~= mr ) || ( np ~= nm ) || ( mr ~= 2 )
    error ( 'MATLAB:ComputeAffineTransformation:Input', ...
            'Matrix dimensions do not agree!' );
  end
  
  matches = [P; M]';

U=zeros(size(matches,1)*2,6);
X=zeros(size(matches,1)*2,1);

for Cnt_Punts = 1 : size(matches,1),
    U(Cnt_Punts*2-1,1) = matches(Cnt_Punts,1); %a
    U(Cnt_Punts*2-1,2) = matches(Cnt_Punts,2); %b
    U(Cnt_Punts*2-1,3) = 1;             %c
    U(Cnt_Punts*2-1,4) = 0;             %d
    U(Cnt_Punts*2-1,5) = 0;             %d
    U(Cnt_Punts*2-1,6) = 0;             %d
    U(Cnt_Punts*2,1)   = 0;
    U(Cnt_Punts*2,2)   = 0;
    U(Cnt_Punts*2,3)   = 0;
    U(Cnt_Punts*2,4)   = matches(Cnt_Punts,1);
    U(Cnt_Punts*2,5)   = matches(Cnt_Punts,2);
    U(Cnt_Punts*2,6)   = 1;

    X(Cnt_Punts*2-1) = matches(Cnt_Punts,3);
    X(Cnt_Punts*2)   = matches(Cnt_Punts,4);
end

if Svd,
  A = CalcBySVD(U,X);
else
  % A =(inv(U'*U)) * U' * X;
  A = U \ X;
end

H = zeros (3,3);

H(1,1)= A(1);
H(1,2)= A(2);
H(1,3)= A(3);
H(2,1)= A(4);
H(2,2)= A(5);
H(2,3)= A(6);
H(3,1)= 0;
H(3,2)= 0;
H(3,3)= 1;

end

%% CalcBySVD

function  result = CalcBySVD(A, X)
% result = CalcBySVD(A, X)
% result = inv(A' * A) * A' * X using Singular Value descomposition
    degrees = size(A,2);
    result = zeros(degrees,1);
    [U, w, V] = svd(A);
    for column=1:degrees
        numeratorMatrix = (U(:,column))' * X;
        numerator = numeratorMatrix(1,1);
        denominator = w(column, column);
        result = result + V(:,column) * (numerator/denominator);
    end
end
