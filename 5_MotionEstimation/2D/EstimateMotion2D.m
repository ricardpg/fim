
% EstimateMotion2D --------------------------------------------------------
%
%  Rejecting of outliers using RANSAC an features coordinates.
%
%  Author(s)     : Marina Kudinova, Jordi Ferrer, Ricard Prados
%  e-mail        : { kudinova, jferrerp, rprados } @ eia.udg.es
%  Working Group : Computer Vision and Robotics Group
%  Homepage      : http://vicorob.udg.es/
%  Project       : FIM
%  File          : EstimateMotion2D.m
%  Date          : 17/09/2006 - 21/06/2007
%  Compiler      : MATLAB > 7.0
%
% -------------------------------------------------------------------------

function [ Homography, Inliers ] = EstimateMotion2D ( Img1Features, ... 
                                                      Img2Features, ...
                                                      Matches, ...
                                                      Parameters )

% Checking Input and Output parameters

error ( nargchk ( 4, 4, nargin ) );
error ( nargoutchk ( 2, 2, nargout ) );

Params = CheckMotionParameters ( Parameters );

if ~isstruct(Params)
    Homography = [];
    Inliers = [];
    return;
end

% EstimationMethod  = upper(Params.EstimationMethod);
HomographyModel   = upper(Params.HomographyModel);
SpatialStdev      = Params.SpatialStdev;

% Defining the Input for RANSAC

Features = [];
Features(1:2,:) = Img2Features ( 1:2, Matches(2,:) );
Features(3:4,:) = Img1Features ( 1:2, Matches(1,:) );

switch HomographyModel    
    case 'EUCLIDEAN';
        s = 2;
        fittingfn  = @ComputeEuclideanTransformationFIM;
    case 'SIMILARITY';
        s = 2;
        fittingfn  = @ComputeSimilarityTransformationFIM;
    case 'AFFINE';
        s = 3;
        fittingfn  = @ComputeAffineTransformationFIM;
    case 'PROJECTIVE';
        s = 4;
        fittingfn  = @ComputeProjectiveTransformationFIM;
    case 'PROJECTIVENLM';
        s = 4;
        fittingfn  = @ComputeNLProjectiveTransformationFIM;
    otherwise; error('Wrong homography model.\n');
end

distfn   = @DetectInliers;
degenfn  = @FindSamePoints;
t        = sqrt(5.99) * SpatialStdev;

% Checking if there is enough matches for Homography estimation

if size(Features,2) < s;
    Homography = [];
    Inliers = [];
    return;
end

% Executing RANSAC

[ Homography, Inliers ] = ransac ( Features, fittingfn, distfn, degenfn, s, t );

if isempty ( Homography ); return; end;

% !!! Reestimation of the Homography using the whole set of the inliers

AcceptedMatches = Matches ( :, Inliers );
GoodFeatures(1:2,:) = Img2Features ( 1:2, AcceptedMatches(2,:) );
GoodFeatures(3:4,:) = Img1Features ( 1:2, AcceptedMatches(1,:) );
Homography = fittingfn ( GoodFeatures );

end

% distfn for RANSAC: DetectInliers ----------------------------------------

function [ inliers, M ] = DetectInliers ( H, x, t )
error ( nargchk ( 3, 3, nargin ) );
error ( nargoutchk ( 2, 2, nargout ) );
Wrp = x(1:2,:);
Src = x(3:4,:);
Wrp_prj = ApplyHomo ( H, Wrp );
a = ( ( Src - Wrp_prj ) .^ 2 );
d = sqrt ( a(1,:) + a(2,:) );
inliers = find ( abs(d) < sqrt(5.99) * t ); % Multiple View Geometry, p.123
M = H;
end

% degenfn for RANSAC: FindSamePoints --------------------------------------

function r = FindSamePoints ( x )
error ( nargchk ( 1, 1, nargin ) );
error ( nargoutchk ( 1, 1, nargout ) );
s = size ( x, 2 );
k = 0;
for i = 1 : s
    k = k + sum ( ismember( x(1:2,:)', x(1:2,i)','rows') );
    k = k + sum ( ismember( x(3:4,:)', x(3:4,i)','rows') );
end
r = ( k ~= 2*s );
end

function H = ComputeEuclideanTransformationFIM ( P )
  H = ComputeEuclideanTransformation ( P(1:2,:), P(3:4,:) );
end

function H = ComputeSimilarityTransformationFIM ( P )
  H = ComputeSimilarityTransformation ( P(1:2,:), P(3:4,:) );
end

function H = ComputeAffineTransformationFIM ( P )
  H = ComputeAffineTransformation ( P(1:2,:), P(3:4,:) );
end

function H = ComputeProjectiveTransformationFIM ( P )
  H = ComputeProjectiveTransformation ( P(1:2,:), P(3:4,:) );
end

function H = ComputeNLProjectiveTransformationFIM ( P )
  H = getNLProjectiveTransformation ( P' );
end
