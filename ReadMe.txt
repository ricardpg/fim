
-- FIM : Feature Based Image Matching -------------------------------------
  
   Author(s)     : Marina Kudinova, Jordi Ferrer, Ricard Prados
   e-mail        : { kudinova, jferrerp, rprados } @ eia.udg.es

   Working Group : Computer Vision and Robotics Group
   Homepage      : http://vicorob.udg.es/

   Project       : FIM
   File          : ReadMe.txt
   Date          : 29/01/2007 - 01/03/2007
   Compilers     : MATLAB > 7.0, Visual Studio 7 or 8

---------------------------------------------------------------------------

!!! FIM compilation needs Visual Studio 7 or 8 installed on PC !!!

When starting FIM for the first time follow the next workload: 


WINDOWS USERS:
==============

1. Select the Microsoft Visual Studio 7 or 8 as the compiler that should 
   be used by Matlab. To do this enter the command displayed below and 
   then press "Y", select the coresponding to this compiler number 
   and press "Y" again.

   >> mex -setup

 
2. In the Matlab Command Window, from the FIM root folder run the 
   Compile.m script:
   
   >> Compile


3. To be able to use SURF detector and descriptor copy the dynamic library 

       surfWINDLL.dll

   from 

       FIM/0_3rdParty/surf

   into one of the folders of your dynamic library path.
 
   Typical places are:

       C:\Program Files\MATLAB\R2006b\bin\win32

       C:\Program Files\MATLAB\R2006b\bin;

       C:\WINDOWS;

       C:\WINDOWS\system32\wbem;

       C:\WINDOWS\system32;

       C:\Program Files\MATLAB\R2006b\bin\win32;


4. To be able to use MSER detector add the absolute path to "mser" folder 

       D:\FIM\0_3rdParty\mser  (example)
   
   as one of your system paths, or copy mser.exe executable from that 
   folder into one of the folders of your system path (see above).


5. Restart your Matlab to add new system paths.

 
6. Execute the RunFIM script from FIM root folder to check whether FIM 
   works:

   >> RunFIM



LINUX USERS:
============


1. Ensure that g++ 4.x compiler is already installed in your system.


2. In the Matlab Command Window, from the FIM root folder run the 
   Compile.m script:
   
   >> Compile


3. To be able to use SURF detector and descriptor copy the dynamic library 

       libSurf.so

   into one of the folders of your dynamic library path. This places
   can be looked for in the /etc/ld.so.conf file.


4. To be able to use MSER detector add the absolute path to the next folder
   as user path.

       FIM/0_3rdParty/mser

   
5. Restart your Matlab to add new system paths.

 
6. Execute the RunFIM script from FIM root folder to check whether FIM 
   works:

   >> RunFIM

